(function (dF, dB) {
    var dd, cB, ej = dF.document, cG = dF.location, cI = dF.navigator, cZ = dF.jQuery, e = dF.$, eu = Array.prototype.push, eA = Array.prototype.slice, eq = Array.prototype.indexOf, bZ = Object.prototype.toString, eo = Object.prototype.hasOwnProperty, c0 = String.prototype.trim, cE = function (a, b) {
        return new cE.fn.init(a, b, dd)
    }, es = /[\-+]?(?:\d*\.|)\d+(?:[eE][\-+]?\d+|)/.source, ew = /\S/, ey = /\s+/, dO = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, ds = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/, dI = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, dc = /^[\],:{}\s]*$/, da = /(?:^|:|,)(?:\s*\[)+/g, de = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g, dg = /"[^"\\\r\n]*"|true|false|null|-?(?:\d\d*\.|)\d+(?:[eE][\-+]?\d+|)/g, b7 = /^-ms-/, cx = /-([\da-z])/gi, et = function (b, a) {
        return (a + "").toUpperCase()
    }, el = function () {
        if (ej.addEventListener) {
            ej.removeEventListener("DOMContentLoaded", el, false);
            cE.ready()
        } else {
            if (ej.readyState === "complete") {
                ej.detachEvent("onreadystatechange", el);
                cE.ready()
            }
        }
    }, ei = {};
    cE.fn = cE.prototype = {
        constructor: cE, init: function (f, h, d) {
            var b, a, c, g;
            if (!f) {
                return this
            }
            if (f.nodeType) {
                this.context = this[0] = f;
                this.length = 1;
                return this
            }
            if (typeof f === "string") {
                if (f.charAt(0) === "<" && f.charAt(f.length - 1) === ">" && f.length >= 3) {
                    b = [null, f, null]
                } else {
                    b = ds.exec(f)
                }
                if (b && (b[1] || !h)) {
                    if (b[1]) {
                        h = h instanceof cE ? h[0] : h;
                        g = (h && h.nodeType ? h.ownerDocument || h : ej);
                        f = cE.parseHTML(b[1], g, true);
                        if (dI.test(b[1]) && cE.isPlainObject(h)) {
                            this.attr.call(f, h, true)
                        }
                        return cE.merge(this, f)
                    } else {
                        a = ej.getElementById(b[2]);
                        if (a && a.parentNode) {
                            if (a.id !== b[2]) {
                                return d.find(f)
                            }
                            this.length = 1;
                            this[0] = a
                        }
                        this.context = ej;
                        this.selector = f;
                        return this
                    }
                } else {
                    if (!h || h.jquery) {
                        return (h || d).find(f)
                    } else {
                        return this.constructor(h).find(f)
                    }
                }
            } else {
                if (cE.isFunction(f)) {
                    return d.ready(f)
                }
            }
            if (f.selector !== dB) {
                this.selector = f.selector;
                this.context = f.context
            }
            return cE.makeArray(f, this)
        }, selector: "", jquery: "1.8.3", length: 0, size: function () {
            return this.length
        }, toArray: function () {
            return eA.call(this)
        }, get: function (a) {
            return a == null ? this.toArray() : (a < 0 ? this[this.length + a] : this[a])
        }, pushStack: function (d, c, b) {
            var a = cE.merge(this.constructor(), d);
            a.prevObject = this;
            a.context = this.context;
            if (c === "find") {
                a.selector = this.selector + (this.selector ? " " : "") + b
            } else {
                if (c) {
                    a.selector = this.selector + "." + c + "(" + b + ")"
                }
            }
            return a
        }, each: function (a, b) {
            return cE.each(this, a, b)
        }, ready: function (a) {
            cE.ready.promise().done(a);
            return this
        }, eq: function (a) {
            a = +a;
            return a === -1 ? this.slice(a) : this.slice(a, a + 1)
        }, first: function () {
            return this.eq(0)
        }, last: function () {
            return this.eq(-1)
        }, slice: function () {
            return this.pushStack(eA.apply(this, arguments), "slice", eA.call(arguments).join(","))
        }, map: function (a) {
            return this.pushStack(cE.map(this, function (c, b) {
                return a.call(c, b, c)
            }))
        }, end: function () {
            return this.prevObject || this.constructor(null)
        }, push: eu, sort: [].sort, splice: [].splice
    };
    cE.fn.init.prototype = cE.fn;
    cE.extend = cE.fn.extend = function () {
        var g, f, h, j, a, k, i = arguments[0] || {}, c = 1, d = arguments.length, b = false;
        if (typeof i === "boolean") {
            b = i;
            i = arguments[1] || {};
            c = 2
        }
        if (typeof i !== "object" && !cE.isFunction(i)) {
            i = {}
        }
        if (d === c) {
            i = this;
            --c
        }
        for (; c < d; c++) {
            if ((g = arguments[c]) != null) {
                for (f in g) {
                    h = i[f];
                    j = g[f];
                    if (i === j) {
                        continue
                    }
                    if (b && j && (cE.isPlainObject(j) || (a = cE.isArray(j)))) {
                        if (a) {
                            a = false;
                            k = h && cE.isArray(h) ? h : []
                        } else {
                            k = h && cE.isPlainObject(h) ? h : {}
                        }
                        i[f] = cE.extend(b, k, j)
                    } else {
                        if (j !== dB) {
                            i[f] = j
                        }
                    }
                }
            }
        }
        return i
    };
    cE.extend({
        noConflict: function (a) {
            if (dF.$ === cE) {
                dF.$ = e
            }
            if (a && dF.jQuery === cE) {
                dF.jQuery = cZ
            }
            return cE
        }, isReady: false, readyWait: 1, holdReady: function (a) {
            if (a) {
                cE.readyWait++
            } else {
                cE.ready(true)
            }
        }, ready: function (a) {
            if (a === true ? --cE.readyWait : cE.isReady) {
                return
            }
            if (!ej.body) {
                return setTimeout(cE.ready, 1)
            }
            cE.isReady = true;
            if (a !== true && --cE.readyWait > 0) {
                return
            }
            cB.resolveWith(ej, [cE]);
            if (cE.fn.trigger) {
                cE(ej).trigger("ready").off("ready")
            }
        }, isFunction: function (a) {
            return cE.type(a) === "function"
        }, isArray: Array.isArray || function (a) {
            return cE.type(a) === "array"
        }, isWindow: function (a) {
            return a != null && a == a.window
        }, isNumeric: function (a) {
            return !isNaN(parseFloat(a)) && isFinite(a)
        }, type: function (a) {
            return a == null ? String(a) : ei[bZ.call(a)] || "object"
        }, isPlainObject: function (b) {
            if (!b || cE.type(b) !== "object" || b.nodeType || cE.isWindow(b)) {
                return false
            }
            try {
                if (b.constructor && !eo.call(b, "constructor") && !eo.call(b.constructor.prototype, "isPrototypeOf")) {
                    return false
                }
            } catch (c) {
                return false
            }
            var a;
            for (a in b) {
            }
            return a === dB || eo.call(b, a)
        }, isEmptyObject: function (a) {
            var b;
            for (b in a) {
                return false
            }
            return true
        }, error: function (a) {
            throw new Error(a)
        }, parseHTML: function (c, d, b) {
            var a;
            if (!c || typeof c !== "string") {
                return null
            }
            if (typeof d === "boolean") {
                b = d;
                d = 0
            }
            d = d || ej;
            if ((a = dI.exec(c))) {
                return [d.createElement(a[1])]
            }
            a = cE.buildFragment([c], d, b ? null : []);
            return cE.merge([], (a.cacheable ? cE.clone(a.fragment) : a.fragment).childNodes)
        }, parseJSON: function (a) {
            if (!a || typeof a !== "string") {
                return null
            }
            a = cE.trim(a);
            if (dF.JSON && dF.JSON.parse) {
                return dF.JSON.parse(a)
            }
            if (dc.test(a.replace(de, "@").replace(dg, "]").replace(da, ""))) {
                return (new Function("return " + a))()
            }
            cE.error("Invalid JSON: " + a)
        }, parseXML: function (d) {
            var c, b;
            if (!d || typeof d !== "string") {
                return null
            }
            try {
                if (dF.DOMParser) {
                    b = new DOMParser();
                    c = b.parseFromString(d, "text/xml")
                } else {
                    c = new ActiveXObject("Microsoft.XMLDOM");
                    c.async = "false";
                    c.loadXML(d)
                }
            } catch (a) {
                c = dB
            }
            if (!c || !c.documentElement || c.getElementsByTagName("parsererror").length) {
                cE.error("Invalid XML: " + d)
            }
            return c
        }, noop: function () {
        }, globalEval: function (a) {
            if (a && ew.test(a)) {
                (dF.execScript || function (b) {
                    dF["eval"].call(dF, b)
                })(a)
            }
        }, camelCase: function (a) {
            return a.replace(b7, "ms-").replace(cx, et)
        }, nodeName: function (b, a) {
            return b.nodeName && b.nodeName.toLowerCase() === a.toLowerCase()
        }, each: function (f, g, h) {
            var d, a = 0, c = f.length, b = c === dB || cE.isFunction(f);
            if (h) {
                if (b) {
                    for (d in f) {
                        if (g.apply(f[d], h) === false) {
                            break
                        }
                    }
                } else {
                    for (; a < c;) {
                        if (g.apply(f[a++], h) === false) {
                            break
                        }
                    }
                }
            } else {
                if (b) {
                    for (d in f) {
                        if (g.call(f[d], d, f[d]) === false) {
                            break
                        }
                    }
                } else {
                    for (; a < c;) {
                        if (g.call(f[a], a, f[a++]) === false) {
                            break
                        }
                    }
                }
            }
            return f
        }, trim: c0 && !c0.call("\uFEFF\xA0") ? function (a) {
            return a == null ? "" : c0.call(a)
        } : function (a) {
            return a == null ? "" : (a + "").replace(dO, "")
        }, makeArray: function (d, c) {
            var b, a = c || [];
            if (d != null) {
                b = cE.type(d);
                if (d.length == null || b === "string" || b === "function" || b === "regexp" || cE.isWindow(d)) {
                    eu.call(a, d)
                } else {
                    cE.merge(a, d)
                }
            }
            return a
        }, inArray: function (c, d, a) {
            var b;
            if (d) {
                if (eq) {
                    return eq.call(d, c, a)
                }
                b = d.length;
                a = a ? a < 0 ? Math.max(0, b + a) : a : 0;
                for (; a < b; a++) {
                    if (a in d && d[a] === c) {
                        return a
                    }
                }
            }
            return -1
        }, merge: function (f, c) {
            var b = c.length, d = f.length, a = 0;
            if (typeof b === "number") {
                for (; a < b; a++) {
                    f[d++] = c[a]
                }
            } else {
                while (c[a] !== dB) {
                    f[d++] = c[a++]
                }
            }
            f.length = d;
            return f
        }, grep: function (g, h, b) {
            var f, d = [], a = 0, c = g.length;
            b = !!b;
            for (; a < c; a++) {
                f = !!h(g[a], a);
                if (b !== f) {
                    d.push(g[a])
                }
            }
            return d
        }, map: function (a, i, j) {
            var h, d, g = [], b = 0, f = a.length, c = a instanceof cE || f !== dB && typeof f === "number" && ((f > 0 && a[0] && a[f - 1]) || f === 0 || cE.isArray(a));
            if (c) {
                for (; b < f; b++) {
                    h = i(a[b], b, j);
                    if (h != null) {
                        g[g.length] = h
                    }
                }
            } else {
                for (d in a) {
                    h = i(a[d], d, j);
                    if (h != null) {
                        g[g.length] = h
                    }
                }
            }
            return g.concat.apply([], g)
        }, guid: 1, proxy: function (a, d) {
            var c, f, b;
            if (typeof d === "string") {
                c = a[d];
                d = a;
                a = c
            }
            if (!cE.isFunction(a)) {
                return dB
            }
            f = eA.call(arguments, 2);
            b = function () {
                return a.apply(d, f.concat(eA.call(arguments)))
            };
            b.guid = a.guid = a.guid || cE.guid++;
            return b
        }, access: function (a, d, g, j, k, b, i) {
            var c, l = g == null, f = 0, h = a.length;
            if (g && typeof g === "object") {
                for (f in g) {
                    cE.access(a, d, f, g[f], 1, b, j)
                }
                k = 1
            } else {
                if (j !== dB) {
                    c = i === dB && cE.isFunction(j);
                    if (l) {
                        if (c) {
                            c = d;
                            d = function (m, n, o) {
                                return c.call(cE(m), o)
                            }
                        } else {
                            d.call(a, j);
                            d = null
                        }
                    }
                    if (d) {
                        for (; f < h; f++) {
                            d(a[f], g, c ? j.call(a[f], f, d(a[f], g)) : j, i)
                        }
                    }
                    k = 1
                }
            }
            return k ? a : l ? d.call(a) : h ? d(a[0], g) : b
        }, now: function () {
            return (new Date()).getTime()
        }
    });
    cE.ready.promise = function (b) {
        if (!cB) {
            cB = cE.Deferred();
            if (ej.readyState === "complete") {
                setTimeout(cE.ready, 1)
            } else {
                if (ej.addEventListener) {
                    ej.addEventListener("DOMContentLoaded", el, false);
                    dF.addEventListener("load", cE.ready, false)
                } else {
                    ej.attachEvent("onreadystatechange", el);
                    dF.attachEvent("onload", cE.ready);
                    var c = false;
                    try {
                        c = dF.frameElement == null && ej.documentElement
                    } catch (a) {
                    }
                    if (c && c.doScroll) {
                        (function d() {
                            if (!cE.isReady) {
                                try {
                                    c.doScroll("left")
                                } catch (f) {
                                    return setTimeout(d, 50)
                                }
                                cE.ready()
                            }
                        })()
                    }
                }
            }
        }
        return cB.promise(b)
    };
    cE.each("Boolean Number String Function Array Date RegExp Object".split(" "), function (b, a) {
        ei["[object " + a + "]"] = a.toLowerCase()
    });
    dd = cE(ej);
    var cT = {};

    function dV(a) {
        var b = cT[a] = {};
        cE.each(a.split(ey), function (c, d) {
            b[d] = true
        });
        return b
    }

    cE.Callbacks = function (h) {
        h = typeof h === "string" ? (cT[h] || dV(h)) : cE.extend({}, h);
        var g, k, a, d, c, b, f = [], j = !h.once && [], l = function (m) {
            g = h.memory && m;
            k = true;
            b = d || 0;
            d = 0;
            c = f.length;
            a = true;
            for (; f && b < c; b++) {
                if (f[b].apply(m[0], m[1]) === false && h.stopOnFalse) {
                    g = false;
                    break
                }
            }
            a = false;
            if (f) {
                if (j) {
                    if (j.length) {
                        l(j.shift())
                    }
                } else {
                    if (g) {
                        f = []
                    } else {
                        i.disable()
                    }
                }
            }
        }, i = {
            add: function () {
                if (f) {
                    var n = f.length;
                    (function m(o) {
                        cE.each(o, function (p, q) {
                            var r = cE.type(q);
                            if (r === "function") {
                                if (!h.unique || !i.has(q)) {
                                    f.push(q)
                                }
                            } else {
                                if (q && q.length && r !== "string") {
                                    m(q)
                                }
                            }
                        })
                    })(arguments);
                    if (a) {
                        c = f.length
                    } else {
                        if (g) {
                            d = n;
                            l(g)
                        }
                    }
                }
                return this
            }, remove: function () {
                if (f) {
                    cE.each(arguments, function (m, n) {
                        var o;
                        while ((o = cE.inArray(n, f, o)) > -1) {
                            f.splice(o, 1);
                            if (a) {
                                if (o <= c) {
                                    c--
                                }
                                if (o <= b) {
                                    b--
                                }
                            }
                        }
                    })
                }
                return this
            }, has: function (m) {
                return cE.inArray(m, f) > -1
            }, empty: function () {
                f = [];
                return this
            }, disable: function () {
                f = j = g = dB;
                return this
            }, disabled: function () {
                return !f
            }, lock: function () {
                j = dB;
                if (!g) {
                    i.disable()
                }
                return this
            }, locked: function () {
                return !j
            }, fireWith: function (n, m) {
                m = m || [];
                m = [n, m.slice ? m.slice() : m];
                if (f && (!k || j)) {
                    if (a) {
                        j.push(m)
                    } else {
                        l(m)
                    }
                }
                return this
            }, fire: function () {
                i.fireWith(this, arguments);
                return this
            }, fired: function () {
                return !!k
            }
        };
        return i
    };
    cE.extend({
        Deferred: function (d) {
            var c = [["resolve", "done", cE.Callbacks("once memory"), "resolved"], ["reject", "fail", cE.Callbacks("once memory"), "rejected"], ["notify", "progress", cE.Callbacks("memory")]], b = "pending", a = {
                state: function () {
                    return b
                }, always: function () {
                    f.done(arguments).fail(arguments);
                    return this
                }, then: function () {
                    var g = arguments;
                    return cE.Deferred(function (h) {
                        cE.each(c, function (k, l) {
                            var i = l[0], j = g[k];
                            f[l[1]](cE.isFunction(j) ? function () {
                                var m = j.apply(this, arguments);
                                if (m && cE.isFunction(m.promise)) {
                                    m.promise().done(h.resolve).fail(h.reject).progress(h.notify)
                                } else {
                                    h[i + "With"](this === f ? h : this, [m])
                                }
                            } : h[i])
                        });
                        g = null
                    }).promise()
                }, promise: function (g) {
                    return g != null ? cE.extend(g, a) : a
                }
            }, f = {};
            a.pipe = a.then;
            cE.each(c, function (g, j) {
                var h = j[2], i = j[3];
                a[j[1]] = h.add;
                if (i) {
                    h.add(function () {
                        b = i
                    }, c[g ^ 1][2].disable, c[2][2].lock)
                }
                f[j[0]] = h.fire;
                f[j[0] + "With"] = h.fireWith
            });
            a.promise(f);
            if (d) {
                d.call(f, f)
            }
            return f
        }, when: function (h) {
            var j = 0, g = eA.call(arguments), a = g.length, d = a !== 1 || (h && cE.isFunction(h.promise)) ? a : 0, k = d === 1 ? h : cE.Deferred(), i = function (m, l, n) {
                return function (o) {
                    l[m] = this;
                    n[m] = arguments.length > 1 ? eA.call(arguments) : o;
                    if (n === c) {
                        k.notifyWith(l, n)
                    } else {
                        if (!(--d)) {
                            k.resolveWith(l, n)
                        }
                    }
                }
            }, c, b, f;
            if (a > 1) {
                c = new Array(a);
                b = new Array(a);
                f = new Array(a);
                for (; j < a; j++) {
                    if (g[j] && cE.isFunction(g[j].promise)) {
                        g[j].promise().done(i(j, f, g)).fail(k.reject).progress(i(j, b, c))
                    } else {
                        --d
                    }
                }
            }
            if (!d) {
                k.resolveWith(f, g)
            }
            return k.promise()
        }
    });
    cE.support = (function () {
        var n, a, l, m, k, i, g, f, h, j, b, c = ej.createElement("div");
        c.setAttribute("className", "t");
        c.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
        a = c.getElementsByTagName("*");
        l = c.getElementsByTagName("a")[0];
        if (!a || !l || !a.length) {
            return {}
        }
        m = ej.createElement("select");
        k = m.appendChild(ej.createElement("option"));
        i = c.getElementsByTagName("input")[0];
        l.style.cssText = "top:1px;float:left;opacity:.5";
        n = {
            leadingWhitespace: (c.firstChild.nodeType === 3),
            tbody: !c.getElementsByTagName("tbody").length,
            htmlSerialize: !!c.getElementsByTagName("link").length,
            style: /top/.test(l.getAttribute("style")),
            hrefNormalized: (l.getAttribute("href") === "/a"),
            opacity: /^0.5/.test(l.style.opacity),
            cssFloat: !!l.style.cssFloat,
            checkOn: (i.value === "on"),
            optSelected: k.selected,
            getSetAttribute: c.className !== "t",
            enctype: !!ej.createElement("form").enctype,
            html5Clone: ej.createElement("nav").cloneNode(true).outerHTML !== "<:nav></:nav>",
            boxModel: (ej.compatMode === "CSS1Compat"),
            submitBubbles: true,
            changeBubbles: true,
            focusinBubbles: false,
            deleteExpando: true,
            noCloneEvent: true,
            inlineBlockNeedsLayout: false,
            shrinkWrapBlocks: false,
            reliableMarginRight: true,
            boxSizingReliable: true,
            pixelPosition: false
        };
        i.checked = true;
        n.noCloneChecked = i.cloneNode(true).checked;
        m.disabled = true;
        n.optDisabled = !k.disabled;
        try {
            delete c.test
        } catch (d) {
            n.deleteExpando = false
        }
        if (!c.addEventListener && c.attachEvent && c.fireEvent) {
            c.attachEvent("onclick", b = function () {
                n.noCloneEvent = false
            });
            c.cloneNode(true).fireEvent("onclick");
            c.detachEvent("onclick", b)
        }
        i = ej.createElement("input");
        i.value = "t";
        i.setAttribute("type", "radio");
        n.radioValue = i.value === "t";
        i.setAttribute("checked", "checked");
        i.setAttribute("name", "t");
        c.appendChild(i);
        g = ej.createDocumentFragment();
        g.appendChild(c.lastChild);
        n.checkClone = g.cloneNode(true).cloneNode(true).lastChild.checked;
        n.appendChecked = i.checked;
        g.removeChild(i);
        g.appendChild(c);
        if (c.attachEvent) {
            for (h in {submit: true, change: true, focusin: true}) {
                f = "on" + h;
                j = (f in c);
                if (!j) {
                    c.setAttribute(f, "return;");
                    j = (typeof c[f] === "function")
                }
                n[h + "Bubbles"] = j
            }
        }
        cE(function () {
            var o, p, s, r, q = "padding:0;margin:0;border:0;display:block;overflow:hidden;", t = ej.getElementsByTagName("body")[0];
            if (!t) {
                return
            }
            o = ej.createElement("div");
            o.style.cssText = "visibility:hidden;border:0;width:0;height:0;position:static;top:0;margin-top:1px";
            t.insertBefore(o, t.firstChild);
            p = ej.createElement("div");
            o.appendChild(p);
            p.innerHTML = "<table><tr><td></td><td>t</td></tr></table>";
            s = p.getElementsByTagName("td");
            s[0].style.cssText = "padding:0;margin:0;border:0;display:none";
            j = (s[0].offsetHeight === 0);
            s[0].style.display = "";
            s[1].style.display = "none";
            n.reliableHiddenOffsets = j && (s[0].offsetHeight === 0);
            p.innerHTML = "";
            p.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;";
            n.boxSizing = (p.offsetWidth === 4);
            n.doesNotIncludeMarginInBodyOffset = (t.offsetTop !== 1);
            if (dF.getComputedStyle) {
                n.pixelPosition = (dF.getComputedStyle(p, null) || {}).top !== "1%";
                n.boxSizingReliable = (dF.getComputedStyle(p, null) || {width: "4px"}).width === "4px";
                r = ej.createElement("div");
                r.style.cssText = p.style.cssText = q;
                r.style.marginRight = r.style.width = "0";
                p.style.width = "1px";
                p.appendChild(r);
                n.reliableMarginRight = !parseFloat((dF.getComputedStyle(r, null) || {}).marginRight)
            }
            if (typeof p.style.zoom !== "undefined") {
                p.innerHTML = "";
                p.style.cssText = q + "width:1px;padding:1px;display:inline;zoom:1";
                n.inlineBlockNeedsLayout = (p.offsetWidth === 3);
                p.style.display = "block";
                p.style.overflow = "visible";
                p.innerHTML = "<div></div>";
                p.firstChild.style.width = "5px";
                n.shrinkWrapBlocks = (p.offsetWidth !== 3);
                o.style.zoom = 1
            }
            t.removeChild(o);
            o = p = s = r = null
        });
        g.removeChild(c);
        a = l = m = k = i = g = c = null;
        return n
    })();
    var ch = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/, b8 = /([A-Z])/g;
    cE.extend({
        cache: {},
        deletedIds: [],
        uuid: 0,
        expando: "jQuery" + (cE.fn.jquery + Math.random()).replace(/\D/g, ""),
        noData: {embed: true, object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000", applet: true},
        hasData: function (a) {
            a = a.nodeType ? cE.cache[a[cE.expando]] : a[cE.expando];
            return !!a && !cy(a)
        },
        data: function (a, g, k, h) {
            if (!cE.acceptData(a)) {
                return
            }
            var j, i, d = cE.expando, b = typeof g === "string", f = a.nodeType, l = f ? cE.cache : a, c = f ? a[d] : a[d] && d;
            if ((!c || !l[c] || (!h && !l[c].data)) && b && k === dB) {
                return
            }
            if (!c) {
                if (f) {
                    a[d] = c = cE.deletedIds.pop() || cE.guid++
                } else {
                    c = d
                }
            }
            if (!l[c]) {
                l[c] = {};
                if (!f) {
                    l[c].toJSON = cE.noop
                }
            }
            if (typeof g === "object" || typeof g === "function") {
                if (h) {
                    l[c] = cE.extend(l[c], g)
                } else {
                    l[c].data = cE.extend(l[c].data, g)
                }
            }
            j = l[c];
            if (!h) {
                if (!j.data) {
                    j.data = {}
                }
                j = j.data
            }
            if (k !== dB) {
                j[cE.camelCase(g)] = k
            }
            if (b) {
                i = j[g];
                if (i == null) {
                    i = j[cE.camelCase(g)]
                }
            } else {
                i = j
            }
            return i
        },
        removeData: function (i, f, g) {
            if (!cE.acceptData(i)) {
                return
            }
            var h, a, d, c = i.nodeType, j = c ? cE.cache : i, b = c ? i[cE.expando] : cE.expando;
            if (!j[b]) {
                return
            }
            if (f) {
                h = g ? j[b] : j[b].data;
                if (h) {
                    if (!cE.isArray(f)) {
                        if (f in h) {
                            f = [f]
                        } else {
                            f = cE.camelCase(f);
                            if (f in h) {
                                f = [f]
                            } else {
                                f = f.split(" ")
                            }
                        }
                    }
                    for (a = 0, d = f.length; a < d; a++) {
                        delete h[f[a]]
                    }
                    if (!(g ? cy : cE.isEmptyObject)(h)) {
                        return
                    }
                }
            }
            if (!g) {
                delete j[b].data;
                if (!cy(j[b])) {
                    return
                }
            }
            if (c) {
                cE.cleanData([i], true)
            } else {
                if (cE.support.deleteExpando || j != j.window) {
                    delete j[b]
                } else {
                    j[b] = null
                }
            }
        },
        _data: function (b, a, c) {
            return cE.data(b, a, c, true)
        },
        acceptData: function (b) {
            var a = b.nodeName && cE.noData[b.nodeName.toLowerCase()];
            return !a || a !== true && b.getAttribute("classid") === a
        }
    });
    cE.fn.extend({
        data: function (c, i) {
            var h, g, k, f, d, a = this[0], b = 0, j = null;
            if (c === dB) {
                if (this.length) {
                    j = cE.data(a);
                    if (a.nodeType === 1 && !cE._data(a, "parsedAttrs")) {
                        k = a.attributes;
                        for (d = k.length; b < d; b++) {
                            f = k[b].name;
                            if (!f.indexOf("data-")) {
                                f = cE.camelCase(f.substring(5));
                                ef(a, f, j[f])
                            }
                        }
                        cE._data(a, "parsedAttrs", true)
                    }
                }
                return j
            }
            if (typeof c === "object") {
                return this.each(function () {
                    cE.data(this, c)
                })
            }
            h = c.split(".", 2);
            h[1] = h[1] ? "." + h[1] : "";
            g = h[1] + "!";
            return cE.access(this, function (l) {
                if (l === dB) {
                    j = this.triggerHandler("getData" + g, [h[0]]);
                    if (j === dB && a) {
                        j = cE.data(a, c);
                        j = ef(a, c, j)
                    }
                    return j === dB && h[1] ? this.data(h[0]) : j
                }
                h[1] = l;
                this.each(function () {
                    var m = cE(this);
                    m.triggerHandler("setData" + g, h);
                    cE.data(this, c, l);
                    m.triggerHandler("changeData" + g, h)
                })
            }, null, i, arguments.length > 1, null, false)
        }, removeData: function (a) {
            return this.each(function () {
                cE.removeData(this, a)
            })
        }
    });
    function ef(b, c, f) {
        if (f === dB && b.nodeType === 1) {
            var d = "data-" + c.replace(b8, "-$1").toLowerCase();
            f = b.getAttribute(d);
            if (typeof f === "string") {
                try {
                    f = f === "true" ? true : f === "false" ? false : f === "null" ? null : +f + "" === f ? +f : ch.test(f) ? cE.parseJSON(f) : f
                } catch (a) {
                }
                cE.data(b, c, f)
            } else {
                f = dB
            }
        }
        return f
    }

    function cy(a) {
        var b;
        for (b in a) {
            if (b === "data" && cE.isEmptyObject(a[b])) {
                continue
            }
            if (b !== "toJSON") {
                return false
            }
        }
        return true
    }

    cE.extend({
        queue: function (c, b, d) {
            var a;
            if (c) {
                b = (b || "fx") + "queue";
                a = cE._data(c, b);
                if (d) {
                    if (!a || cE.isArray(d)) {
                        a = cE._data(c, b, cE.makeArray(d))
                    } else {
                        a.push(d)
                    }
                }
                return a || []
            }
        }, dequeue: function (h, f) {
            f = f || "fx";
            var c = cE.queue(h, f), d = c.length, g = c.shift(), a = cE._queueHooks(h, f), b = function () {
                cE.dequeue(h, f)
            };
            if (g === "inprogress") {
                g = c.shift();
                d--
            }
            if (g) {
                if (f === "fx") {
                    c.unshift("inprogress")
                }
                delete a.stop;
                g.call(h, b, a)
            }
            if (!d && a) {
                a.empty.fire()
            }
        }, _queueHooks: function (c, a) {
            var b = a + "queueHooks";
            return cE._data(c, b) || cE._data(c, b, {
                    empty: cE.Callbacks("once memory").add(function () {
                        cE.removeData(c, a + "queue", true);
                        cE.removeData(c, b, true)
                    })
                })
        }
    });
    cE.fn.extend({
        queue: function (a, c) {
            var b = 2;
            if (typeof a !== "string") {
                c = a;
                a = "fx";
                b--
            }
            if (arguments.length < b) {
                return cE.queue(this[0], a)
            }
            return c === dB ? this : this.each(function () {
                var d = cE.queue(this, a, c);
                cE._queueHooks(this, a);
                if (a === "fx" && d[0] !== "inprogress") {
                    cE.dequeue(this, a)
                }
            })
        }, dequeue: function (a) {
            return this.each(function () {
                cE.dequeue(this, a)
            })
        }, delay: function (b, a) {
            b = cE.fx ? cE.fx.speeds[b] || b : b;
            a = a || "fx";
            return this.queue(a, function (d, c) {
                var f = setTimeout(d, b);
                c.stop = function () {
                    clearTimeout(f)
                }
            })
        }, clearQueue: function (a) {
            return this.queue(a || "fx", [])
        }, promise: function (g, c) {
            var f, i = 1, h = cE.Deferred(), a = this, b = this.length, d = function () {
                if (!(--i)) {
                    h.resolveWith(a, [a])
                }
            };
            if (typeof g !== "string") {
                c = g;
                g = dB
            }
            g = g || "fx";
            while (b--) {
                f = cE._data(a[b], g + "queueHooks");
                if (f && f.empty) {
                    i++;
                    f.empty.add(d)
                }
            }
            d();
            return h.promise(c)
        }
    });
    var cL, ee, ez, cp = /[\t\r\n]/g, dw = /\r/g, c2 = /^(?:button|input)$/i, cH = /^(?:button|input|object|select|textarea)$/i, ct = /^a(?:rea|)$/i, cf = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i, cg = cE.support.getSetAttribute;
    cE.fn.extend({
        attr: function (b, a) {
            return cE.access(this, cE.attr, b, a, arguments.length > 1)
        }, removeAttr: function (a) {
            return this.each(function () {
                cE.removeAttr(this, a)
            })
        }, prop: function (b, a) {
            return cE.access(this, cE.prop, b, a, arguments.length > 1)
        }, removeProp: function (a) {
            a = cE.propFix[a] || a;
            return this.each(function () {
                try {
                    this[a] = dB;
                    delete this[a]
                } catch (b) {
                }
            })
        }, addClass: function (g) {
            var a, c, d, b, f, i, h;
            if (cE.isFunction(g)) {
                return this.each(function (j) {
                    cE(this).addClass(g.call(this, j, this.className))
                })
            }
            if (g && typeof g === "string") {
                a = g.split(ey);
                for (c = 0, d = this.length; c < d; c++) {
                    b = this[c];
                    if (b.nodeType === 1) {
                        if (!b.className && a.length === 1) {
                            b.className = g
                        } else {
                            f = " " + b.className + " ";
                            for (i = 0, h = a.length; i < h; i++) {
                                if (f.indexOf(" " + a[i] + " ") < 0) {
                                    f += a[i] + " "
                                }
                            }
                            b.className = cE.trim(f)
                        }
                    }
                }
            }
            return this
        }, removeClass: function (g) {
            var f, a, b, i, h, c, d;
            if (cE.isFunction(g)) {
                return this.each(function (j) {
                    cE(this).removeClass(g.call(this, j, this.className))
                })
            }
            if ((g && typeof g === "string") || g === dB) {
                f = (g || "").split(ey);
                for (c = 0, d = this.length; c < d; c++) {
                    b = this[c];
                    if (b.nodeType === 1 && b.className) {
                        a = (" " + b.className + " ").replace(cp, " ");
                        for (i = 0, h = f.length; i < h; i++) {
                            while (a.indexOf(" " + f[i] + " ") >= 0) {
                                a = a.replace(" " + f[i] + " ", " ")
                            }
                        }
                        b.className = g ? cE.trim(a) : ""
                    }
                }
            }
            return this
        }, toggleClass: function (b, c) {
            var a = typeof b, d = typeof c === "boolean";
            if (cE.isFunction(b)) {
                return this.each(function (f) {
                    cE(this).toggleClass(b.call(this, f, this.className, c), c)
                })
            }
            return this.each(function () {
                if (a === "string") {
                    var f, h = 0, i = cE(this), j = c, g = b.split(ey);
                    while ((f = g[h++])) {
                        j = d ? j : !i.hasClass(f);
                        i[j ? "addClass" : "removeClass"](f)
                    }
                } else {
                    if (a === "undefined" || a === "boolean") {
                        if (this.className) {
                            cE._data(this, "__className__", this.className)
                        }
                        this.className = this.className || b === false ? "" : cE._data(this, "__className__") || ""
                    }
                }
            })
        }, hasClass: function (b) {
            var d = " " + b + " ", c = 0, a = this.length;
            for (; c < a; c++) {
                if (this[c].nodeType === 1 && (" " + this[c].className + " ").replace(cp, " ").indexOf(d) >= 0) {
                    return true
                }
            }
            return false
        }, val: function (c) {
            var d, b, a, f = this[0];
            if (!arguments.length) {
                if (f) {
                    d = cE.valHooks[f.type] || cE.valHooks[f.nodeName.toLowerCase()];
                    if (d && "get" in d && (b = d.get(f, "value")) !== dB) {
                        return b
                    }
                    b = f.value;
                    return typeof b === "string" ? b.replace(dw, "") : b == null ? "" : b
                }
                return
            }
            a = cE.isFunction(c);
            return this.each(function (g) {
                var i, h = cE(this);
                if (this.nodeType !== 1) {
                    return
                }
                if (a) {
                    i = c.call(this, g, h.val())
                } else {
                    i = c
                }
                if (i == null) {
                    i = ""
                } else {
                    if (typeof i === "number") {
                        i += ""
                    } else {
                        if (cE.isArray(i)) {
                            i = cE.map(i, function (j) {
                                return j == null ? "" : j + ""
                            })
                        }
                    }
                }
                d = cE.valHooks[this.type] || cE.valHooks[this.nodeName.toLowerCase()];
                if (!d || !("set" in d) || d.set(this, i, "value") === dB) {
                    this.value = i
                }
            })
        }
    });
    cE.extend({
        valHooks: {
            option: {
                get: function (b) {
                    var a = b.attributes.value;
                    return !a || a.specified ? b.value : b.text
                }
            }, select: {
                get: function (j) {
                    var g, d, f = j.options, a = j.selectedIndex, c = j.type === "select-one" || a < 0, h = c ? null : [], b = c ? a + 1 : f.length, i = a < 0 ? b : c ? a : 0;
                    for (; i < b; i++) {
                        d = f[i];
                        if ((d.selected || i === a) && (cE.support.optDisabled ? !d.disabled : d.getAttribute("disabled") === null) && (!d.parentNode.disabled || !cE.nodeName(d.parentNode, "optgroup"))) {
                            g = cE(d).val();
                            if (c) {
                                return g
                            }
                            h.push(g)
                        }
                    }
                    return h
                }, set: function (c, b) {
                    var a = cE.makeArray(b);
                    cE(c).find("option").each(function () {
                        this.selected = cE.inArray(cE(this).val(), a) >= 0
                    });
                    if (!a.length) {
                        c.selectedIndex = -1
                    }
                    return a
                }
            }
        },
        attrFn: {},
        attr: function (i, a, g, d) {
            var f, h, b, c = i.nodeType;
            if (!i || c === 3 || c === 8 || c === 2) {
                return
            }
            if (d && cE.isFunction(cE.fn[a])) {
                return cE(i)[a](g)
            }
            if (typeof i.getAttribute === "undefined") {
                return cE.prop(i, a, g)
            }
            b = c !== 1 || !cE.isXMLDoc(i);
            if (b) {
                a = a.toLowerCase();
                h = cE.attrHooks[a] || (cf.test(a) ? ee : cL)
            }
            if (g !== dB) {
                if (g === null) {
                    cE.removeAttr(i, a);
                    return
                } else {
                    if (h && "set" in h && b && (f = h.set(i, g, a)) !== dB) {
                        return f
                    } else {
                        i.setAttribute(a, g + "");
                        return g
                    }
                }
            } else {
                if (h && "get" in h && b && (f = h.get(i, a)) !== null) {
                    return f
                } else {
                    f = i.getAttribute(a);
                    return f === null ? dB : f
                }
            }
        },
        removeAttr: function (g, f) {
            var d, h, c, b, a = 0;
            if (f && g.nodeType === 1) {
                h = f.split(ey);
                for (; a < h.length; a++) {
                    c = h[a];
                    if (c) {
                        d = cE.propFix[c] || c;
                        b = cf.test(c);
                        if (!b) {
                            cE.attr(g, c, "")
                        }
                        g.removeAttribute(cg ? c : d);
                        if (b && d in g) {
                            g[d] = false
                        }
                    }
                }
            }
        },
        attrHooks: {
            type: {
                set: function (c, a) {
                    if (c2.test(c.nodeName) && c.parentNode) {
                        cE.error("type property can't be changed")
                    } else {
                        if (!cE.support.radioValue && a === "radio" && cE.nodeName(c, "input")) {
                            var b = c.value;
                            c.setAttribute("type", a);
                            if (b) {
                                c.value = b
                            }
                            return a
                        }
                    }
                }
            }, value: {
                get: function (b, a) {
                    if (cL && cE.nodeName(b, "button")) {
                        return cL.get(b, a)
                    }
                    return a in b ? b.value : null
                }, set: function (c, a, b) {
                    if (cL && cE.nodeName(c, "button")) {
                        return cL.set(c, a, b)
                    }
                    c.value = a
                }
            }
        },
        propFix: {
            tabindex: "tabIndex",
            readonly: "readOnly",
            "for": "htmlFor",
            "class": "className",
            maxlength: "maxLength",
            cellspacing: "cellSpacing",
            cellpadding: "cellPadding",
            rowspan: "rowSpan",
            colspan: "colSpan",
            usemap: "useMap",
            frameborder: "frameBorder",
            contenteditable: "contentEditable"
        },
        prop: function (h, a, f) {
            var d, g, b, c = h.nodeType;
            if (!h || c === 3 || c === 8 || c === 2) {
                return
            }
            b = c !== 1 || !cE.isXMLDoc(h);
            if (b) {
                a = cE.propFix[a] || a;
                g = cE.propHooks[a]
            }
            if (f !== dB) {
                if (g && "set" in g && (d = g.set(h, f, a)) !== dB) {
                    return d
                } else {
                    return (h[a] = f)
                }
            } else {
                if (g && "get" in g && (d = g.get(h, a)) !== null) {
                    return d
                } else {
                    return h[a]
                }
            }
        },
        propHooks: {
            tabIndex: {
                get: function (a) {
                    var b = a.getAttributeNode("tabindex");
                    return b && b.specified ? parseInt(b.value, 10) : cH.test(a.nodeName) || ct.test(a.nodeName) && a.href ? 0 : dB
                }
            }
        }
    });
    ee = {
        get: function (c, a) {
            var d, b = cE.prop(c, a);
            return b === true || typeof b !== "boolean" && (d = c.getAttributeNode(a)) && d.nodeValue !== false ? a.toLowerCase() : dB
        }, set: function (d, b, c) {
            var a;
            if (b === false) {
                cE.removeAttr(d, c)
            } else {
                a = cE.propFix[c] || c;
                if (a in d) {
                    d[a] = true
                }
                d.setAttribute(c, c.toLowerCase())
            }
            return c
        }
    };
    if (!cg) {
        ez = {name: true, id: true, coords: true};
        cL = cE.valHooks.button = {
            get: function (c, b) {
                var a;
                a = c.getAttributeNode(b);
                return a && (ez[b] ? a.value !== "" : a.specified) ? a.value : dB
            }, set: function (d, b, c) {
                var a = d.getAttributeNode(c);
                if (!a) {
                    a = ej.createAttribute(c);
                    d.setAttributeNode(a)
                }
                return (a.value = b + "")
            }
        };
        cE.each(["width", "height"], function (b, a) {
            cE.attrHooks[a] = cE.extend(cE.attrHooks[a], {
                set: function (c, d) {
                    if (d === "") {
                        c.setAttribute(a, "auto");
                        return d
                    }
                }
            })
        });
        cE.attrHooks.contenteditable = {
            get: cL.get, set: function (c, a, b) {
                if (a === "") {
                    a = "false"
                }
                cL.set(c, a, b)
            }
        }
    }
    if (!cE.support.hrefNormalized) {
        cE.each(["href", "src", "width", "height"], function (b, a) {
            cE.attrHooks[a] = cE.extend(cE.attrHooks[a], {
                get: function (c) {
                    var d = c.getAttribute(a, 2);
                    return d === null ? dB : d
                }
            })
        })
    }
    if (!cE.support.style) {
        cE.attrHooks.style = {
            get: function (a) {
                return a.style.cssText.toLowerCase() || dB
            }, set: function (b, a) {
                return (b.style.cssText = a + "")
            }
        }
    }
    if (!cE.support.optSelected) {
        cE.propHooks.selected = cE.extend(cE.propHooks.selected, {
            get: function (b) {
                var a = b.parentNode;
                if (a) {
                    a.selectedIndex;
                    if (a.parentNode) {
                        a.parentNode.selectedIndex
                    }
                }
                return null
            }
        })
    }
    if (!cE.support.enctype) {
        cE.propFix.enctype = "encoding"
    }
    if (!cE.support.checkOn) {
        cE.each(["radio", "checkbox"], function () {
            cE.valHooks[this] = {
                get: function (a) {
                    return a.getAttribute("value") === null ? "on" : a.value
                }
            }
        })
    }
    cE.each(["radio", "checkbox"], function () {
        cE.valHooks[this] = cE.extend(cE.valHooks[this], {
            set: function (b, a) {
                if (cE.isArray(a)) {
                    return (b.checked = cE.inArray(cE(b).val(), a) >= 0)
                }
            }
        })
    });
    var cK = /^(?:textarea|input|select)$/i, c4 = /^([^\.]*|)(?:\.(.+)|)$/, cU = /(?:^|\s)hover(\.\S+|)\b/, b2 = /^key/, b6 = /^(?:mouse|contextmenu)|click/, cJ = /^(?:focusinfocus|focusoutblur)$/, co = function (a) {
        return cE.event.special.hover ? a : a.replace(cU, "mouseenter$1 mouseleave$1")
    };
    cE.event = {
        add: function (l, p, g, q, j) {
            var a, b, c, m, n, o, i, d, f, h, k;
            if (l.nodeType === 3 || l.nodeType === 8 || !p || !g || !(a = cE._data(l))) {
                return
            }
            if (g.handler) {
                f = g;
                g = f.handler;
                j = f.selector
            }
            if (!g.guid) {
                g.guid = cE.guid++
            }
            c = a.events;
            if (!c) {
                a.events = c = {}
            }
            b = a.handle;
            if (!b) {
                a.handle = b = function (r) {
                    return typeof cE !== "undefined" && (!r || cE.event.triggered !== r.type) ? cE.event.dispatch.apply(b.elem, arguments) : dB
                };
                b.elem = l
            }
            p = cE.trim(co(p)).split(" ");
            for (m = 0; m < p.length; m++) {
                n = c4.exec(p[m]) || [];
                o = n[1];
                i = (n[2] || "").split(".").sort();
                k = cE.event.special[o] || {};
                o = (j ? k.delegateType : k.bindType) || o;
                k = cE.event.special[o] || {};
                d = cE.extend({
                    type: o,
                    origType: n[1],
                    data: q,
                    handler: g,
                    guid: g.guid,
                    selector: j,
                    needsContext: j && cE.expr.match.needsContext.test(j),
                    namespace: i.join(".")
                }, f);
                h = c[o];
                if (!h) {
                    h = c[o] = [];
                    h.delegateCount = 0;
                    if (!k.setup || k.setup.call(l, q, i, b) === false) {
                        if (l.addEventListener) {
                            l.addEventListener(o, b, false)
                        } else {
                            if (l.attachEvent) {
                                l.attachEvent("on" + o, b)
                            }
                        }
                    }
                }
                if (k.add) {
                    k.add.call(l, d);
                    if (!d.handler.guid) {
                        d.handler.guid = g.guid
                    }
                }
                if (j) {
                    h.splice(h.delegateCount++, 0, d)
                } else {
                    h.push(d)
                }
                cE.event.global[o] = true
            }
            l = null
        },
        global: {},
        remove: function (r, q, d, k, g) {
            var n, o, p, j, h, i, f, a, m, b, c, l = cE.hasData(r) && cE._data(r);
            if (!l || !(a = l.events)) {
                return
            }
            q = cE.trim(co(q || "")).split(" ");
            for (n = 0; n < q.length; n++) {
                o = c4.exec(q[n]) || [];
                p = j = o[1];
                h = o[2];
                if (!p) {
                    for (p in a) {
                        cE.event.remove(r, p + q[n], d, k, true)
                    }
                    continue
                }
                m = cE.event.special[p] || {};
                p = (k ? m.delegateType : m.bindType) || p;
                b = a[p] || [];
                i = b.length;
                h = h ? new RegExp("(^|\\.)" + h.split(".").sort().join("\\.(?:.*\\.|)") + "(\\.|$)") : null;
                for (f = 0; f < b.length; f++) {
                    c = b[f];
                    if ((g || j === c.origType) && (!d || d.guid === c.guid) && (!h || h.test(c.namespace)) && (!k || k === c.selector || k === "**" && c.selector)) {
                        b.splice(f--, 1);
                        if (c.selector) {
                            b.delegateCount--
                        }
                        if (m.remove) {
                            m.remove.call(r, c)
                        }
                    }
                }
                if (b.length === 0 && i !== b.length) {
                    if (!m.teardown || m.teardown.call(r, h, l.handle) === false) {
                        cE.removeEvent(r, p, l.handle)
                    }
                    delete a[p]
                }
            }
            if (cE.isEmptyObject(a)) {
                delete l.handle;
                cE.removeData(r, "events", true)
            }
        },
        customEvent: {getData: true, setData: true, changeData: true},
        trigger: function (d, b, c, m) {
            if (c && (c.nodeType === 3 || c.nodeType === 8)) {
                return
            }
            var l, g, i, a, k, n, o, h, f, q, p = d.type || d, j = [];
            if (cJ.test(p + cE.event.triggered)) {
                return
            }
            if (p.indexOf("!") >= 0) {
                p = p.slice(0, -1);
                g = true
            }
            if (p.indexOf(".") >= 0) {
                j = p.split(".");
                p = j.shift();
                j.sort()
            }
            if ((!c || cE.event.customEvent[p]) && !cE.event.global[p]) {
                return
            }
            d = typeof d === "object" ? d[cE.expando] ? d : new cE.Event(p, d) : new cE.Event(p);
            d.type = p;
            d.isTrigger = true;
            d.exclusive = g;
            d.namespace = j.join(".");
            d.namespace_re = d.namespace ? new RegExp("(^|\\.)" + j.join("\\.(?:.*\\.|)") + "(\\.|$)") : null;
            n = p.indexOf(":") < 0 ? "on" + p : "";
            if (!c) {
                l = cE.cache;
                for (i in l) {
                    if (l[i].events && l[i].events[p]) {
                        cE.event.trigger(d, b, l[i].handle.elem, true)
                    }
                }
                return
            }
            d.result = dB;
            if (!d.target) {
                d.target = c
            }
            b = b != null ? cE.makeArray(b) : [];
            b.unshift(d);
            o = cE.event.special[p] || {};
            if (o.trigger && o.trigger.apply(c, b) === false) {
                return
            }
            f = [[c, o.bindType || p]];
            if (!m && !o.noBubble && !cE.isWindow(c)) {
                q = o.delegateType || p;
                a = cJ.test(q + p) ? c : c.parentNode;
                for (k = c; a; a = a.parentNode) {
                    f.push([a, q]);
                    k = a
                }
                if (k === (c.ownerDocument || ej)) {
                    f.push([k.defaultView || k.parentWindow || dF, q])
                }
            }
            for (i = 0; i < f.length && !d.isPropagationStopped(); i++) {
                a = f[i][0];
                d.type = f[i][1];
                h = (cE._data(a, "events") || {})[d.type] && cE._data(a, "handle");
                if (h) {
                    h.apply(a, b)
                }
                h = n && a[n];
                if (h && cE.acceptData(a) && h.apply && h.apply(a, b) === false) {
                    d.preventDefault()
                }
            }
            d.type = p;
            if (!m && !d.isDefaultPrevented()) {
                if ((!o._default || o._default.apply(c.ownerDocument, b) === false) && !(p === "click" && cE.nodeName(c, "a")) && cE.acceptData(c)) {
                    if (n && c[p] && ((p !== "focus" && p !== "blur") || d.target.offsetWidth !== 0) && !cE.isWindow(c)) {
                        k = c[n];
                        if (k) {
                            c[n] = null
                        }
                        cE.event.triggered = p;
                        c[p]();
                        cE.event.triggered = dB;
                        if (k) {
                            c[n] = k
                        }
                    }
                }
            }
            return d.result
        },
        dispatch: function (b) {
            b = cE.event.fix(b || dF.event);
            var g, h, l, m, p, i, j, c, o, k, f = ((cE._data(this, "events") || {})[b.type] || []), a = f.delegateCount, r = eA.call(arguments), n = !b.exclusive && !b.namespace, q = cE.event.special[b.type] || {}, d = [];
            r[0] = b;
            b.delegateTarget = this;
            if (q.preDispatch && q.preDispatch.call(this, b) === false) {
                return
            }
            if (a && !(b.button && b.type === "click")) {
                for (l = b.target; l != this; l = l.parentNode || this) {
                    if (l.disabled !== true || b.type !== "click") {
                        p = {};
                        j = [];
                        for (g = 0; g < a; g++) {
                            c = f[g];
                            o = c.selector;
                            if (p[o] === dB) {
                                p[o] = c.needsContext ? cE(o, this).index(l) >= 0 : cE.find(o, this, null, [l]).length
                            }
                            if (p[o]) {
                                j.push(c)
                            }
                        }
                        if (j.length) {
                            d.push({elem: l, matches: j})
                        }
                    }
                }
            }
            if (f.length > a) {
                d.push({elem: this, matches: f.slice(a)})
            }
            for (g = 0; g < d.length && !b.isPropagationStopped(); g++) {
                i = d[g];
                b.currentTarget = i.elem;
                for (h = 0; h < i.matches.length && !b.isImmediatePropagationStopped(); h++) {
                    c = i.matches[h];
                    if (n || (!b.namespace && !c.namespace) || b.namespace_re && b.namespace_re.test(c.namespace)) {
                        b.data = c.data;
                        b.handleObj = c;
                        m = ((cE.event.special[c.origType] || {}).handle || c.handler).apply(i.elem, r);
                        if (m !== dB) {
                            b.result = m;
                            if (m === false) {
                                b.preventDefault();
                                b.stopPropagation()
                            }
                        }
                    }
                }
            }
            if (q.postDispatch) {
                q.postDispatch.call(this, b)
            }
            return b.result
        },
        props: "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "), filter: function (b, a) {
                if (b.which == null) {
                    b.which = a.charCode != null ? a.charCode : a.keyCode
                }
                return b
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function (b, f) {
                var c, a, h, g = f.button, d = f.fromElement;
                if (b.pageX == null && f.clientX != null) {
                    c = b.target.ownerDocument || ej;
                    a = c.documentElement;
                    h = c.body;
                    b.pageX = f.clientX + (a && a.scrollLeft || h && h.scrollLeft || 0) - (a && a.clientLeft || h && h.clientLeft || 0);
                    b.pageY = f.clientY + (a && a.scrollTop || h && h.scrollTop || 0) - (a && a.clientTop || h && h.clientTop || 0)
                }
                if (!b.relatedTarget && d) {
                    b.relatedTarget = d === b.target ? f.toElement : d
                }
                if (!b.which && g !== dB) {
                    b.which = (g & 1 ? 1 : (g & 2 ? 3 : (g & 4 ? 2 : 0)))
                }
                return b
            }
        },
        fix: function (f) {
            if (f[cE.expando]) {
                return f
            }
            var b, d, c = f, a = cE.event.fixHooks[f.type] || {}, g = a.props ? this.props.concat(a.props) : this.props;
            f = cE.Event(c);
            for (b = g.length; b;) {
                d = g[--b];
                f[d] = c[d]
            }
            if (!f.target) {
                f.target = c.srcElement || ej
            }
            if (f.target.nodeType === 3) {
                f.target = f.target.parentNode
            }
            f.metaKey = !!f.metaKey;
            return a.filter ? a.filter(f, c) : f
        },
        special: {
            load: {noBubble: true},
            focus: {delegateType: "focusin"},
            blur: {delegateType: "focusout"},
            beforeunload: {
                setup: function (c, a, b) {
                    if (cE.isWindow(this)) {
                        this.onbeforeunload = b
                    }
                }, teardown: function (a, b) {
                    if (this.onbeforeunload === b) {
                        this.onbeforeunload = null
                    }
                }
            }
        },
        simulate: function (d, b, c, f) {
            var a = cE.extend(new cE.Event(), c, {type: d, isSimulated: true, originalEvent: {}});
            if (f) {
                cE.event.trigger(a, null, b)
            } else {
                cE.event.dispatch.call(b, a)
            }
            if (a.isDefaultPrevented()) {
                c.preventDefault()
            }
        }
    };
    cE.event.handle = cE.event.dispatch;
    cE.removeEvent = ej.removeEventListener ? function (c, a, b) {
        if (c.removeEventListener) {
            c.removeEventListener(a, b, false)
        }
    } : function (d, b, c) {
        var a = "on" + b;
        if (d.detachEvent) {
            if (typeof d[a] === "undefined") {
                d[a] = null
            }
            d.detachEvent(a, c)
        }
    };
    cE.Event = function (a, b) {
        if (!(this instanceof cE.Event)) {
            return new cE.Event(a, b)
        }
        if (a && a.type) {
            this.originalEvent = a;
            this.type = a.type;
            this.isDefaultPrevented = (a.defaultPrevented || a.returnValue === false || a.getPreventDefault && a.getPreventDefault()) ? cF : cD
        } else {
            this.type = a
        }
        if (b) {
            cE.extend(this, b)
        }
        this.timeStamp = a && a.timeStamp || cE.now();
        this[cE.expando] = true
    };
    function cD() {
        return false
    }

    function cF() {
        return true
    }

    cE.Event.prototype = {
        preventDefault: function () {
            this.isDefaultPrevented = cF;
            var a = this.originalEvent;
            if (!a) {
                return
            }
            if (a.preventDefault) {
                a.preventDefault()
            } else {
                a.returnValue = false
            }
        }, stopPropagation: function () {
            this.isPropagationStopped = cF;
            var a = this.originalEvent;
            if (!a) {
                return
            }
            if (a.stopPropagation) {
                a.stopPropagation()
            }
            a.cancelBubble = true
        }, stopImmediatePropagation: function () {
            this.isImmediatePropagationStopped = cF;
            this.stopPropagation()
        }, isDefaultPrevented: cD, isPropagationStopped: cD, isImmediatePropagationStopped: cD
    };
    cE.each({mouseenter: "mouseover", mouseleave: "mouseout"}, function (a, b) {
        cE.event.special[a] = {
            delegateType: b, bindType: b, handle: function (c) {
                var g, i = this, f = c.relatedTarget, d = c.handleObj, h = d.selector;
                if (!f || (f !== i && !cE.contains(i, f))) {
                    c.type = d.origType;
                    g = d.handler.apply(this, arguments);
                    c.type = b
                }
                return g
            }
        }
    });
    if (!cE.support.submitBubbles) {
        cE.event.special.submit = {
            setup: function () {
                if (cE.nodeName(this, "form")) {
                    return false
                }
                cE.event.add(this, "click._submit keypress._submit", function (c) {
                    var a = c.target, b = cE.nodeName(a, "input") || cE.nodeName(a, "button") ? a.form : dB;
                    if (b && !cE._data(b, "_submit_attached")) {
                        cE.event.add(b, "submit._submit", function (d) {
                            d._submit_bubble = true
                        });
                        cE._data(b, "_submit_attached", true)
                    }
                })
            }, postDispatch: function (a) {
                if (a._submit_bubble) {
                    delete a._submit_bubble;
                    if (this.parentNode && !a.isTrigger) {
                        cE.event.simulate("submit", this.parentNode, a, true)
                    }
                }
            }, teardown: function () {
                if (cE.nodeName(this, "form")) {
                    return false
                }
                cE.event.remove(this, "._submit")
            }
        }
    }
    if (!cE.support.changeBubbles) {
        cE.event.special.change = {
            setup: function () {
                if (cK.test(this.nodeName)) {
                    if (this.type === "checkbox" || this.type === "radio") {
                        cE.event.add(this, "propertychange._change", function (a) {
                            if (a.originalEvent.propertyName === "checked") {
                                this._just_changed = true
                            }
                        });
                        cE.event.add(this, "click._change", function (a) {
                            if (this._just_changed && !a.isTrigger) {
                                this._just_changed = false
                            }
                            cE.event.simulate("change", this, a, true)
                        })
                    }
                    return false
                }
                cE.event.add(this, "beforeactivate._change", function (b) {
                    var a = b.target;
                    if (cK.test(a.nodeName) && !cE._data(a, "_change_attached")) {
                        cE.event.add(a, "change._change", function (c) {
                            if (this.parentNode && !c.isSimulated && !c.isTrigger) {
                                cE.event.simulate("change", this.parentNode, c, true)
                            }
                        });
                        cE._data(a, "_change_attached", true)
                    }
                })
            }, handle: function (a) {
                var b = a.target;
                if (this !== b || a.isSimulated || a.isTrigger || (b.type !== "radio" && b.type !== "checkbox")) {
                    return a.handleObj.handler.apply(this, arguments)
                }
            }, teardown: function () {
                cE.event.remove(this, "._change");
                return !cK.test(this.nodeName)
            }
        }
    }
    if (!cE.support.focusinBubbles) {
        cE.each({focus: "focusin", blur: "focusout"}, function (b, c) {
            var d = 0, a = function (f) {
                cE.event.simulate(c, f.target, cE.event.fix(f), true)
            };
            cE.event.special[c] = {
                setup: function () {
                    if (d++ === 0) {
                        ej.addEventListener(b, a, true)
                    }
                }, teardown: function () {
                    if (--d === 0) {
                        ej.removeEventListener(b, a, true)
                    }
                }
            }
        })
    }
    cE.fn.extend({
        on: function (f, c, h, g, a) {
            var b, d;
            if (typeof f === "object") {
                if (typeof c !== "string") {
                    h = h || c;
                    c = dB
                }
                for (d in f) {
                    this.on(d, c, h, f[d], a)
                }
                return this
            }
            if (h == null && g == null) {
                g = c;
                h = c = dB
            } else {
                if (g == null) {
                    if (typeof c === "string") {
                        g = h;
                        h = dB
                    } else {
                        g = h;
                        h = c;
                        c = dB
                    }
                }
            }
            if (g === false) {
                g = cD
            } else {
                if (!g) {
                    return this
                }
            }
            if (a === 1) {
                b = g;
                g = function (i) {
                    cE().off(i);
                    return b.apply(this, arguments)
                };
                g.guid = b.guid || (b.guid = cE.guid++)
            }
            return this.each(function () {
                cE.event.add(this, f, g, h, c)
            })
        }, one: function (b, a, d, c) {
            return this.on(b, a, d, c, 1)
        }, off: function (c, a, f) {
            var d, b;
            if (c && c.preventDefault && c.handleObj) {
                d = c.handleObj;
                cE(c.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler);
                return this
            }
            if (typeof c === "object") {
                for (b in c) {
                    this.off(b, a, c[b])
                }
                return this
            }
            if (a === false || typeof a === "function") {
                f = a;
                a = dB
            }
            if (f === false) {
                f = cD
            }
            return this.each(function () {
                cE.event.remove(this, c, f, a)
            })
        }, bind: function (a, c, b) {
            return this.on(a, null, c, b)
        }, unbind: function (a, b) {
            return this.off(a, null, b)
        }, live: function (a, c, b) {
            cE(this.context).on(a, this.selector, c, b);
            return this
        }, die: function (a, b) {
            cE(this.context).off(a, this.selector || "**", b);
            return this
        }, delegate: function (a, b, d, c) {
            return this.on(b, a, d, c)
        }, undelegate: function (b, a, c) {
            return arguments.length === 1 ? this.off(b, "**") : this.off(a, b || "**", c)
        }, trigger: function (a, b) {
            return this.each(function () {
                cE.event.trigger(a, b, this)
            })
        }, triggerHandler: function (a, b) {
            if (this[0]) {
                return cE.event.trigger(a, b, this[0], true)
            }
        }, toggle: function (d) {
            var f = arguments, a = d.guid || cE.guid++, b = 0, c = function (g) {
                var h = (cE._data(this, "lastToggle" + d.guid) || 0) % b;
                cE._data(this, "lastToggle" + d.guid, h + 1);
                g.preventDefault();
                return f[h].apply(this, arguments) || false
            };
            c.guid = a;
            while (b < f.length) {
                f[b++].guid = a
            }
            return this.click(c)
        }, hover: function (a, b) {
            return this.mouseenter(a).mouseleave(b || a)
        }
    });
    cE.each(("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu").split(" "), function (b, a) {
        cE.fn[a] = function (c, d) {
            if (d == null) {
                d = c;
                c = null
            }
            return arguments.length > 0 ? this.on(a, null, c, d) : this.trigger(a)
        };
        if (b2.test(a)) {
            cE.event.fixHooks[a] = cE.event.keyHooks
        }
        if (b6.test(a)) {
            cE.event.fixHooks[a] = cE.event.mouseHooks
        }
    });
    (function (s, q) {
        var k, c, ac, ae, am, D, x, au, ag, A, j = true, m = "undefined", aa = ("sizcache" + Math.random()).replace(".", ""), n = String, S = s.document, Q = S.documentElement, O = 0, U = 0, C = [].pop, J = [].push, ar = [].slice, ak = [].indexOf || function (ax) {
                var av = 0, aw = this.length;
                for (; av < aw; av++) {
                    if (this[av] === ax) {
                        return av
                    }
                }
                return -1
            }, ao = function (aw, av) {
            aw[aa] = av == null || av;
            return aw
        }, I = function () {
            var aw = {}, av = [];
            return ao(function (ax, ay) {
                if (av.push(ax) > ac.cacheLength) {
                    delete aw[av.shift()]
                }
                return (aw[ax + " "] = ay)
            }, aw)
        }, v = I(), o = I(), z = I(), r = "[\\x20\\t\\r\\n\\f]", t = "(?:\\\\.|[-\\w]|[^\\x00-\\xa0])+", ai = t.replace("w", "w#"), y = "([*^$|!~]?=)", i = "\\[" + r + "*(" + t + ")" + r + "*(?:" + y + r + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + ai + ")|)|)" + r + "*\\]", G = ":(" + t + ")(?:\\((?:(['\"])((?:\\\\.|[^\\\\])*?)\\2|([^()[\\]]*|(?:(?:" + i + ")|[^:]|\\\\.)*|.*))\\)|)", E = ":(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + r + "*((?:-\\d)?\\d*)" + r + "*\\)|)(?=[^-]|$)", af = new RegExp("^" + r + "+|((?:^|[^\\\\])(?:\\\\.)*)" + r + "+$", "g"), P = new RegExp("^" + r + "*," + r + "*"), N = new RegExp("^" + r + "*([\\x20\\t\\r\\n\\f>+~])" + r + "*"), Z = new RegExp(G), ab = /^(?:#([\w\-]+)|(\w+)|\.([\w\-]+))$/, X = /^:not/, ad = /[\x20\t\r\n\f]*[+~]/, R = /:not\($/, T = /h\d/i, V = /input|select|textarea|button/i, L = /\\(?!\\)/g, u = {
            ID: new RegExp("^#(" + t + ")"),
            CLASS: new RegExp("^\\.(" + t + ")"),
            NAME: new RegExp("^\\[name=['\"]?(" + t + ")['\"]?\\]"),
            TAG: new RegExp("^(" + t.replace("w", "w*") + ")"),
            ATTR: new RegExp("^" + i),
            PSEUDO: new RegExp("^" + G),
            POS: new RegExp(E, "i"),
            CHILD: new RegExp("^:(only|nth|first|last)-child(?:\\(" + r + "*(even|odd|(([+-]|)(\\d*)n|)" + r + "*(?:([+-]|)" + r + "*(\\d+)|))" + r + "*\\)|)", "i"),
            needsContext: new RegExp("^" + r + "*[>+~]|" + E, "i")
        }, a = function (ax) {
            var av = S.createElement("div");
            try {
                return ax(av)
            } catch (aw) {
                return false
            } finally {
                av = null
            }
        }, f = a(function (av) {
            av.appendChild(S.createComment(""));
            return !av.getElementsByTagName("*").length
        }), d = a(function (av) {
            av.innerHTML = "<a href='#'></a>";
            return av.firstChild && typeof av.firstChild.getAttribute !== m && av.firstChild.getAttribute("href") === "#"
        }), b = a(function (aw) {
            aw.innerHTML = "<select></select>";
            var av = typeof aw.lastChild.getAttribute("multiple");
            return av !== "boolean" && av !== "string"
        }), g = a(function (av) {
            av.innerHTML = "<div class='hidden e'></div><div class='hidden'></div>";
            if (!av.getElementsByClassName || !av.getElementsByClassName("e").length) {
                return false
            }
            av.lastChild.className = "e";
            return av.getElementsByClassName("e").length === 2
        }), h = a(function (aw) {
            aw.id = aa + 0;
            aw.innerHTML = "<a name='" + aa + "'></a><div name='" + aa + "'></div>";
            Q.insertBefore(aw, Q.firstChild);
            var av = S.getElementsByName && S.getElementsByName(aa).length === 2 + S.getElementsByName(aa + 0).length;
            c = !S.getElementById(aa);
            Q.removeChild(aw);
            return av
        });
        try {
            ar.call(Q.childNodes, 0)[0].nodeType
        } catch (W) {
            ar = function (av) {
                var ax, aw = [];
                for (; (ax = this[av]); av++) {
                    aw.push(ax)
                }
                return aw
            }
        }
        function ap(aB, aD, az, aA) {
            az = az || [];
            aD = aD || S;
            var ax, av, aC, aw, ay = aD.nodeType;
            if (!aB || typeof aB !== "string") {
                return az
            }
            if (ay !== 1 && ay !== 9) {
                return []
            }
            aC = am(aD);
            if (!aC && !aA) {
                if ((ax = ab.exec(aB))) {
                    if ((aw = ax[1])) {
                        if (ay === 9) {
                            av = aD.getElementById(aw);
                            if (av && av.parentNode) {
                                if (av.id === aw) {
                                    az.push(av);
                                    return az
                                }
                            } else {
                                return az
                            }
                        } else {
                            if (aD.ownerDocument && (av = aD.ownerDocument.getElementById(aw)) && D(aD, av) && av.id === aw) {
                                az.push(av);
                                return az
                            }
                        }
                    } else {
                        if (ax[2]) {
                            J.apply(az, ar.call(aD.getElementsByTagName(aB), 0));
                            return az
                        } else {
                            if ((aw = ax[3]) && g && aD.getElementsByClassName) {
                                J.apply(az, ar.call(aD.getElementsByClassName(aw), 0));
                                return az
                            }
                        }
                    }
                }
            }
            return ah(aB.replace(af, "$1"), aD, az, aA, aC)
        }

        ap.matches = function (av, aw) {
            return ap(av, null, null, aw)
        };
        ap.matchesSelector = function (aw, av) {
            return ap(av, null, null, [aw]).length > 0
        };
        function K(av) {
            return function (aw) {
                var ax = aw.nodeName.toLowerCase();
                return ax === "input" && aw.type === av
            }
        }

        function F(av) {
            return function (aw) {
                var ax = aw.nodeName.toLowerCase();
                return (ax === "input" || ax === "button") && aw.type === av
            }
        }

        function M(av) {
            return ao(function (aw) {
                aw = +aw;
                return ao(function (aB, az) {
                    var ay, aA = av([], aB.length, aw), ax = aA.length;
                    while (ax--) {
                        if (aB[(ay = aA[ax])]) {
                            aB[ay] = !(az[ay] = aB[ay])
                        }
                    }
                })
            })
        }

        ae = ap.getText = function (az) {
            var aw, ay = "", av = 0, ax = az.nodeType;
            if (ax) {
                if (ax === 1 || ax === 9 || ax === 11) {
                    if (typeof az.textContent === "string") {
                        return az.textContent
                    } else {
                        for (az = az.firstChild; az; az = az.nextSibling) {
                            ay += ae(az)
                        }
                    }
                } else {
                    if (ax === 3 || ax === 4) {
                        return az.nodeValue
                    }
                }
            } else {
                for (; (aw = az[av]); av++) {
                    ay += ae(aw)
                }
            }
            return ay
        };
        am = ap.isXML = function (av) {
            var aw = av && (av.ownerDocument || av).documentElement;
            return aw ? aw.nodeName !== "HTML" : false
        };
        D = ap.contains = Q.contains ? function (ay, aw) {
            var av = ay.nodeType === 9 ? ay.documentElement : ay, ax = aw && aw.parentNode;
            return ay === ax || !!(ax && ax.nodeType === 1 && av.contains && av.contains(ax))
        } : Q.compareDocumentPosition ? function (aw, av) {
            return av && !!(aw.compareDocumentPosition(av) & 16)
        } : function (aw, av) {
            while ((av = av.parentNode)) {
                if (av === aw) {
                    return true
                }
            }
            return false
        };
        ap.attr = function (ay, av) {
            var aw, ax = am(ay);
            if (!ax) {
                av = av.toLowerCase()
            }
            if ((aw = ac.attrHandle[av])) {
                return aw(ay)
            }
            if (ax || b) {
                return ay.getAttribute(av)
            }
            aw = ay.getAttributeNode(av);
            return aw ? typeof ay[av] === "boolean" ? ay[av] ? av : null : aw.specified ? aw.value : null : null
        };
        ac = ap.selectors = {
            cacheLength: 50,
            createPseudo: ao,
            match: u,
            attrHandle: d ? {} : {
                href: function (av) {
                    return av.getAttribute("href", 2)
                }, type: function (av) {
                    return av.getAttribute("type")
                }
            },
            find: {
                ID: c ? function (av, ay, ax) {
                    if (typeof ay.getElementById !== m && !ax) {
                        var aw = ay.getElementById(av);
                        return aw && aw.parentNode ? [aw] : []
                    }
                } : function (av, ay, ax) {
                    if (typeof ay.getElementById !== m && !ax) {
                        var aw = ay.getElementById(av);
                        return aw ? aw.id === av || typeof aw.getAttributeNode !== m && aw.getAttributeNode("id").value === av ? [aw] : q : []
                    }
                }, TAG: f ? function (av, aw) {
                    if (typeof aw.getElementsByTagName !== m) {
                        return aw.getElementsByTagName(av)
                    }
                } : function (ay, aA) {
                    var ax = aA.getElementsByTagName(ay);
                    if (ay === "*") {
                        var av, az = [], aw = 0;
                        for (; (av = ax[aw]); aw++) {
                            if (av.nodeType === 1) {
                                az.push(av)
                            }
                        }
                        return az
                    }
                    return ax
                }, NAME: h && function (av, aw) {
                    if (typeof aw.getElementsByName !== m) {
                        return aw.getElementsByName(name)
                    }
                }, CLASS: g && function (ax, av, aw) {
                    if (typeof av.getElementsByClassName !== m && !aw) {
                        return av.getElementsByClassName(ax)
                    }
                }
            },
            relative: {
                ">": {dir: "parentNode", first: true},
                " ": {dir: "parentNode"},
                "+": {dir: "previousSibling", first: true},
                "~": {dir: "previousSibling"}
            },
            preFilter: {
                ATTR: function (av) {
                    av[1] = av[1].replace(L, "");
                    av[3] = (av[4] || av[5] || "").replace(L, "");
                    if (av[2] === "~=") {
                        av[3] = " " + av[3] + " "
                    }
                    return av.slice(0, 4)
                }, CHILD: function (av) {
                    av[1] = av[1].toLowerCase();
                    if (av[1] === "nth") {
                        if (!av[2]) {
                            ap.error(av[0])
                        }
                        av[3] = +(av[3] ? av[4] + (av[5] || 1) : 2 * (av[2] === "even" || av[2] === "odd"));
                        av[4] = +((av[6] + av[7]) || av[2] === "odd")
                    } else {
                        if (av[2]) {
                            ap.error(av[0])
                        }
                    }
                    return av
                }, PSEUDO: function (av) {
                    var aw, ax;
                    if (u.CHILD.test(av[0])) {
                        return null
                    }
                    if (av[3]) {
                        av[2] = av[3]
                    } else {
                        if ((aw = av[4])) {
                            if (Z.test(aw) && (ax = p(aw, true)) && (ax = aw.indexOf(")", aw.length - ax) - aw.length)) {
                                aw = aw.slice(0, ax);
                                av[0] = av[0].slice(0, ax)
                            }
                            av[2] = aw
                        }
                    }
                    return av.slice(0, 3)
                }
            },
            filter: {
                ID: c ? function (av) {
                    av = av.replace(L, "");
                    return function (aw) {
                        return aw.getAttribute("id") === av
                    }
                } : function (av) {
                    av = av.replace(L, "");
                    return function (aw) {
                        var ax = typeof aw.getAttributeNode !== m && aw.getAttributeNode("id");
                        return ax && ax.value === av
                    }
                }, TAG: function (av) {
                    if (av === "*") {
                        return function () {
                            return true
                        }
                    }
                    av = av.replace(L, "").toLowerCase();
                    return function (aw) {
                        return aw.nodeName && aw.nodeName.toLowerCase() === av
                    }
                }, CLASS: function (aw) {
                    var av = v[aa][aw + " "];
                    return av || (av = new RegExp("(^|" + r + ")" + aw + "(" + r + "|$)")) && v(aw, function (ax) {
                            return av.test(ax.className || (typeof ax.getAttribute !== m && ax.getAttribute("class")) || "")
                        })
                }, ATTR: function (av, aw, ax) {
                    return function (az, ay) {
                        var aA = ap.attr(az, av);
                        if (aA == null) {
                            return aw === "!="
                        }
                        if (!aw) {
                            return true
                        }
                        aA += "";
                        return aw === "=" ? aA === ax : aw === "!=" ? aA !== ax : aw === "^=" ? ax && aA.indexOf(ax) === 0 : aw === "*=" ? ax && aA.indexOf(ax) > -1 : aw === "$=" ? ax && aA.substr(aA.length - ax.length) === ax : aw === "~=" ? (" " + aA + " ").indexOf(ax) > -1 : aw === "|=" ? aA === ax || aA.substr(0, ax.length + 1) === ax + "-" : false
                    }
                }, CHILD: function (ax, ay, av, aw) {
                    if (ax === "nth") {
                        return function (aA) {
                            var aB, az, aC = aA.parentNode;
                            if (av === 1 && aw === 0) {
                                return true
                            }
                            if (aC) {
                                az = 0;
                                for (aB = aC.firstChild; aB; aB = aB.nextSibling) {
                                    if (aB.nodeType === 1) {
                                        az++;
                                        if (aA === aB) {
                                            break
                                        }
                                    }
                                }
                            }
                            az -= aw;
                            return az === av || (az % av === 0 && az / av >= 0)
                        }
                    }
                    return function (az) {
                        var aA = az;
                        switch (ax) {
                            case"only":
                            case"first":
                                while ((aA = aA.previousSibling)) {
                                    if (aA.nodeType === 1) {
                                        return false
                                    }
                                }
                                if (ax === "first") {
                                    return true
                                }
                                aA = az;
                            case"last":
                                while ((aA = aA.nextSibling)) {
                                    if (aA.nodeType === 1) {
                                        return false
                                    }
                                }
                                return true
                        }
                    }
                }, PSEUDO: function (ax, av) {
                    var ay, aw = ac.pseudos[ax] || ac.setFilters[ax.toLowerCase()] || ap.error("unsupported pseudo: " + ax);
                    if (aw[aa]) {
                        return aw(av)
                    }
                    if (aw.length > 1) {
                        ay = [ax, ax, "", av];
                        return ac.setFilters.hasOwnProperty(ax.toLowerCase()) ? ao(function (aD, aC) {
                            var aA, aB = aw(aD, av), az = aB.length;
                            while (az--) {
                                aA = ak.call(aD, aB[az]);
                                aD[aA] = !(aC[aA] = aB[az])
                            }
                        }) : function (az) {
                            return aw(az, 0, ay)
                        }
                    }
                    return aw
                }
            },
            pseudos: {
                not: ao(function (ax) {
                    var ay = [], aw = [], av = x(ax.replace(af, "$1"));
                    return av[aa] ? ao(function (aD, aC, az, aF) {
                        var aA, aE = av(aD, null, aF, []), aB = aD.length;
                        while (aB--) {
                            if ((aA = aE[aB])) {
                                aD[aB] = !(aC[aB] = aA)
                            }
                        }
                    }) : function (aA, az, aB) {
                        ay[0] = aA;
                        av(ay, null, aB, aw);
                        return !aw.pop()
                    }
                }),
                has: ao(function (av) {
                    return function (aw) {
                        return ap(av, aw).length > 0
                    }
                }),
                contains: ao(function (av) {
                    return function (aw) {
                        return (aw.textContent || aw.innerText || ae(aw)).indexOf(av) > -1
                    }
                }),
                enabled: function (av) {
                    return av.disabled === false
                },
                disabled: function (av) {
                    return av.disabled === true
                },
                checked: function (aw) {
                    var av = aw.nodeName.toLowerCase();
                    return (av === "input" && !!aw.checked) || (av === "option" && !!aw.selected)
                },
                selected: function (av) {
                    if (av.parentNode) {
                        av.parentNode.selectedIndex
                    }
                    return av.selected === true
                },
                parent: function (av) {
                    return !ac.pseudos.empty(av)
                },
                empty: function (aw) {
                    var av;
                    aw = aw.firstChild;
                    while (aw) {
                        if (aw.nodeName > "@" || (av = aw.nodeType) === 3 || av === 4) {
                            return false
                        }
                        aw = aw.nextSibling
                    }
                    return true
                },
                header: function (av) {
                    return T.test(av.nodeName)
                },
                text: function (av) {
                    var aw, ax;
                    return av.nodeName.toLowerCase() === "input" && (aw = av.type) === "text" && ((ax = av.getAttribute("type")) == null || ax.toLowerCase() === aw)
                },
                radio: K("radio"),
                checkbox: K("checkbox"),
                file: K("file"),
                password: K("password"),
                image: K("image"),
                submit: F("submit"),
                reset: F("reset"),
                button: function (aw) {
                    var av = aw.nodeName.toLowerCase();
                    return av === "input" && aw.type === "button" || av === "button"
                },
                input: function (av) {
                    return V.test(av.nodeName)
                },
                focus: function (av) {
                    var aw = av.ownerDocument;
                    return av === aw.activeElement && (!aw.hasFocus || aw.hasFocus()) && !!(av.type || av.href || ~av.tabIndex)
                },
                active: function (av) {
                    return av === av.ownerDocument.activeElement
                },
                first: M(function () {
                    return [0]
                }),
                last: M(function (av, aw) {
                    return [aw - 1]
                }),
                eq: M(function (aw, av, ax) {
                    return [ax < 0 ? ax + av : ax]
                }),
                even: M(function (aw, av) {
                    for (var ax = 0; ax < av; ax += 2) {
                        aw.push(ax)
                    }
                    return aw
                }),
                odd: M(function (aw, av) {
                    for (var ax = 1; ax < av; ax += 2) {
                        aw.push(ax)
                    }
                    return aw
                }),
                lt: M(function (ax, aw, ay) {
                    for (var av = ay < 0 ? ay + aw : ay; --av >= 0;) {
                        ax.push(av)
                    }
                    return ax
                }),
                gt: M(function (ax, aw, ay) {
                    for (var av = ay < 0 ? ay + aw : ay; ++av < aw;) {
                        ax.push(av)
                    }
                    return ax
                })
            }
        };
        function an(ay, av, ax) {
            if (ay === av) {
                return ax
            }
            var aw = ay.nextSibling;
            while (aw) {
                if (aw === av) {
                    return -1
                }
                aw = aw.nextSibling
            }
            return 1
        }

        au = Q.compareDocumentPosition ? function (aw, av) {
            if (aw === av) {
                ag = true;
                return 0
            }
            return (!aw.compareDocumentPosition || !av.compareDocumentPosition ? aw.compareDocumentPosition : aw.compareDocumentPosition(av) & 4) ? -1 : 1
        } : function (aE, ay) {
            if (aE === ay) {
                ag = true;
                return 0
            } else {
                if (aE.sourceIndex && ay.sourceIndex) {
                    return aE.sourceIndex - ay.sourceIndex
                }
            }
            var av, az, aw = [], aA = [], ax = aE.parentNode, aB = ay.parentNode, aC = ax;
            if (ax === aB) {
                return an(aE, ay)
            } else {
                if (!ax) {
                    return -1
                } else {
                    if (!aB) {
                        return 1
                    }
                }
            }
            while (aC) {
                aw.unshift(aC);
                aC = aC.parentNode
            }
            aC = aB;
            while (aC) {
                aA.unshift(aC);
                aC = aC.parentNode
            }
            av = aw.length;
            az = aA.length;
            for (var aD = 0; aD < av && aD < az; aD++) {
                if (aw[aD] !== aA[aD]) {
                    return an(aw[aD], aA[aD])
                }
            }
            return aD === av ? an(aE, aA[aD], -1) : an(aw[aD], ay, 1)
        };
        [0, 0].sort(au);
        j = !ag;
        ap.uniqueSort = function (ay) {
            var av, az = [], aw = 1, ax = 0;
            ag = j;
            ay.sort(au);
            if (ag) {
                for (; (av = ay[aw]); aw++) {
                    if (av === ay[aw - 1]) {
                        ax = az.push(aw)
                    }
                }
                while (ax--) {
                    ay.splice(az[ax], 1)
                }
            }
            return ay
        };
        ap.error = function (av) {
            throw new Error("Syntax error, unrecognized expression: " + av)
        };
        function p(aA, ay) {
            var ax, aw, aC, aD, aB, av, az, aE = o[aa][aA + " "];
            if (aE) {
                return ay ? 0 : aE.slice(0)
            }
            aB = aA;
            av = [];
            az = ac.preFilter;
            while (aB) {
                if (!ax || (aw = P.exec(aB))) {
                    if (aw) {
                        aB = aB.slice(aw[0].length) || aB
                    }
                    av.push(aC = [])
                }
                ax = false;
                if ((aw = N.exec(aB))) {
                    aC.push(ax = new n(aw.shift()));
                    aB = aB.slice(ax.length);
                    ax.type = aw[0].replace(af, " ")
                }
                for (aD in ac.filter) {
                    if ((aw = u[aD].exec(aB)) && (!az[aD] || (aw = az[aD](aw)))) {
                        aC.push(ax = new n(aw.shift()));
                        aB = aB.slice(ax.length);
                        ax.type = aD;
                        ax.matches = aw
                    }
                }
                if (!ax) {
                    break
                }
            }
            return ay ? aB.length : aB ? ap.error(aA) : o(aA, av).slice(0)
        }

        function l(az, aw, aA) {
            var ax = aw.dir, av = aA && aw.dir === "parentNode", ay = U++;
            return aw.first ? function (aC, aB, aD) {
                while ((aC = aC[ax])) {
                    if (av || aC.nodeType === 1) {
                        return az(aC, aB, aD)
                    }
                }
            } : function (aF, aD, aG) {
                if (!aG) {
                    var aB, aE = O + " " + ay + " ", aC = aE + k;
                    while ((aF = aF[ax])) {
                        if (av || aF.nodeType === 1) {
                            if ((aB = aF[aa]) === aC) {
                                return aF.sizset
                            } else {
                                if (typeof aB === "string" && aB.indexOf(aE) === 0) {
                                    if (aF.sizset) {
                                        return aF
                                    }
                                } else {
                                    aF[aa] = aC;
                                    if (az(aF, aD, aG)) {
                                        aF.sizset = true;
                                        return aF
                                    }
                                    aF.sizset = false
                                }
                            }
                        }
                    }
                } else {
                    while ((aF = aF[ax])) {
                        if (av || aF.nodeType === 1) {
                            if (az(aF, aD, aG)) {
                                return aF
                            }
                        }
                    }
                }
            }
        }

        function Y(av) {
            return av.length > 1 ? function (ax, aw, az) {
                var ay = av.length;
                while (ay--) {
                    if (!av[ay](ax, aw, az)) {
                        return false
                    }
                }
                return true
            } : av[0]
        }

        function B(aC, az, aw, aE, aD) {
            var av, aB = [], ax = 0, ay = aC.length, aA = az != null;
            for (; ax < ay; ax++) {
                if ((av = aC[ax])) {
                    if (!aw || aw(av, aE, aD)) {
                        aB.push(av);
                        if (aA) {
                            az.push(ax)
                        }
                    }
                }
            }
            return aB
        }

        function al(ay, az, aA, av, aw, ax) {
            if (av && !av[aa]) {
                av = al(av)
            }
            if (aw && !aw[aa]) {
                aw = al(aw, ax)
            }
            return ao(function (aL, aK, aB, aN) {
                var aM, aE, aC, aJ = [], aH = [], aI = aK.length, aD = aL || w(az || "*", aB.nodeType ? [aB] : aB, []), aF = ay && (aL || !az) ? B(aD, aJ, ay, aB, aN) : aD, aG = aA ? aw || (aL ? ay : aI || av) ? [] : aK : aF;
                if (aA) {
                    aA(aF, aG, aB, aN)
                }
                if (av) {
                    aM = B(aG, aH);
                    av(aM, [], aB, aN);
                    aE = aM.length;
                    while (aE--) {
                        if ((aC = aM[aE])) {
                            aG[aH[aE]] = !(aF[aH[aE]] = aC)
                        }
                    }
                }
                if (aL) {
                    if (aw || ay) {
                        if (aw) {
                            aM = [];
                            aE = aG.length;
                            while (aE--) {
                                if ((aC = aG[aE])) {
                                    aM.push((aF[aE] = aC))
                                }
                            }
                            aw(null, (aG = []), aM, aN)
                        }
                        aE = aG.length;
                        while (aE--) {
                            if ((aC = aG[aE]) && (aM = aw ? ak.call(aL, aC) : aJ[aE]) > -1) {
                                aL[aM] = !(aK[aM] = aC)
                            }
                        }
                    }
                } else {
                    aG = B(aG === aK ? aG.splice(aI, aG.length) : aG);
                    if (aw) {
                        aw(null, aK, aG, aN)
                    } else {
                        J.apply(aK, aG)
                    }
                }
            })
        }

        function at(aE) {
            var aF, aC, ax, az = aE.length, ay = ac.relative[aE[0].type], aw = ay || ac.relative[" "], av = ay ? 1 : 0, aB = l(function (aG) {
                return aG === aF
            }, aw, true), aA = l(function (aG) {
                return ak.call(aF, aG) > -1
            }, aw, true), aD = [function (aH, aG, aI) {
                return (!ay && (aI || aG !== A)) || ((aF = aG).nodeType ? aB(aH, aG, aI) : aA(aH, aG, aI))
            }];
            for (; av < az; av++) {
                if ((aC = ac.relative[aE[av].type])) {
                    aD = [l(Y(aD), aC)]
                } else {
                    aC = ac.filter[aE[av].type].apply(null, aE[av].matches);
                    if (aC[aa]) {
                        ax = ++av;
                        for (; ax < az; ax++) {
                            if (ac.relative[aE[ax].type]) {
                                break
                            }
                        }
                        return al(av > 1 && Y(aD), av > 1 && aE.slice(0, av - 1).join("").replace(af, "$1"), aC, av < ax && at(aE.slice(av, ax)), ax < az && at((aE = aE.slice(ax))), ax < az && aE.join(""))
                    }
                    aD.push(aC)
                }
            }
            return Y(aD)
        }

        function aq(aw, ax) {
            var av = ax.length > 0, az = aw.length > 0, ay = function (aM, aA, aP, aL, aF) {
                var aD, aH, aJ, aN = [], aI = 0, aG = "0", aO = aM && [], aK = aF != null, aB = A, aE = aM || az && ac.find.TAG("*", aF && aA.parentNode || aA), aC = (O += aB == null ? 1 : Math.E);
                if (aK) {
                    A = aA !== S && aA;
                    k = ay.el
                }
                for (; (aD = aE[aG]) != null; aG++) {
                    if (az && aD) {
                        for (aH = 0; (aJ = aw[aH]); aH++) {
                            if (aJ(aD, aA, aP)) {
                                aL.push(aD);
                                break
                            }
                        }
                        if (aK) {
                            O = aC;
                            k = ++ay.el
                        }
                    }
                    if (av) {
                        if ((aD = !aJ && aD)) {
                            aI--
                        }
                        if (aM) {
                            aO.push(aD)
                        }
                    }
                }
                aI += aG;
                if (av && aG !== aI) {
                    for (aH = 0; (aJ = ax[aH]); aH++) {
                        aJ(aO, aN, aA, aP)
                    }
                    if (aM) {
                        if (aI > 0) {
                            while (aG--) {
                                if (!(aO[aG] || aN[aG])) {
                                    aN[aG] = C.call(aL)
                                }
                            }
                        }
                        aN = B(aN)
                    }
                    J.apply(aL, aN);
                    if (aK && !aM && aN.length > 0 && (aI + ax.length) > 1) {
                        ap.uniqueSort(aL)
                    }
                }
                if (aK) {
                    O = aC;
                    A = aB
                }
                return aO
            };
            ay.el = 0;
            return av ? ao(ay) : ay
        }

        x = ap.compile = function (ay, aw) {
            var ax, az = [], av = [], aA = z[aa][ay + " "];
            if (!aA) {
                if (!aw) {
                    aw = p(ay)
                }
                ax = aw.length;
                while (ax--) {
                    aA = at(aw[ax]);
                    if (aA[aa]) {
                        az.push(aA)
                    } else {
                        av.push(aA)
                    }
                }
                aA = z(ay, aq(av, az))
            }
            return aA
        };
        function w(ay, az, ax) {
            var av = 0, aw = az.length;
            for (; av < aw; av++) {
                ap(ay, az[av], ax)
            }
            return ax
        }

        function ah(aB, aG, az, aA, aF) {
            var aw, aD, aC, aE, av, ay = p(aB), ax = ay.length;
            if (!aA) {
                if (ay.length === 1) {
                    aD = ay[0] = ay[0].slice(0);
                    if (aD.length > 2 && (aC = aD[0]).type === "ID" && aG.nodeType === 9 && !aF && ac.relative[aD[1].type]) {
                        aG = ac.find.ID(aC.matches[0].replace(L, ""), aG, aF)[0];
                        if (!aG) {
                            return az
                        }
                        aB = aB.slice(aD.shift().length)
                    }
                    for (aw = u.POS.test(aB) ? -1 : aD.length - 1; aw >= 0; aw--) {
                        aC = aD[aw];
                        if (ac.relative[(aE = aC.type)]) {
                            break
                        }
                        if ((av = ac.find[aE])) {
                            if ((aA = av(aC.matches[0].replace(L, ""), ad.test(aD[0].type) && aG.parentNode || aG, aF))) {
                                aD.splice(aw, 1);
                                aB = aA.length && aD.join("");
                                if (!aB) {
                                    J.apply(az, ar.call(aA, 0));
                                    return az
                                }
                                break
                            }
                        }
                    }
                }
            }
            x(aB, ay)(aA, aG, aF, az, ad.test(aB));
            return az
        }

        if (S.querySelectorAll) {
            (function () {
                var aB, aw = ah, aA = /'|\\/g, ax = /\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g, az = [":focus"], ay = [":active"], av = Q.matchesSelector || Q.mozMatchesSelector || Q.webkitMatchesSelector || Q.oMatchesSelector || Q.msMatchesSelector;
                a(function (aC) {
                    aC.innerHTML = "<select><option selected=''></option></select>";
                    if (!aC.querySelectorAll("[selected]").length) {
                        az.push("\\[" + r + "*(?:checked|disabled|ismap|multiple|readonly|selected|value)")
                    }
                    if (!aC.querySelectorAll(":checked").length) {
                        az.push(":checked")
                    }
                });
                a(function (aC) {
                    aC.innerHTML = "<p test=''></p>";
                    if (aC.querySelectorAll("[test^='']").length) {
                        az.push("[*^$]=" + r + "*(?:\"\"|'')")
                    }
                    aC.innerHTML = "<input type='hidden'/>";
                    if (!aC.querySelectorAll(":enabled").length) {
                        az.push(":enabled", ":disabled")
                    }
                });
                az = new RegExp(az.join("|"));
                ah = function (aM, aC, aK, aL, aN) {
                    if (!aL && !aN && !az.test(aM)) {
                        var aD, aE, aI = true, aH = aa, aF = aC, aG = aC.nodeType === 9 && aM;
                        if (aC.nodeType === 1 && aC.nodeName.toLowerCase() !== "object") {
                            aD = p(aM);
                            if ((aI = aC.getAttribute("id"))) {
                                aH = aI.replace(aA, "\\$&")
                            } else {
                                aC.setAttribute("id", aH)
                            }
                            aH = "[id='" + aH + "'] ";
                            aE = aD.length;
                            while (aE--) {
                                aD[aE] = aH + aD[aE].join("")
                            }
                            aF = ad.test(aM) && aC.parentNode || aC;
                            aG = aD.join(",")
                        }
                        if (aG) {
                            try {
                                J.apply(aK, ar.call(aF.querySelectorAll(aG), 0));
                                return aK
                            } catch (aJ) {
                            } finally {
                                if (!aI) {
                                    aC.removeAttribute("id")
                                }
                            }
                        }
                    }
                    return aw(aM, aC, aK, aL, aN)
                };
                if (av) {
                    a(function (aC) {
                        aB = av.call(aC, "div");
                        try {
                            av.call(aC, "[test!='']:sizzle");
                            ay.push("!=", G)
                        } catch (aD) {
                        }
                    });
                    ay = new RegExp(ay.join("|"));
                    ap.matchesSelector = function (aD, aE) {
                        aE = aE.replace(ax, "='$1']");
                        if (!am(aD) && !ay.test(aE) && !az.test(aE)) {
                            try {
                                var aF = av.call(aD, aE);
                                if (aF || aB || aD.document && aD.document.nodeType !== 11) {
                                    return aF
                                }
                            } catch (aC) {
                            }
                        }
                        return ap(aE, null, null, [aD]).length > 0
                    }
                }
            })()
        }
        ac.pseudos.nth = ac.pseudos.eq;
        function aj() {
        }

        ac.filters = aj.prototype = ac.pseudos;
        ac.setFilters = new aj();
        ap.attr = cE.attr;
        cE.find = ap;
        cE.expr = ap.selectors;
        cE.expr[":"] = cE.expr.pseudos;
        cE.unique = ap.uniqueSort;
        cE.text = ap.getText;
        cE.isXMLDoc = ap.isXML;
        cE.contains = ap.contains
    })(dF);
    var c6 = /Until$/, dh = /^(?:parents|prev(?:Until|All))/, cC = /^.[^:#\[\.,]*$/, b9 = cE.expr.match.needsContext, cm = {
        children: true,
        contents: true,
        next: true,
        prev: true
    };
    cE.fn.extend({
        find: function (f) {
            var i, h, a, b, c, d, g = this;
            if (typeof f !== "string") {
                return cE(f).filter(function () {
                    for (i = 0, h = g.length; i < h; i++) {
                        if (cE.contains(g[i], this)) {
                            return true
                        }
                    }
                })
            }
            d = this.pushStack("", "find", f);
            for (i = 0, h = this.length; i < h; i++) {
                a = d.length;
                cE.find(f, this[i], d);
                if (i > 0) {
                    for (b = a; b < d.length; b++) {
                        for (c = 0; c < a; c++) {
                            if (d[c] === d[b]) {
                                d.splice(b--, 1);
                                break
                            }
                        }
                    }
                }
            }
            return d
        }, has: function (a) {
            var d, b = cE(a, this), c = b.length;
            return this.filter(function () {
                for (d = 0; d < c; d++) {
                    if (cE.contains(this, b[d])) {
                        return true
                    }
                }
            })
        }, not: function (a) {
            return this.pushStack(dH(this, a, false), "not", a)
        }, filter: function (a) {
            return this.pushStack(dH(this, a, true), "filter", a)
        }, is: function (a) {
            return !!a && (typeof a === "string" ? b9.test(a) ? cE(a, this.context).index(this[0]) >= 0 : cE.filter(a, this).length > 0 : this.filter(a).length > 0)
        }, closest: function (f, h) {
            var g, a = 0, b = this.length, d = [], c = b9.test(f) || typeof f !== "string" ? cE(f, h || this.context) : 0;
            for (; a < b; a++) {
                g = this[a];
                while (g && g.ownerDocument && g !== h && g.nodeType !== 11) {
                    if (c ? c.index(g) > -1 : cE.find.matchesSelector(g, f)) {
                        d.push(g);
                        break
                    }
                    g = g.parentNode
                }
            }
            d = d.length > 1 ? cE.unique(d) : d;
            return this.pushStack(d, "closest", f)
        }, index: function (a) {
            if (!a) {
                return (this[0] && this[0].parentNode) ? this.prevAll().length : -1
            }
            if (typeof a === "string") {
                return cE.inArray(this[0], cE(a))
            }
            return cE.inArray(a.jquery ? a[0] : a, this)
        }, add: function (a, c) {
            var b = typeof a === "string" ? cE(a, c) : cE.makeArray(a && a.nodeType ? [a] : a), d = cE.merge(this.get(), b);
            return this.pushStack(cw(b[0]) || cw(d[0]) ? d : cE.unique(d))
        }, addBack: function (a) {
            return this.add(a == null ? this.prevObject : this.prevObject.filter(a))
        }
    });
    cE.fn.andSelf = cE.fn.addBack;
    function cw(a) {
        return !a || !a.parentNode || a.parentNode.nodeType === 11
    }

    function dr(b, a) {
        do {
            b = b[a]
        } while (b && b.nodeType !== 1);
        return b
    }

    cE.each({
        parent: function (b) {
            var a = b.parentNode;
            return a && a.nodeType !== 11 ? a : null
        }, parents: function (a) {
            return cE.dir(a, "parentNode")
        }, parentsUntil: function (c, b, a) {
            return cE.dir(c, "parentNode", a)
        }, next: function (a) {
            return dr(a, "nextSibling")
        }, prev: function (a) {
            return dr(a, "previousSibling")
        }, nextAll: function (a) {
            return cE.dir(a, "nextSibling")
        }, prevAll: function (a) {
            return cE.dir(a, "previousSibling")
        }, nextUntil: function (c, b, a) {
            return cE.dir(c, "nextSibling", a)
        }, prevUntil: function (c, b, a) {
            return cE.dir(c, "previousSibling", a)
        }, siblings: function (a) {
            return cE.sibling((a.parentNode || {}).firstChild, a)
        }, children: function (a) {
            return cE.sibling(a.firstChild)
        }, contents: function (a) {
            return cE.nodeName(a, "iframe") ? a.contentDocument || a.contentWindow.document : cE.merge([], a.childNodes)
        }
    }, function (a, b) {
        cE.fn[a] = function (f, d) {
            var c = cE.map(this, b, f);
            if (!c6.test(a)) {
                d = f
            }
            if (d && typeof d === "string") {
                c = cE.filter(d, c)
            }
            c = this.length > 1 && !cm[a] ? cE.unique(c) : c;
            if (this.length > 1 && dh.test(a)) {
                c = c.reverse()
            }
            return this.pushStack(c, a, eA.call(arguments).join(","))
        }
    });
    cE.extend({
        filter: function (b, c, a) {
            if (a) {
                b = ":not(" + b + ")"
            }
            return c.length === 1 ? cE.find.matchesSelector(c[0], b) ? [c[0]] : [] : cE.find.matches(b, c)
        }, dir: function (a, d, c) {
            var b = [], f = a[d];
            while (f && f.nodeType !== 9 && (c === dB || f.nodeType !== 1 || !cE(f).is(c))) {
                if (f.nodeType === 1) {
                    b.push(f)
                }
                f = f[d]
            }
            return b
        }, sibling: function (b, c) {
            var a = [];
            for (; b; b = b.nextSibling) {
                if (b.nodeType === 1 && b !== c) {
                    a.push(b)
                }
            }
            return a
        }
    });
    function dH(d, b, a) {
        b = b || 0;
        if (cE.isFunction(b)) {
            return cE.grep(d, function (f, g) {
                var h = !!b.call(f, g, f);
                return h === a
            })
        } else {
            if (b.nodeType) {
                return cE.grep(d, function (f, g) {
                    return (f === b) === a
                })
            } else {
                if (typeof b === "string") {
                    var c = cE.grep(d, function (f) {
                        return f.nodeType === 1
                    });
                    if (cC.test(b)) {
                        return cE.filter(b, c, !a)
                    } else {
                        b = cE.filter(b, c)
                    }
                }
            }
        }
        return cE.grep(d, function (f, g) {
            return (cE.inArray(f, b) >= 0) === a
        })
    }

    function dX(c) {
        var b = cN.split("|"), a = c.createDocumentFragment();
        if (a.createElement) {
            while (b.length) {
                a.createElement(b.pop())
            }
        }
        return a
    }

    var cN = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video", cY = / jQuery\d+="(?:null|\d+)"/g, b3 = /^\s+/, di = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, dK = /<([\w:]+)/, dM = /<tbody/i, cW = /<|&#?\w+;/, c5 = /<(?:script|style|link)/i, c1 = /<(?:script|object|embed|option|style)/i, c7 = new RegExp("<(?:" + cN + ")[\\s/>]", "i"), cl = /^(?:checkbox|radio)$/, cn = /checked\s*(?:[^=]|=\s*.checked.)/i, dE = /\/(java|ecma)script/i, cr = /^\s*<!(?:\[CDATA\[|\-\-)|[\]\-]{2}>\s*$/g, dJ = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        legend: [1, "<fieldset>", "</fieldset>"],
        thead: [1, "<table>", "</table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
        area: [1, "<map>", "</map>"],
        _default: [0, "", ""]
    }, dk = dX(ej), eB = dk.appendChild(ej.createElement("div"));
    dJ.optgroup = dJ.option;
    dJ.tbody = dJ.tfoot = dJ.colgroup = dJ.caption = dJ.thead;
    dJ.th = dJ.td;
    if (!cE.support.htmlSerialize) {
        dJ._default = [1, "X<div>", "</div>"]
    }
    cE.fn.extend({
        text: function (a) {
            return cE.access(this, function (b) {
                return b === dB ? cE.text(this) : this.empty().append((this[0] && this[0].ownerDocument || ej).createTextNode(b))
            }, null, a, arguments.length)
        }, wrapAll: function (b) {
            if (cE.isFunction(b)) {
                return this.each(function (c) {
                    cE(this).wrapAll(b.call(this, c))
                })
            }
            if (this[0]) {
                var a = cE(b, this[0].ownerDocument).eq(0).clone(true);
                if (this[0].parentNode) {
                    a.insertBefore(this[0])
                }
                a.map(function () {
                    var c = this;
                    while (c.firstChild && c.firstChild.nodeType === 1) {
                        c = c.firstChild
                    }
                    return c
                }).append(this)
            }
            return this
        }, wrapInner: function (a) {
            if (cE.isFunction(a)) {
                return this.each(function (b) {
                    cE(this).wrapInner(a.call(this, b))
                })
            }
            return this.each(function () {
                var b = cE(this), c = b.contents();
                if (c.length) {
                    c.wrapAll(a)
                } else {
                    b.append(a)
                }
            })
        }, wrap: function (b) {
            var a = cE.isFunction(b);
            return this.each(function (c) {
                cE(this).wrapAll(a ? b.call(this, c) : b)
            })
        }, unwrap: function () {
            return this.parent().each(function () {
                if (!cE.nodeName(this, "body")) {
                    cE(this).replaceWith(this.childNodes)
                }
            }).end()
        }, append: function () {
            return this.domManip(arguments, true, function (a) {
                if (this.nodeType === 1 || this.nodeType === 11) {
                    this.appendChild(a)
                }
            })
        }, prepend: function () {
            return this.domManip(arguments, true, function (a) {
                if (this.nodeType === 1 || this.nodeType === 11) {
                    this.insertBefore(a, this.firstChild)
                }
            })
        }, before: function () {
            if (!cw(this[0])) {
                return this.domManip(arguments, false, function (b) {
                    this.parentNode.insertBefore(b, this)
                })
            }
            if (arguments.length) {
                var a = cE.clean(arguments);
                return this.pushStack(cE.merge(a, this), "before", this.selector)
            }
        }, after: function () {
            if (!cw(this[0])) {
                return this.domManip(arguments, false, function (b) {
                    this.parentNode.insertBefore(b, this.nextSibling)
                })
            }
            if (arguments.length) {
                var a = cE.clean(arguments);
                return this.pushStack(cE.merge(this, a), "after", this.selector)
            }
        }, remove: function (b, a) {
            var d, c = 0;
            for (; (d = this[c]) != null; c++) {
                if (!b || cE.filter(b, [d]).length) {
                    if (!a && d.nodeType === 1) {
                        cE.cleanData(d.getElementsByTagName("*"));
                        cE.cleanData([d])
                    }
                    if (d.parentNode) {
                        d.parentNode.removeChild(d)
                    }
                }
            }
            return this
        }, empty: function () {
            var b, a = 0;
            for (; (b = this[a]) != null; a++) {
                if (b.nodeType === 1) {
                    cE.cleanData(b.getElementsByTagName("*"))
                }
                while (b.firstChild) {
                    b.removeChild(b.firstChild)
                }
            }
            return this
        }, clone: function (b, a) {
            b = b == null ? false : b;
            a = a == null ? b : a;
            return this.map(function () {
                return cE.clone(this, b, a)
            })
        }, html: function (a) {
            return cE.access(this, function (f) {
                var b = this[0] || {}, c = 0, d = this.length;
                if (f === dB) {
                    return b.nodeType === 1 ? b.innerHTML.replace(cY, "") : dB
                }
                if (typeof f === "string" && !c5.test(f) && (cE.support.htmlSerialize || !c7.test(f)) && (cE.support.leadingWhitespace || !b3.test(f)) && !dJ[(dK.exec(f) || ["", ""])[1].toLowerCase()]) {
                    f = f.replace(di, "<$1></$2>");
                    try {
                        for (; c < d; c++) {
                            b = this[c] || {};
                            if (b.nodeType === 1) {
                                cE.cleanData(b.getElementsByTagName("*"));
                                b.innerHTML = f
                            }
                        }
                        b = 0
                    } catch (g) {
                    }
                }
                if (b) {
                    this.empty().append(f)
                }
            }, null, a, arguments.length)
        }, replaceWith: function (a) {
            if (!cw(this[0])) {
                if (cE.isFunction(a)) {
                    return this.each(function (d) {
                        var c = cE(this), b = c.html();
                        c.replaceWith(a.call(this, d, b))
                    })
                }
                if (typeof a !== "string") {
                    a = cE(a).detach()
                }
                return this.each(function () {
                    var c = this.nextSibling, b = this.parentNode;
                    cE(this).remove();
                    if (c) {
                        cE(c).before(a)
                    } else {
                        cE(b).append(a)
                    }
                })
            }
            return this.length ? this.pushStack(cE(cE.isFunction(a) ? a() : a), "replaceWith", a) : this
        }, detach: function (a) {
            return this.remove(a, true)
        }, domManip: function (l, i, k) {
            l = [].concat.apply([], l);
            var g, a, b, d, c = 0, j = l[0], h = [], f = this.length;
            if (!cE.support.checkClone && f > 1 && typeof j === "string" && cn.test(j)) {
                return this.each(function () {
                    cE(this).domManip(l, i, k)
                })
            }
            if (cE.isFunction(j)) {
                return this.each(function (m) {
                    var n = cE(this);
                    l[0] = j.call(this, m, i ? n.html() : dB);
                    n.domManip(l, i, k)
                })
            }
            if (this[0]) {
                g = cE.buildFragment(l, this, h);
                b = g.fragment;
                a = b.firstChild;
                if (b.childNodes.length === 1) {
                    b = a
                }
                if (a) {
                    i = i && cE.nodeName(a, "tr");
                    for (d = g.cacheable || f - 1; c < f; c++) {
                        k.call(i && cE.nodeName(this[c], "table") ? ev(this[c], "tbody") : this[c], c === d ? b : cE.clone(b, true, true))
                    }
                }
                b = a = null;
                if (h.length) {
                    cE.each(h, function (n, m) {
                        if (m.src) {
                            if (cE.ajax) {
                                cE.ajax({
                                    url: m.src,
                                    type: "GET",
                                    dataType: "script",
                                    async: false,
                                    global: false,
                                    "throws": true
                                })
                            } else {
                                cE.error("no ajax")
                            }
                        } else {
                            cE.globalEval((m.text || m.textContent || m.innerHTML || "").replace(cr, ""))
                        }
                        if (m.parentNode) {
                            m.parentNode.removeChild(m)
                        }
                    })
                }
            }
            return this
        }
    });
    function ev(b, a) {
        return b.getElementsByTagName(a)[0] || b.appendChild(b.ownerDocument.createElement(a))
    }

    function ek(f, h) {
        if (h.nodeType !== 1 || !cE.hasData(f)) {
            return
        }
        var g, b, c, d = cE._data(f), i = cE._data(h, d), a = d.events;
        if (a) {
            delete i.handle;
            i.events = {};
            for (g in a) {
                for (b = 0, c = a[g].length; b < c; b++) {
                    cE.event.add(h, g, a[g][b])
                }
            }
        }
        if (i.data) {
            i.data = cE.extend({}, i.data)
        }
    }

    function em(a, c) {
        var b;
        if (c.nodeType !== 1) {
            return
        }
        if (c.clearAttributes) {
            c.clearAttributes()
        }
        if (c.mergeAttributes) {
            c.mergeAttributes(a)
        }
        b = c.nodeName.toLowerCase();
        if (b === "object") {
            if (c.parentNode) {
                c.outerHTML = a.outerHTML
            }
            if (cE.support.html5Clone && (a.innerHTML && !cE.trim(c.innerHTML))) {
                c.innerHTML = a.innerHTML
            }
        } else {
            if (b === "input" && cl.test(a.type)) {
                c.defaultChecked = c.checked = a.checked;
                if (c.value !== a.value) {
                    c.value = a.value
                }
            } else {
                if (b === "option") {
                    c.selected = a.defaultSelected
                } else {
                    if (b === "input" || b === "textarea") {
                        c.defaultValue = a.defaultValue
                    } else {
                        if (b === "script" && c.text !== a.text) {
                            c.text = a.text
                        }
                    }
                }
            }
        }
        c.removeAttribute(cE.expando)
    }

    cE.buildFragment = function (h, b, f) {
        var d, g, a, c = h[0];
        b = b || ej;
        b = !b.nodeType && b[0] || b;
        b = b.ownerDocument || b;
        if (h.length === 1 && typeof c === "string" && c.length < 512 && b === ej && c.charAt(0) === "<" && !c1.test(c) && (cE.support.checkClone || !cn.test(c)) && (cE.support.html5Clone || !c7.test(c))) {
            g = true;
            d = cE.fragments[c];
            a = d !== dB
        }
        if (!d) {
            d = b.createDocumentFragment();
            cE.clean(h, b, d, f);
            if (g) {
                cE.fragments[c] = a && d
            }
        }
        return {fragment: d, cacheable: g}
    };
    cE.fragments = {};
    cE.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (b, a) {
        cE.fn[b] = function (j) {
            var c, d = 0, i = [], f = cE(j), g = f.length, h = this.length === 1 && this[0].parentNode;
            if ((h == null || h && h.nodeType === 11 && h.childNodes.length === 1) && g === 1) {
                f[a](this[0]);
                return this
            } else {
                for (; d < g; d++) {
                    c = (d > 0 ? this.clone(true) : this).get();
                    cE(f[d])[a](c);
                    i = i.concat(c)
                }
                return this.pushStack(i, b, f.selector)
            }
        }
    });
    function ce(a) {
        if (typeof a.getElementsByTagName !== "undefined") {
            return a.getElementsByTagName("*")
        } else {
            if (typeof a.querySelectorAll !== "undefined") {
                return a.querySelectorAll("*")
            } else {
                return []
            }
        }
    }

    function ex(a) {
        if (cl.test(a.type)) {
            a.defaultChecked = a.checked
        }
    }

    cE.extend({
        clone: function (c, g, a) {
            var f, b, d, h;
            if (cE.support.html5Clone || cE.isXMLDoc(c) || !c7.test("<" + c.nodeName + ">")) {
                h = c.cloneNode(true)
            } else {
                eB.innerHTML = c.outerHTML;
                eB.removeChild(h = eB.firstChild)
            }
            if ((!cE.support.noCloneEvent || !cE.support.noCloneChecked) && (c.nodeType === 1 || c.nodeType === 11) && !cE.isXMLDoc(c)) {
                em(c, h);
                f = ce(c);
                b = ce(h);
                for (d = 0; f[d]; ++d) {
                    if (b[d]) {
                        em(f[d], b[d])
                    }
                }
            }
            if (g) {
                ek(c, h);
                if (a) {
                    f = ce(c);
                    b = ce(h);
                    for (d = 0; f[d]; ++d) {
                        ek(f[d], b[d])
                    }
                }
            }
            f = b = null;
            return h
        }, clean: function (c, s, d, o) {
            var h, i, b, p, r, l, a, g, q, k, f, j, n = s === ej && dk, m = [];
            if (!s || typeof s.createDocumentFragment === "undefined") {
                s = ej
            }
            for (h = 0; (b = c[h]) != null; h++) {
                if (typeof b === "number") {
                    b += ""
                }
                if (!b) {
                    continue
                }
                if (typeof b === "string") {
                    if (!cW.test(b)) {
                        b = s.createTextNode(b)
                    } else {
                        n = n || dX(s);
                        a = s.createElement("div");
                        n.appendChild(a);
                        b = b.replace(di, "<$1></$2>");
                        p = (dK.exec(b) || ["", ""])[1].toLowerCase();
                        r = dJ[p] || dJ._default;
                        l = r[0];
                        a.innerHTML = r[1] + b + r[2];
                        while (l--) {
                            a = a.lastChild
                        }
                        if (!cE.support.tbody) {
                            g = dM.test(b);
                            q = p === "table" && !g ? a.firstChild && a.firstChild.childNodes : r[1] === "<table>" && !g ? a.childNodes : [];
                            for (i = q.length - 1; i >= 0; --i) {
                                if (cE.nodeName(q[i], "tbody") && !q[i].childNodes.length) {
                                    q[i].parentNode.removeChild(q[i])
                                }
                            }
                        }
                        if (!cE.support.leadingWhitespace && b3.test(b)) {
                            a.insertBefore(s.createTextNode(b3.exec(b)[0]), a.firstChild)
                        }
                        b = a.childNodes;
                        a.parentNode.removeChild(a)
                    }
                }
                if (b.nodeType) {
                    m.push(b)
                } else {
                    cE.merge(m, b)
                }
            }
            if (a) {
                b = a = n = null
            }
            if (!cE.support.appendChecked) {
                for (h = 0; (b = m[h]) != null; h++) {
                    if (cE.nodeName(b, "input")) {
                        ex(b)
                    } else {
                        if (typeof b.getElementsByTagName !== "undefined") {
                            cE.grep(b.getElementsByTagName("input"), ex)
                        }
                    }
                }
            }
            if (d) {
                f = function (t) {
                    if (!t.type || dE.test(t.type)) {
                        return o ? o.push(t.parentNode ? t.parentNode.removeChild(t) : t) : d.appendChild(t)
                    }
                };
                for (h = 0; (b = m[h]) != null; h++) {
                    if (!(cE.nodeName(b, "script") && f(b))) {
                        d.appendChild(b);
                        if (typeof b.getElementsByTagName !== "undefined") {
                            j = cE.grep(cE.merge([], b.getElementsByTagName("script")), f);
                            m.splice.apply(m, [h + 1, 0].concat(j));
                            h += j.length
                        }
                    }
                }
            }
            return m
        }, cleanData: function (d, l) {
            var a, g, c, j, f = 0, h = cE.expando, k = cE.cache, b = cE.support.deleteExpando, i = cE.event.special;
            for (; (c = d[f]) != null; f++) {
                if (l || cE.acceptData(c)) {
                    g = c[h];
                    a = g && k[g];
                    if (a) {
                        if (a.events) {
                            for (j in a.events) {
                                if (i[j]) {
                                    cE.event.remove(c, j)
                                } else {
                                    cE.removeEvent(c, j, a.handle)
                                }
                            }
                        }
                        if (k[g]) {
                            delete k[g];
                            if (b) {
                                delete c[h]
                            } else {
                                if (c.removeAttribute) {
                                    c.removeAttribute(h)
                                } else {
                                    c[h] = null
                                }
                            }
                            cE.deletedIds.push(g)
                        }
                    }
                }
            }
        }
    });
    (function () {
        var a, b;
        cE.uaMatch = function (d) {
            d = d.toLowerCase();
            var c = /(chrome)[ \/]([\w.]+)/.exec(d) || /(webkit)[ \/]([\w.]+)/.exec(d) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(d) || /(msie) ([\w.]+)/.exec(d) || d.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(d) || [];
            return {browser: c[1] || "", version: c[2] || "0"}
        };
        a = cE.uaMatch(cI.userAgent);
        b = {};
        if (a.browser) {
            b[a.browser] = true;
            b.version = a.version
        }
        if (b.chrome) {
            b.webkit = true
        } else {
            if (b.webkit) {
                b.safari = true
            }
        }
        cE.browser = b;
        cE.sub = function () {
            function d(h, g) {
                return new d.fn.init(h, g)
            }

            cE.extend(true, d, this);
            d.superclass = this;
            d.fn = d.prototype = this();
            d.fn.constructor = d;
            d.sub = this.sub;
            d.fn.init = function c(h, g) {
                if (g && g instanceof cE && !(g instanceof d)) {
                    g = d(g)
                }
                return cE.fn.init.call(this, h, g, f)
            };
            d.fn.init.prototype = d.fn;
            var f = d(ej);
            return d
        }
    })();
    var ed, cq, cs, cd = /alpha\([^)]*\)/i, df = /opacity=([^)]*)/, dj = /^(top|right|bottom|left)$/, cz = /^(none|table(?!-c[ea]).+)/, b5 = /^margin/, db = new RegExp("^(" + es + ")(.*)$", "i"), c9 = new RegExp("^(" + es + ")(?!px)[a-z%]+$", "i"), du = new RegExp("^([-+])=(" + es + ")", "i"), ep = {BODY: "block"}, eb = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
    }, d7 = {
        letterSpacing: 0,
        fontWeight: 400
    }, d5 = ["Top", "Right", "Bottom", "Left"], d9 = ["Webkit", "O", "Moz", "ms"], er = cE.fn.toggle;

    function dD(c, a) {
        if (a in c) {
            return a
        }
        var f = a.charAt(0).toUpperCase() + a.slice(1), b = a, d = d9.length;
        while (d--) {
            a = d9[d] + f;
            if (a in c) {
                return a
            }
        }
        return b
    }

    function cA(a, b) {
        a = b || a;
        return cE.css(a, "display") === "none" || !cE.contains(a.ownerDocument, a)
    }

    function dp(a, d) {
        var g, h, f = [], b = 0, c = a.length;
        for (; b < c; b++) {
            g = a[b];
            if (!g.style) {
                continue
            }
            f[b] = cE._data(g, "olddisplay");
            if (d) {
                if (!f[b] && g.style.display === "none") {
                    g.style.display = ""
                }
                if (g.style.display === "" && cA(g)) {
                    f[b] = cE._data(g, "olddisplay", d3(g.nodeName))
                }
            } else {
                h = ed(g, "display");
                if (!f[b] && h !== "none") {
                    cE._data(g, "olddisplay", h)
                }
            }
        }
        for (b = 0; b < c; b++) {
            g = a[b];
            if (!g.style) {
                continue
            }
            if (!d || g.style.display === "none" || g.style.display === "") {
                g.style.display = d ? f[b] || "" : "none"
            }
        }
        return a
    }

    cE.fn.extend({
        css: function (b, a) {
            return cE.access(this, function (c, d, f) {
                return f !== dB ? cE.style(c, d, f) : cE.css(c, d)
            }, b, a, arguments.length > 1)
        }, show: function () {
            return dp(this, true)
        }, hide: function () {
            return dp(this)
        }, toggle: function (a, b) {
            var c = typeof a === "boolean";
            if (cE.isFunction(a) && cE.isFunction(b)) {
                return er.apply(this, arguments)
            }
            return this.each(function () {
                if (c ? a : cA(this)) {
                    cE(this).show()
                } else {
                    cE(this).hide()
                }
            })
        }
    });
    cE.extend({
        cssHooks: {
            opacity: {
                get: function (b, c) {
                    if (c) {
                        var a = ed(b, "opacity");
                        return a === "" ? "1" : a
                    }
                }
            }
        },
        cssNumber: {
            fillOpacity: true,
            fontWeight: true,
            lineHeight: true,
            opacity: true,
            orphans: true,
            widows: true,
            zIndex: true,
            zoom: true
        },
        cssProps: {"float": cE.support.cssFloat ? "cssFloat" : "styleFloat"},
        style: function (a, d, j, b) {
            if (!a || a.nodeType === 3 || a.nodeType === 8 || !a.style) {
                return
            }
            var g, i, c, f = cE.camelCase(d), h = a.style;
            d = cE.cssProps[f] || (cE.cssProps[f] = dD(h, f));
            c = cE.cssHooks[d] || cE.cssHooks[f];
            if (j !== dB) {
                i = typeof j;
                if (i === "string" && (g = du.exec(j))) {
                    j = (g[1] + 1) * g[2] + parseFloat(cE.css(a, d));
                    i = "number"
                }
                if (j == null || i === "number" && isNaN(j)) {
                    return
                }
                if (i === "number" && !cE.cssNumber[f]) {
                    j += "px"
                }
                if (!c || !("set" in c) || (j = c.set(a, j, b)) !== dB) {
                    try {
                        h[d] = j
                    } catch (k) {
                    }
                }
            } else {
                if (c && "get" in c && (g = c.get(a, false, b)) !== dB) {
                    return g
                }
                return h[d]
            }
        },
        css: function (i, b, d, h) {
            var g, c, a, f = cE.camelCase(b);
            b = cE.cssProps[f] || (cE.cssProps[f] = dD(i.style, f));
            a = cE.cssHooks[b] || cE.cssHooks[f];
            if (a && "get" in a) {
                g = a.get(i, true, h)
            }
            if (g === dB) {
                g = ed(i, b)
            }
            if (g === "normal" && b in d7) {
                g = d7[b]
            }
            if (d || h !== dB) {
                c = parseFloat(g);
                return d || cE.isNumeric(c) ? c || 0 : g
            }
            return g
        },
        swap: function (f, c, g) {
            var d, a, b = {};
            for (a in c) {
                b[a] = f.style[a];
                f.style[a] = c[a]
            }
            d = g.call(f);
            for (a in c) {
                f.style[a] = b[a]
            }
            return d
        }
    });
    if (dF.getComputedStyle) {
        ed = function (h, c) {
            var d, g, b, a, i = dF.getComputedStyle(h, null), f = h.style;
            if (i) {
                d = i.getPropertyValue(c) || i[c];
                if (d === "" && !cE.contains(h.ownerDocument, h)) {
                    d = cE.style(h, c)
                }
                if (c9.test(d) && b5.test(c)) {
                    g = f.width;
                    b = f.minWidth;
                    a = f.maxWidth;
                    f.minWidth = f.maxWidth = f.width = d;
                    d = i.width;
                    f.width = g;
                    f.minWidth = b;
                    f.maxWidth = a
                }
            }
            return d
        }
    } else {
        if (ej.documentElement.currentStyle) {
            ed = function (g, a) {
                var f, c, b = g.currentStyle && g.currentStyle[a], d = g.style;
                if (b == null && d && d[a]) {
                    b = d[a]
                }
                if (c9.test(b) && !dj.test(a)) {
                    f = d.left;
                    c = g.runtimeStyle && g.runtimeStyle.left;
                    if (c) {
                        g.runtimeStyle.left = g.currentStyle.left
                    }
                    d.left = a === "fontSize" ? "1em" : b;
                    b = d.pixelLeft + "px";
                    d.left = f;
                    if (c) {
                        g.runtimeStyle.left = c
                    }
                }
                return b === "" ? "auto" : b
            }
        }
    }
    function dm(d, b, a) {
        var c = db.exec(b);
        return c ? Math.max(0, c[1] - (a || 0)) + (c[2] || "px") : b
    }

    function ec(g, c, f, b) {
        var a = f === (b ? "border" : "content") ? 4 : c === "width" ? 1 : 0, d = 0;
        for (; a < 4; a += 2) {
            if (f === "margin") {
                d += cE.css(g, f + d5[a], true)
            }
            if (b) {
                if (f === "content") {
                    d -= parseFloat(ed(g, "padding" + d5[a])) || 0
                }
                if (f !== "margin") {
                    d -= parseFloat(ed(g, "border" + d5[a] + "Width")) || 0
                }
            } else {
                d += parseFloat(ed(g, "padding" + d5[a])) || 0;
                if (f !== "padding") {
                    d += parseFloat(ed(g, "border" + d5[a] + "Width")) || 0
                }
            }
        }
        return d
    }

    function ci(g, b, f) {
        var c = b === "width" ? g.offsetWidth : g.offsetHeight, d = true, a = cE.support.boxSizing && cE.css(g, "boxSizing") === "border-box";
        if (c <= 0 || c == null) {
            c = ed(g, b);
            if (c < 0 || c == null) {
                c = g.style[b]
            }
            if (c9.test(c)) {
                return c
            }
            d = a && (cE.support.boxSizingReliable || c === g.style[b]);
            c = parseFloat(c) || 0
        }
        return (c + ec(g, b, f || (a ? "border" : "content"), d)) + "px"
    }

    function d3(a) {
        if (ep[a]) {
            return ep[a]
        }
        var b = cE("<" + a + ">").appendTo(ej.body), c = b.css("display");
        b.remove();
        if (c === "none" || c === "") {
            cq = ej.body.appendChild(cq || cE.extend(ej.createElement("iframe"), {
                    frameBorder: 0,
                    width: 0,
                    height: 0
                }));
            if (!cs || !cq.createElement) {
                cs = (cq.contentWindow || cq.contentDocument).document;
                cs.write("<!doctype html><html><body>");
                cs.close()
            }
            b = cs.body.appendChild(cs.createElement(a));
            c = ed(b, "display");
            ej.body.removeChild(cq)
        }
        ep[a] = c;
        return c
    }

    cE.each(["height", "width"], function (b, a) {
        cE.cssHooks[a] = {
            get: function (d, c, f) {
                if (c) {
                    if (d.offsetWidth === 0 && cz.test(ed(d, "display"))) {
                        return cE.swap(d, eb, function () {
                            return ci(d, a, f)
                        })
                    } else {
                        return ci(d, a, f)
                    }
                }
            }, set: function (c, f, d) {
                return dm(c, f, d ? ec(c, a, d, cE.support.boxSizing && cE.css(c, "boxSizing") === "border-box") : 0)
            }
        }
    });
    if (!cE.support.opacity) {
        cE.cssHooks.opacity = {
            get: function (a, b) {
                return df.test((b && a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? (0.01 * parseFloat(RegExp.$1)) + "" : b ? "1" : ""
            }, set: function (f, d) {
                var c = f.style, g = f.currentStyle, b = cE.isNumeric(d) ? "alpha(opacity=" + d * 100 + ")" : "", a = g && g.filter || c.filter || "";
                c.zoom = 1;
                if (d >= 1 && cE.trim(a.replace(cd, "")) === "" && c.removeAttribute) {
                    c.removeAttribute("filter");
                    if (g && !g.filter) {
                        return
                    }
                }
                c.filter = cd.test(a) ? a.replace(cd, b) : a + " " + b
            }
        }
    }
    cE(function () {
        if (!cE.support.reliableMarginRight) {
            cE.cssHooks.marginRight = {
                get: function (a, b) {
                    return cE.swap(a, {display: "inline-block"}, function () {
                        if (b) {
                            return ed(a, "marginRight")
                        }
                    })
                }
            }
        }
        if (!cE.support.pixelPosition && cE.fn.position) {
            cE.each(["top", "left"], function (b, a) {
                cE.cssHooks[a] = {
                    get: function (d, c) {
                        if (c) {
                            var f = ed(d, a);
                            return c9.test(f) ? cE(d).position()[a] + "px" : f
                        }
                    }
                }
            })
        }
    });
    if (cE.expr && cE.expr.filters) {
        cE.expr.filters.hidden = function (a) {
            return (a.offsetWidth === 0 && a.offsetHeight === 0) || (!cE.support.reliableHiddenOffsets && ((a.style && a.style.display) || ed(a, "display")) === "none")
        };
        cE.expr.filters.visible = function (a) {
            return !cE.expr.filters.hidden(a)
        }
    }
    cE.each({margin: "", padding: "", border: "Width"}, function (b, a) {
        cE.cssHooks[b + a] = {
            expand: function (g) {
                var d, f = typeof g === "string" ? g.split(" ") : [g], c = {};
                for (d = 0; d < 4; d++) {
                    c[b + d5[d] + a] = f[d] || f[d - 2] || f[0]
                }
                return c
            }
        };
        if (!b5.test(b)) {
            cE.cssHooks[b + a].set = dm
        }
    });
    var cb = /%20/g, cj = /\[\]$/, cv = /\r?\n/g, b0 = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i, dG = /^(?:select|textarea)/i;
    cE.fn.extend({
        serialize: function () {
            return cE.param(this.serializeArray())
        }, serializeArray: function () {
            return this.map(function () {
                return this.elements ? cE.makeArray(this.elements) : this
            }).filter(function () {
                return this.name && !this.disabled && (this.checked || dG.test(this.nodeName) || b0.test(this.type))
            }).map(function (b, c) {
                var a = cE(this).val();
                return a == null ? null : cE.isArray(a) ? cE.map(a, function (f, d) {
                    return {name: c.name, value: f.replace(cv, "\r\n")}
                }) : {name: c.name, value: a.replace(cv, "\r\n")}
            }).get()
        }
    });
    cE.param = function (f, c) {
        var a, b = [], d = function (g, h) {
            h = cE.isFunction(h) ? h() : (h == null ? "" : h);
            b[b.length] = encodeURIComponent(g) + "=" + encodeURIComponent(h)
        };
        if (c === dB) {
            c = cE.ajaxSettings && cE.ajaxSettings.traditional
        }
        if (cE.isArray(f) || (f.jquery && !cE.isPlainObject(f))) {
            cE.each(f, function () {
                d(this.name, this.value)
            })
        } else {
            for (a in f) {
                eg(a, f[a], c, d)
            }
        }
        return b.join("&").replace(cb, "+")
    };
    function eg(b, a, c, f) {
        var d;
        if (cE.isArray(a)) {
            cE.each(a, function (g, h) {
                if (c || cj.test(b)) {
                    f(b, h)
                } else {
                    eg(b + "[" + (typeof h === "object" ? g : "") + "]", h, c, f)
                }
            })
        } else {
            if (!c && cE.type(a) === "object") {
                for (d in a) {
                    eg(b + "[" + d + "]", a[d], c, f)
                }
            } else {
                f(b, a)
            }
        }
    }

    var d4, d2, cQ = /#.*$/, cS = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg, b4 = /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/, c3 = /^(?:GET|HEAD)$/, dl = /^\/\//, dn = /\?/, dC = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, dQ = /([?&])_=[^&]*/, c8 = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/, dR = cE.fn.load, cV = {}, dv = {}, d6 = ["*/"] + ["*"];
    try {
        d2 = cG.href
    } catch (en) {
        d2 = ej.createElement("a");
        d2.href = "";
        d2 = d2.href
    }
    d4 = c8.exec(d2.toLowerCase()) || [];
    function dT(a) {
        return function (b, d) {
            if (typeof b !== "string") {
                d = b;
                b = "*"
            }
            var j, h, i, c = b.toLowerCase().split(ey), f = 0, g = c.length;
            if (cE.isFunction(d)) {
                for (; f < g; f++) {
                    j = c[f];
                    i = /^\+/.test(j);
                    if (i) {
                        j = j.substr(1) || "*"
                    }
                    h = a[j] = a[j] || [];
                    h[i ? "unshift" : "push"](d)
                }
            }
        }
    }

    function cu(j, g, h, c, l, b) {
        l = l || g.dataTypes[0];
        b = b || {};
        b[l] = true;
        var i, f = j[l], a = 0, d = f ? f.length : 0, k = (j === cV);
        for (; a < d && (k || !i); a++) {
            i = f[a](g, h, c);
            if (typeof i === "string") {
                if (!k || b[i]) {
                    i = dB
                } else {
                    g.dataTypes.unshift(i);
                    i = cu(j, g, h, c, i, b)
                }
            }
        }
        if ((k || !i) && !b["*"]) {
            i = cu(j, g, h, c, "*", b)
        }
        return i
    }

    function dY(c, b) {
        var a, f, d = cE.ajaxSettings.flatOptions || {};
        for (a in b) {
            if (b[a] !== dB) {
                (d[a] ? c : (f || (f = {})))[a] = b[a]
            }
        }
        if (f) {
            cE.extend(true, c, f)
        }
    }

    cE.fn.load = function (g, a, i) {
        if (typeof g !== "string" && dR) {
            return dR.apply(this, arguments)
        }
        if (!this.length) {
            return this
        }
        var c, f, b, d = this, h = g.indexOf(" ");
        if (h >= 0) {
            c = g.slice(h, g.length);
            g = g.slice(0, h)
        }
        if (cE.isFunction(a)) {
            i = a;
            a = dB
        } else {
            if (a && typeof a === "object") {
                f = "POST"
            }
        }
        cE.ajax({
            url: g, type: f, dataType: "html", data: a, complete: function (j, k) {
                if (i) {
                    d.each(i, b || [j.responseText, k, j])
                }
            }
        }).done(function (j) {
            b = arguments;
            d.html(c ? cE("<div>").append(j.replace(dC, "")).find(c) : j)
        });
        return this
    };
    cE.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "), function (b, a) {
        cE.fn[a] = function (c) {
            return this.on(a, c)
        }
    });
    cE.each(["get", "post"], function (b, a) {
        cE[a] = function (g, d, c, f) {
            if (cE.isFunction(d)) {
                f = f || c;
                c = d;
                d = dB
            }
            return cE.ajax({type: a, url: g, data: d, success: c, dataType: f})
        }
    });
    cE.extend({
        getScript: function (a, b) {
            return cE.get(a, dB, b, "script")
        },
        getJSON: function (a, b, c) {
            return cE.get(a, b, c, "json")
        },
        ajaxSetup: function (a, b) {
            if (b) {
                dY(a, cE.ajaxSettings)
            } else {
                b = a;
                a = cE.ajaxSettings
            }
            dY(a, b);
            return a
        },
        ajaxSettings: {
            url: d2,
            isLocal: b4.test(d4[1]),
            global: true,
            type: "GET",
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            processData: true,
            async: true,
            accepts: {
                xml: "application/xml, text/xml",
                html: "text/html",
                text: "text/plain",
                json: "application/json, text/javascript",
                "*": d6
            },
            contents: {xml: /xml/, html: /html/, json: /json/},
            responseFields: {xml: "responseXML", text: "responseText"},
            converters: {"* text": dF.String, "text html": true, "text json": cE.parseJSON, "text xml": cE.parseXML},
            flatOptions: {context: true, url: true}
        },
        ajaxPrefilter: dT(cV),
        ajaxTransport: dT(dv),
        ajax: function (z, k) {
            if (typeof z === "object") {
                k = z;
                z = dB
            }
            k = k || {};
            var i, q, p, x, w, m, f, h, s = cE.ajaxSetup({}, k), l = s.context || s, g = l !== s && (l.nodeType || l instanceof cE) ? cE(l) : cE.event, b = cE.Deferred(), a = cE.Callbacks("once memory"), u = s.statusCode || {}, n = {}, o = {}, t = 0, v = "canceled", j = {
                readyState: 0,
                setRequestHeader: function (A, B) {
                    if (!t) {
                        var C = A.toLowerCase();
                        A = o[C] = o[C] || A;
                        n[A] = B
                    }
                    return this
                },
                getAllResponseHeaders: function () {
                    return t === 2 ? q : null
                },
                getResponseHeader: function (B) {
                    var A;
                    if (t === 2) {
                        if (!p) {
                            p = {};
                            while ((A = cS.exec(q))) {
                                p[A[1].toLowerCase()] = A[2]
                            }
                        }
                        A = p[B.toLowerCase()]
                    }
                    return A === dB ? null : A
                },
                overrideMimeType: function (A) {
                    if (!t) {
                        s.mimeType = A
                    }
                    return this
                },
                abort: function (A) {
                    A = A || v;
                    if (x) {
                        x.abort(A)
                    }
                    c(0, A);
                    return this
                }
            };

            function c(G, D, F, A) {
                var B, J, K, E, C, I = D;
                if (t === 2) {
                    return
                }
                t = 2;
                if (w) {
                    clearTimeout(w)
                }
                x = dB;
                q = A || "";
                j.readyState = G > 0 ? 4 : 0;
                if (F) {
                    E = d0(s, j, F)
                }
                if (G >= 200 && G < 300 || G === 304) {
                    if (s.ifModified) {
                        C = j.getResponseHeader("Last-Modified");
                        if (C) {
                            cE.lastModified[i] = C
                        }
                        C = j.getResponseHeader("Etag");
                        if (C) {
                            cE.etag[i] = C
                        }
                    }
                    if (G === 304) {
                        I = "notmodified";
                        B = true
                    } else {
                        B = dW(s, E);
                        I = B.state;
                        J = B.data;
                        K = B.error;
                        B = !K
                    }
                } else {
                    K = I;
                    if (!I || G) {
                        I = "error";
                        if (G < 0) {
                            G = 0
                        }
                    }
                }
                j.status = G;
                j.statusText = (D || I) + "";
                if (B) {
                    b.resolveWith(l, [J, I, j])
                } else {
                    b.rejectWith(l, [j, I, K])
                }
                j.statusCode(u);
                u = dB;
                if (f) {
                    g.trigger("ajax" + (B ? "Success" : "Error"), [j, s, B ? J : K])
                }
                a.fireWith(l, [j, I]);
                if (f) {
                    g.trigger("ajaxComplete", [j, s]);
                    if (!(--cE.active)) {
                        cE.event.trigger("ajaxStop")
                    }
                }
            }

            b.promise(j);
            j.success = j.done;
            j.error = j.fail;
            j.complete = a.add;
            j.statusCode = function (B) {
                if (B) {
                    var A;
                    if (t < 2) {
                        for (A in B) {
                            u[A] = [u[A], B[A]]
                        }
                    } else {
                        A = B[j.status];
                        j.always(A)
                    }
                }
                return this
            };
            s.url = ((z || s.url) + "").replace(cQ, "").replace(dl, d4[1] + "//");
            s.dataTypes = cE.trim(s.dataType || "*").toLowerCase().split(ey);
            if (s.crossDomain == null) {
                m = c8.exec(s.url.toLowerCase());
                s.crossDomain = !!(m && (m[1] !== d4[1] || m[2] !== d4[2] || (m[3] || (m[1] === "http:" ? 80 : 443)) != (d4[3] || (d4[1] === "http:" ? 80 : 443))))
            }
            if (s.data && s.processData && typeof s.data !== "string") {
                s.data = cE.param(s.data, s.traditional)
            }
            cu(cV, s, k, j);
            if (t === 2) {
                return j
            }
            f = s.global;
            s.type = s.type.toUpperCase();
            s.hasContent = !c3.test(s.type);
            if (f && cE.active++ === 0) {
                cE.event.trigger("ajaxStart")
            }
            if (!s.hasContent) {
                if (s.data) {
                    s.url += (dn.test(s.url) ? "&" : "?") + s.data;
                    delete s.data
                }
                i = s.url;
                if (s.cache === false) {
                    var y = cE.now(), r = s.url.replace(dQ, "$1_=" + y);
                    s.url = r + ((r === s.url) ? (dn.test(s.url) ? "&" : "?") + "_=" + y : "")
                }
            }
            if (s.data && s.hasContent && s.contentType !== false || k.contentType) {
                j.setRequestHeader("Content-Type", s.contentType)
            }
            if (s.ifModified) {
                i = i || s.url;
                if (cE.lastModified[i]) {
                    j.setRequestHeader("If-Modified-Since", cE.lastModified[i])
                }
                if (cE.etag[i]) {
                    j.setRequestHeader("If-None-Match", cE.etag[i])
                }
            }
            j.setRequestHeader("Accept", s.dataTypes[0] && s.accepts[s.dataTypes[0]] ? s.accepts[s.dataTypes[0]] + (s.dataTypes[0] !== "*" ? ", " + d6 + "; q=0.01" : "") : s.accepts["*"]);
            for (h in s.headers) {
                j.setRequestHeader(h, s.headers[h])
            }
            if (s.beforeSend && (s.beforeSend.call(l, j, s) === false || t === 2)) {
                return j.abort()
            }
            v = "abort";
            for (h in {success: 1, error: 1, complete: 1}) {
                j[h](s[h])
            }
            x = cu(dv, s, k, j);
            if (!x) {
                c(-1, "No Transport")
            } else {
                j.readyState = 1;
                if (f) {
                    g.trigger("ajaxSend", [j, s])
                }
                if (s.async && s.timeout > 0) {
                    w = setTimeout(function () {
                        j.abort("timeout")
                    }, s.timeout)
                }
                try {
                    t = 1;
                    x.send(n, c)
                } catch (d) {
                    if (t < 2) {
                        c(-1, d)
                    } else {
                        throw d
                    }
                }
            }
            return j
        },
        active: 0,
        lastModified: {},
        etag: {}
    });
    function d0(h, d, g) {
        var j, i, b, c, k = h.contents, a = h.dataTypes, f = h.responseFields;
        for (i in f) {
            if (i in g) {
                d[f[i]] = g[i]
            }
        }
        while (a[0] === "*") {
            a.shift();
            if (j === dB) {
                j = h.mimeType || d.getResponseHeader("content-type")
            }
        }
        if (j) {
            for (i in k) {
                if (k[i] && k[i].test(j)) {
                    a.unshift(i);
                    break
                }
            }
        }
        if (a[0] in g) {
            b = a[0]
        } else {
            for (i in g) {
                if (!a[0] || h.converters[i + " " + a[0]]) {
                    b = i;
                    break
                }
                if (!c) {
                    c = i
                }
            }
            b = b || c
        }
        if (b) {
            if (b !== a[0]) {
                a.unshift(b)
            }
            return g[b]
        }
    }

    function dW(j, i) {
        var l, a, c, k, d = j.dataTypes.slice(), h = d[0], b = {}, g = 0;
        if (j.dataFilter) {
            i = j.dataFilter(i, j.dataType)
        }
        if (d[1]) {
            for (l in j.converters) {
                b[l.toLowerCase()] = j.converters[l]
            }
        }
        for (; (c = d[++g]);) {
            if (c !== "*") {
                if (h !== "*" && h !== c) {
                    l = b[h + " " + c] || b["* " + c];
                    if (!l) {
                        for (a in b) {
                            k = a.split(" ");
                            if (k[1] === c) {
                                l = b[h + " " + k[0]] || b["* " + k[0]];
                                if (l) {
                                    if (l === true) {
                                        l = b[a]
                                    } else {
                                        if (b[a] !== true) {
                                            c = k[0];
                                            d.splice(g--, 0, c)
                                        }
                                    }
                                    break
                                }
                            }
                        }
                    }
                    if (l !== true) {
                        if (l && j["throws"]) {
                            i = l(i)
                        } else {
                            try {
                                i = l(i)
                            } catch (f) {
                                return {state: "parsererror", error: l ? f : "No conversion from " + h + " to " + c}
                            }
                        }
                    }
                }
                h = c
            }
        }
        return {state: "success", data: i}
    }

    var cR = [], dq = /\?/, b1 = /(=)\?(?=&|$)|\?\?/, cP = cE.now();
    cE.ajaxSetup({
        jsonp: "callback", jsonpCallback: function () {
            var a = cR.pop() || (cE.expando + "_" + (cP++));
            this[a] = true;
            return a
        }
    });
    cE.ajaxPrefilter("json jsonp", function (i, c, b) {
        var l, d, h, k = i.data, j = i.url, a = i.jsonp !== false, g = a && b1.test(j), f = a && !g && typeof k === "string" && !(i.contentType || "").indexOf("application/x-www-form-urlencoded") && b1.test(k);
        if (i.dataTypes[0] === "jsonp" || g || f) {
            l = i.jsonpCallback = cE.isFunction(i.jsonpCallback) ? i.jsonpCallback() : i.jsonpCallback;
            d = dF[l];
            if (g) {
                i.url = j.replace(b1, "$1" + l)
            } else {
                if (f) {
                    i.data = k.replace(b1, "$1" + l)
                } else {
                    if (a) {
                        i.url += (dq.test(j) ? "&" : "?") + i.jsonp + "=" + l
                    }
                }
            }
            i.converters["script json"] = function () {
                if (!h) {
                    cE.error(l + " was not called")
                }
                return h[0]
            };
            i.dataTypes[0] = "json";
            dF[l] = function () {
                h = arguments
            };
            b.always(function () {
                dF[l] = d;
                if (i[l]) {
                    i.jsonpCallback = c.jsonpCallback;
                    cR.push(l)
                }
                if (h && cE.isFunction(d)) {
                    d(h[0])
                }
                h = d = dB
            });
            return "script"
        }
    });
    cE.ajaxSetup({
        accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
        contents: {script: /javascript|ecmascript/},
        converters: {
            "text script": function (a) {
                cE.globalEval(a);
                return a
            }
        }
    });
    cE.ajaxPrefilter("script", function (a) {
        if (a.cache === dB) {
            a.cache = false
        }
        if (a.crossDomain) {
            a.type = "GET";
            a.global = false
        }
    });
    cE.ajaxTransport("script", function (b) {
        if (b.crossDomain) {
            var a, c = ej.head || ej.getElementsByTagName("head")[0] || ej.documentElement;
            return {
                send: function (d, f) {
                    a = ej.createElement("script");
                    a.async = "async";
                    if (b.scriptCharset) {
                        a.charset = b.scriptCharset
                    }
                    a.src = b.url;
                    a.onload = a.onreadystatechange = function (g, h) {
                        if (h || !a.readyState || /loaded|complete/.test(a.readyState)) {
                            a.onload = a.onreadystatechange = null;
                            if (c && a.parentNode) {
                                c.removeChild(a)
                            }
                            a = dB;
                            if (!h) {
                                f(200, "success")
                            }
                        }
                    };
                    c.insertBefore(a, c.firstChild)
                }, abort: function () {
                    if (a) {
                        a.onload(0, 1)
                    }
                }
            }
        }
    });
    var dL, dP = dF.ActiveXObject ? function () {
        for (var a in dL) {
            dL[a](0, 1)
        }
    } : false, dN = 0;

    function dZ() {
        try {
            return new dF.XMLHttpRequest()
        } catch (a) {
        }
    }

    function dS() {
        try {
            return new dF.ActiveXObject("Microsoft.XMLHTTP")
        } catch (a) {
        }
    }

    cE.ajaxSettings.xhr = dF.ActiveXObject ? function () {
        return !this.isLocal && dZ() || dS()
    } : dZ;
    (function (a) {
        cE.extend(cE.support, {ajax: !!a, cors: !!a && ("withCredentials" in a)})
    })(cE.ajaxSettings.xhr());
    if (cE.support.ajax) {
        cE.ajaxTransport(function (a) {
            if (!a.crossDomain || cE.support.cors) {
                var b;
                return {
                    send: function (g, d) {
                        var f, h, i = a.xhr();
                        if (a.username) {
                            i.open(a.type, a.url, a.async, a.username, a.password)
                        } else {
                            i.open(a.type, a.url, a.async)
                        }
                        if (a.xhrFields) {
                            for (h in a.xhrFields) {
                                i[h] = a.xhrFields[h]
                            }
                        }
                        if (a.mimeType && i.overrideMimeType) {
                            i.overrideMimeType(a.mimeType)
                        }
                        if (!a.crossDomain && !g["X-Requested-With"]) {
                            g["X-Requested-With"] = "XMLHttpRequest"
                        }
                        try {
                            for (h in g) {
                                i.setRequestHeader(h, g[h])
                            }
                        } catch (c) {
                        }
                        i.send((a.hasContent && a.data) || null);
                        b = function (j, m) {
                            var p, q, n, o, r;
                            try {
                                if (b && (m || i.readyState === 4)) {
                                    b = dB;
                                    if (f) {
                                        i.onreadystatechange = cE.noop;
                                        if (dP) {
                                            delete dL[f]
                                        }
                                    }
                                    if (m) {
                                        if (i.readyState !== 4) {
                                            i.abort()
                                        }
                                    } else {
                                        p = i.status;
                                        n = i.getAllResponseHeaders();
                                        o = {};
                                        r = i.responseXML;
                                        if (r && r.documentElement) {
                                            o.xml = r
                                        }
                                        try {
                                            o.text = i.responseText
                                        } catch (k) {
                                        }
                                        try {
                                            q = i.statusText
                                        } catch (k) {
                                            q = ""
                                        }
                                        if (!p && a.isLocal && !a.crossDomain) {
                                            p = o.text ? 200 : 404
                                        } else {
                                            if (p === 1223) {
                                                p = 204
                                            }
                                        }
                                    }
                                }
                            } catch (l) {
                                if (!m) {
                                    d(-1, l)
                                }
                            }
                            if (o) {
                                d(p, q, o, n)
                            }
                        };
                        if (!a.async) {
                            b()
                        } else {
                            if (i.readyState === 4) {
                                setTimeout(b, 0)
                            } else {
                                f = ++dN;
                                if (dP) {
                                    if (!dL) {
                                        dL = {};
                                        cE(dF).unload(dP)
                                    }
                                    dL[f] = b
                                }
                                i.onreadystatechange = b
                            }
                        }
                    }, abort: function () {
                        if (b) {
                            b(0, 1)
                        }
                    }
                }
            }
        })
    }
    var ca, dt, cO = /^(?:toggle|show|hide)$/, cM = new RegExp("^(?:([-+])=|)(" + es + ")([a-z%]*)$", "i"), dA = /queueHooks$/, ea = [eh], dz = {
        "*": [function (b, i) {
            var k, h, g = this.createTween(b, i), a = cM.exec(i), f = g.cur(), d = +f || 0, c = 1, j = 20;
            if (a) {
                k = +a[2];
                h = a[3] || (cE.cssNumber[b] ? "" : "px");
                if (h !== "px" && d) {
                    d = cE.css(g.elem, b, true) || k || 1;
                    do {
                        c = c || ".5";
                        d = d / c;
                        cE.style(g.elem, b, d + h)
                    } while (c !== (c = g.cur() / f) && c !== 1 && --j)
                }
                g.unit = h;
                g.start = d;
                g.end = a[1] ? d + (a[1] + 1) * k : k
            }
            return g
        }]
    };

    function dU() {
        setTimeout(function () {
            ca = dB
        }, 0);
        return (ca = cE.now())
    }

    function d1(b, a) {
        cE.each(a, function (g, h) {
            var c = (dz[g] || []).concat(dz["*"]), d = 0, f = c.length;
            for (; d < f; d++) {
                if (c[d].call(b, g, h)) {
                    return
                }
            }
        })
    }

    function d8(a, f, d) {
        var h, b = 0, j = 0, c = ea.length, k = cE.Deferred().always(function () {
            delete i.elem
        }), i = function () {
            var m = ca || dU(), q = Math.max(0, l.startTime + l.duration - m), r = q / l.duration || 0, p = 1 - r, n = 0, o = l.tweens.length;
            for (; n < o; n++) {
                l.tweens[n].run(p)
            }
            k.notifyWith(a, [l, p, q]);
            if (p < 1 && o) {
                return q
            } else {
                k.resolveWith(a, [l]);
                return false
            }
        }, l = k.promise({
            elem: a,
            props: cE.extend({}, f),
            opts: cE.extend(true, {specialEasing: {}}, d),
            originalProperties: f,
            originalOptions: d,
            startTime: ca || dU(),
            duration: d.duration,
            tweens: [],
            createTween: function (o, n, m) {
                var p = cE.Tween(a, l.opts, o, n, l.opts.specialEasing[o] || l.opts.easing);
                l.tweens.push(p);
                return p
            },
            stop: function (m) {
                var n = 0, o = m ? l.tweens.length : 0;
                for (; n < o; n++) {
                    l.tweens[n].run(1)
                }
                if (m) {
                    k.resolveWith(a, [l, m])
                } else {
                    k.rejectWith(a, [l, m])
                }
                return this
            }
        }), g = l.props;
        cX(g, l.opts.specialEasing);
        for (; b < c; b++) {
            h = ea[b].call(l, a, g, l.opts);
            if (h) {
                return h
            }
        }
        d1(l, g);
        if (cE.isFunction(l.opts.start)) {
            l.opts.start.call(a, l)
        }
        cE.fx.timer(cE.extend(i, {anim: l, queue: l.opts.queue, elem: a}));
        return l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always)
    }

    function cX(c, d) {
        var a, b, h, f, g;
        for (a in c) {
            b = cE.camelCase(a);
            h = d[b];
            f = c[a];
            if (cE.isArray(f)) {
                h = f[1];
                f = c[a] = f[0]
            }
            if (a !== b) {
                c[b] = f;
                delete c[a]
            }
            g = cE.cssHooks[b];
            if (g && "expand" in g) {
                f = g.expand(f);
                delete c[b];
                for (a in f) {
                    if (!(a in c)) {
                        c[a] = f[a];
                        d[a] = h
                    }
                }
            } else {
                d[b] = h
            }
        }
    }

    cE.Animation = cE.extend(d8, {
        tweener: function (c, f) {
            if (cE.isFunction(c)) {
                f = c;
                c = ["*"]
            } else {
                c = c.split(" ")
            }
            var b, d = 0, a = c.length;
            for (; d < a; d++) {
                b = c[d];
                dz[b] = dz[b] || [];
                dz[b].unshift(f)
            }
        }, prefilter: function (b, a) {
            if (a) {
                ea.unshift(b)
            } else {
                ea.push(b)
            }
        }
    });
    function eh(a, m, i) {
        var f, k, q, g, l, o, p, d, h, r = this, n = a.style, j = {}, b = [], c = a.nodeType && cA(a);
        if (!i.queue) {
            d = cE._queueHooks(a, "fx");
            if (d.unqueued == null) {
                d.unqueued = 0;
                h = d.empty.fire;
                d.empty.fire = function () {
                    if (!d.unqueued) {
                        h()
                    }
                }
            }
            d.unqueued++;
            r.always(function () {
                r.always(function () {
                    d.unqueued--;
                    if (!cE.queue(a, "fx").length) {
                        d.empty.fire()
                    }
                })
            })
        }
        if (a.nodeType === 1 && ("height" in m || "width" in m)) {
            i.overflow = [n.overflow, n.overflowX, n.overflowY];
            if (cE.css(a, "display") === "inline" && cE.css(a, "float") === "none") {
                if (!cE.support.inlineBlockNeedsLayout || d3(a.nodeName) === "inline") {
                    n.display = "inline-block"
                } else {
                    n.zoom = 1
                }
            }
        }
        if (i.overflow) {
            n.overflow = "hidden";
            if (!cE.support.shrinkWrapBlocks) {
                r.done(function () {
                    n.overflow = i.overflow[0];
                    n.overflowX = i.overflow[1];
                    n.overflowY = i.overflow[2]
                })
            }
        }
        for (f in m) {
            q = m[f];
            if (cO.exec(q)) {
                delete m[f];
                o = o || q === "toggle";
                if (q === (c ? "hide" : "show")) {
                    continue
                }
                b.push(f)
            }
        }
        g = b.length;
        if (g) {
            l = cE._data(a, "fxshow") || cE._data(a, "fxshow", {});
            if ("hidden" in l) {
                c = l.hidden
            }
            if (o) {
                l.hidden = !c
            }
            if (c) {
                cE(a).show()
            } else {
                r.done(function () {
                    cE(a).hide()
                })
            }
            r.done(function () {
                var s;
                cE.removeData(a, "fxshow", true);
                for (s in j) {
                    cE.style(a, s, j[s])
                }
            });
            for (f = 0; f < g; f++) {
                k = b[f];
                p = r.createTween(k, c ? l[k] : 0);
                j[k] = l[k] || cE.style(a, k);
                if (!(k in l)) {
                    l[k] = p.start;
                    if (c) {
                        p.end = p.start;
                        p.start = k === "width" || k === "height" ? 1 : 0
                    }
                }
            }
        }
    }

    function dx(d, b, c, a, f) {
        return new dx.prototype.init(d, b, c, a, f)
    }

    cE.Tween = dx;
    dx.prototype = {
        constructor: dx, init: function (f, b, c, a, g, d) {
            this.elem = f;
            this.prop = c;
            this.easing = g || "swing";
            this.options = b;
            this.start = this.now = this.cur();
            this.end = a;
            this.unit = d || (cE.cssNumber[c] ? "" : "px")
        }, cur: function () {
            var a = dx.propHooks[this.prop];
            return a && a.get ? a.get(this) : dx.propHooks._default.get(this)
        }, run: function (a) {
            var c, b = dx.propHooks[this.prop];
            if (this.options.duration) {
                this.pos = c = cE.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration)
            } else {
                this.pos = c = a
            }
            this.now = (this.end - this.start) * c + this.start;
            if (this.options.step) {
                this.options.step.call(this.elem, this.now, this)
            }
            if (b && b.set) {
                b.set(this)
            } else {
                dx.propHooks._default.set(this)
            }
            return this
        }
    };
    dx.prototype.init.prototype = dx.prototype;
    dx.propHooks = {
        _default: {
            get: function (a) {
                var b;
                if (a.elem[a.prop] != null && (!a.elem.style || a.elem.style[a.prop] == null)) {
                    return a.elem[a.prop]
                }
                b = cE.css(a.elem, a.prop, false, "");
                return !b || b === "auto" ? 0 : b
            }, set: function (a) {
                if (cE.fx.step[a.prop]) {
                    cE.fx.step[a.prop](a)
                } else {
                    if (a.elem.style && (a.elem.style[cE.cssProps[a.prop]] != null || cE.cssHooks[a.prop])) {
                        cE.style(a.elem, a.prop, a.now + a.unit)
                    } else {
                        a.elem[a.prop] = a.now
                    }
                }
            }
        }
    };
    dx.propHooks.scrollTop = dx.propHooks.scrollLeft = {
        set: function (a) {
            if (a.elem.nodeType && a.elem.parentNode) {
                a.elem[a.prop] = a.now
            }
        }
    };
    cE.each(["toggle", "show", "hide"], function (b, a) {
        var c = cE.fn[a];
        cE.fn[a] = function (g, f, d) {
            return g == null || typeof g === "boolean" || (!b && cE.isFunction(g) && cE.isFunction(f)) ? c.apply(this, arguments) : this.animate(cc(a, true), g, f, d)
        }
    });
    cE.fn.extend({
        fadeTo: function (a, b, c, d) {
            return this.filter(cA).css("opacity", 0).show().end().animate({opacity: b}, a, c, d)
        }, animate: function (d, f, a, h) {
            var b = cE.isEmptyObject(d), c = cE.speed(f, a, h), g = function () {
                var i = d8(this, cE.extend({}, d), c);
                if (b) {
                    i.stop(true)
                }
            };
            return b || c.queue === false ? this.each(g) : this.queue(c.queue, g)
        }, stop: function (b, d, c) {
            var a = function (f) {
                var g = f.stop;
                delete f.stop;
                g(c)
            };
            if (typeof b !== "string") {
                c = d;
                d = b;
                b = dB
            }
            if (d && b !== false) {
                this.queue(b || "fx", [])
            }
            return this.each(function () {
                var g = true, h = b != null && b + "queueHooks", i = cE.timers, f = cE._data(this);
                if (h) {
                    if (f[h] && f[h].stop) {
                        a(f[h])
                    }
                } else {
                    for (h in f) {
                        if (f[h] && f[h].stop && dA.test(h)) {
                            a(f[h])
                        }
                    }
                }
                for (h = i.length; h--;) {
                    if (i[h].elem === this && (b == null || i[h].queue === b)) {
                        i[h].anim.stop(c);
                        g = false;
                        i.splice(h, 1)
                    }
                }
                if (g || !c) {
                    cE.dequeue(this, b)
                }
            })
        }
    });
    function cc(b, a) {
        var c, f = {height: b}, d = 0;
        a = a ? 1 : 0;
        for (; d < 4; d += 2 - a) {
            c = d5[d];
            f["margin" + c] = f["padding" + c] = b
        }
        if (a) {
            f.opacity = f.width = b
        }
        return f
    }

    cE.each({
        slideDown: cc("show"),
        slideUp: cc("hide"),
        slideToggle: cc("toggle"),
        fadeIn: {opacity: "show"},
        fadeOut: {opacity: "hide"},
        fadeToggle: {opacity: "toggle"}
    }, function (b, a) {
        cE.fn[b] = function (f, d, c) {
            return this.animate(a, f, d, c)
        }
    });
    cE.speed = function (b, d, c) {
        var a = b && typeof b === "object" ? cE.extend({}, b) : {
            complete: c || !c && d || cE.isFunction(b) && b,
            duration: b,
            easing: c && d || d && !cE.isFunction(d) && d
        };
        a.duration = cE.fx.off ? 0 : typeof a.duration === "number" ? a.duration : a.duration in cE.fx.speeds ? cE.fx.speeds[a.duration] : cE.fx.speeds._default;
        if (a.queue == null || a.queue === true) {
            a.queue = "fx"
        }
        a.old = a.complete;
        a.complete = function () {
            if (cE.isFunction(a.old)) {
                a.old.call(this)
            }
            if (a.queue) {
                cE.dequeue(this, a.queue)
            }
        };
        return a
    };
    cE.easing = {
        linear: function (a) {
            return a
        }, swing: function (a) {
            return 0.5 - Math.cos(a * Math.PI) / 2
        }
    };
    cE.timers = [];
    cE.fx = dx.prototype.init;
    cE.fx.tick = function () {
        var b, a = cE.timers, c = 0;
        ca = cE.now();
        for (; c < a.length; c++) {
            b = a[c];
            if (!b() && a[c] === b) {
                a.splice(c--, 1)
            }
        }
        if (!a.length) {
            cE.fx.stop()
        }
        ca = dB
    };
    cE.fx.timer = function (a) {
        if (a() && cE.timers.push(a) && !dt) {
            dt = setInterval(cE.fx.tick, cE.fx.interval)
        }
    };
    cE.fx.interval = 13;
    cE.fx.stop = function () {
        clearInterval(dt);
        dt = null
    };
    cE.fx.speeds = {slow: 600, fast: 200, _default: 400};
    cE.fx.step = {};
    if (cE.expr && cE.expr.filters) {
        cE.expr.filters.animated = function (a) {
            return cE.grep(cE.timers, function (b) {
                return a === b.elem
            }).length
        }
    }
    var dy = /^(?:body|html)$/i;
    cE.fn.offset = function (g) {
        if (arguments.length) {
            return g === dB ? this : this.each(function (m) {
                cE.offset.setOffset(this, g, m)
            })
        }
        var d, l, j, b, a, i, h, k = {top: 0, left: 0}, f = this[0], c = f && f.ownerDocument;
        if (!c) {
            return
        }
        if ((l = c.body) === f) {
            return cE.offset.bodyOffset(f)
        }
        d = c.documentElement;
        if (!cE.contains(d, f)) {
            return k
        }
        if (typeof f.getBoundingClientRect !== "undefined") {
            k = f.getBoundingClientRect()
        }
        j = ck(c);
        b = d.clientTop || l.clientTop || 0;
        a = d.clientLeft || l.clientLeft || 0;
        i = j.pageYOffset || d.scrollTop;
        h = j.pageXOffset || d.scrollLeft;
        return {top: k.top + i - b, left: k.left + h - a}
    };
    cE.offset = {
        bodyOffset: function (c) {
            var a = c.offsetTop, b = c.offsetLeft;
            if (cE.support.doesNotIncludeMarginInBodyOffset) {
                a += parseFloat(cE.css(c, "marginTop")) || 0;
                b += parseFloat(cE.css(c, "marginLeft")) || 0
            }
            return {top: a, left: b}
        }, setOffset: function (h, j, i) {
            var k = cE.css(h, "position");
            if (k === "static") {
                h.style.position = "relative"
            }
            var b = cE(h), d = b.offset(), a = cE.css(h, "top"), l = cE.css(h, "left"), n = (k === "absolute" || k === "fixed") && cE.inArray("auto", [a, l]) > -1, m = {}, f = {}, g, c;
            if (n) {
                f = b.position();
                g = f.top;
                c = f.left
            } else {
                g = parseFloat(a) || 0;
                c = parseFloat(l) || 0
            }
            if (cE.isFunction(j)) {
                j = j.call(h, i, d)
            }
            if (j.top != null) {
                m.top = (j.top - d.top) + g
            }
            if (j.left != null) {
                m.left = (j.left - d.left) + c
            }
            if ("using" in j) {
                j.using.call(h, m)
            } else {
                b.css(m)
            }
        }
    };
    cE.fn.extend({
        position: function () {
            if (!this[0]) {
                return
            }
            var d = this[0], a = this.offsetParent(), c = this.offset(), b = dy.test(a[0].nodeName) ? {
                top: 0,
                left: 0
            } : a.offset();
            c.top -= parseFloat(cE.css(d, "marginTop")) || 0;
            c.left -= parseFloat(cE.css(d, "marginLeft")) || 0;
            b.top += parseFloat(cE.css(a[0], "borderTopWidth")) || 0;
            b.left += parseFloat(cE.css(a[0], "borderLeftWidth")) || 0;
            return {top: c.top - b.top, left: c.left - b.left}
        }, offsetParent: function () {
            return this.map(function () {
                var a = this.offsetParent || ej.body;
                while (a && (!dy.test(a.nodeName) && cE.css(a, "position") === "static")) {
                    a = a.offsetParent
                }
                return a || ej.body
            })
        }
    });
    cE.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (c, b) {
        var a = /Y/.test(b);
        cE.fn[c] = function (d) {
            return cE.access(this, function (f, g, h) {
                var i = ck(f);
                if (h === dB) {
                    return i ? (b in i) ? i[b] : i.document.documentElement[g] : f[g]
                }
                if (i) {
                    i.scrollTo(!a ? h : cE(i).scrollLeft(), a ? h : cE(i).scrollTop())
                } else {
                    f[g] = h
                }
            }, c, d, arguments.length, null)
        }
    });
    function ck(a) {
        return cE.isWindow(a) ? a : a.nodeType === 9 ? a.defaultView || a.parentWindow : false
    }

    cE.each({Height: "height", Width: "width"}, function (b, a) {
        cE.each({padding: "inner" + b, content: a, "": "outer" + b}, function (c, d) {
            cE.fn[d] = function (h, i) {
                var f = arguments.length && (c || typeof h !== "boolean"), g = c || (h === true || i === true ? "margin" : "border");
                return cE.access(this, function (k, l, m) {
                    var j;
                    if (cE.isWindow(k)) {
                        return k.document.documentElement["client" + b]
                    }
                    if (k.nodeType === 9) {
                        j = k.documentElement;
                        return Math.max(k.body["scroll" + b], j["scroll" + b], k.body["offset" + b], j["offset" + b], j["client" + b])
                    }
                    return m === dB ? cE.css(k, l, m, g) : cE.style(k, l, m, g)
                }, a, f ? h : dB, f, null)
            }
        })
    });
    dF.jQuery = dF.$ = cE;
    if (typeof define === "function" && define.amd && define.amd.jQuery) {
        define("jquery", [], function () {
            return cE
        })
    }
})(window);
(function (ao, aQ, aD) {
    var aM = {
        html: false,
        photo: false,
        iframe: false,
        inline: false,
        transition: "elastic",
        speed: 300,
        fadeOut: 300,
        width: false,
        initialWidth: "600",
        innerWidth: false,
        maxWidth: false,
        height: false,
        initialHeight: "450",
        innerHeight: false,
        maxHeight: false,
        scalePhotos: true,
        scrolling: true,
        href: false,
        title: false,
        rel: false,
        opacity: 0.9,
        preloading: true,
        className: false,
        overlayClose: true,
        escKey: true,
        arrowKey: true,
        top: false,
        bottom: false,
        left: false,
        right: false,
        fixed: false,
        data: undefined,
        closeButton: true,
        fastIframe: true,
        open: false,
        reposition: true,
        loop: true,
        slideshow: false,
        slideshowAuto: true,
        slideshowSpeed: 2500,
        slideshowStart: "start slideshow",
        slideshowStop: "stop slideshow",
        photoRegex: /\.(gif|png|jp(e|g|eg)|bmp|ico|webp)((#|\?).*)?$/i,
        retinaImage: false,
        retinaUrl: false,
        retinaSuffix: "@2x.$1",
        current: "image {current} of {total}",
        previous: "previous",
        next: "next",
        close: "close",
        xhrError: "This content failed to load.",
        imgError: "This image failed to load.",
        returnFocus: true,
        trapFocus: true,
        onOpen: false,
        onLoad: false,
        onComplete: false,
        onCleanup: false,
        onClosed: false
    }, aK = "colorbox", at = "cbox", ap = at + "Element", a2 = at + "_open", a0 = at + "_load", aY = at + "_complete", aU = at + "_cleanup", aW = at + "_closed", a4 = at + "_purge", a3, aH, bl, aL, bh, aT, a9, aF, a7, bj, aV, aX, aZ, bf, aN, bb, a1, a5, aJ, aR, aP = ao("<a/>"), az, bc, be, bm, bo, aS, a8, ar, aq, bn, aI, bq, av, aO = "div", aG, aw = 0, au = {}, ba;

    function bd(d, c, a) {
        var b = aQ.createElement(d);
        if (c) {
            b.id = at + c
        }
        if (a) {
            b.style.cssText = a
        }
        return ao(b)
    }

    function aE() {
        return aD.innerHeight ? aD.innerHeight : ao(aD).height()
    }

    function a6(a) {
        var b = a7.length, c = (a8 + a) % b;
        return (c < 0) ? b + c : c
    }

    function ay(b, a) {
        return Math.round((/%/.test(b) ? ((a === "x" ? bj.width() : aE()) / 100) : 1) * parseInt(b, 10))
    }

    function bg(a, b) {
        return a.photo || a.photoRegex.test(b)
    }

    function ax(a, b) {
        return a.retinaUrl && aD.devicePixelRatio > 1 ? b.replace(a.photoRegex, a.retinaSuffix) : b
    }

    function aB(a) {
        if ("contains" in aH[0] && !aH[0].contains(a.target)) {
            a.stopPropagation();
            aH.focus()
        }
    }

    function bs() {
        var b, a = ao.data(aS, aK);
        if (a == null) {
            az = ao.extend({}, aM);
            if (console && console.log) {
                console.log("Error: cboxElement missing settings object")
            }
        } else {
            az = ao.extend({}, a)
        }
        for (b in az) {
            if (ao.isFunction(az[b]) && b.slice(0, 2) !== "on") {
                az[b] = az[b].call(aS)
            }
        }
        az.rel = az.rel || aS.rel || ao(aS).data("rel") || "nofollow";
        az.href = az.href || ao(aS).attr("href");
        az.title = az.title || aS.title;
        if (typeof az.href === "string") {
            az.href = ao.trim(az.href)
        }
    }

    function aC(b, a) {
        ao(aQ).trigger(b);
        aP.trigger(b);
        if (ao.isFunction(a)) {
            a.call(aS)
        }
    }

    var aA = (function () {
        var a, b = at + "Slideshow_", d = "click." + at, i;

        function c() {
            clearTimeout(i)
        }

        function f() {
            if (az.loop || a7[a8 + 1]) {
                c();
                i = setTimeout(av.next, az.slideshowSpeed)
            }
        }

        function g() {
            bb.html(az.slideshowStop).unbind(d).one(d, h);
            aP.bind(aY, f).bind(a0, c);
            aH.removeClass(b + "off").addClass(b + "on")
        }

        function h() {
            c();
            aP.unbind(aY, f).unbind(a0, c);
            bb.html(az.slideshowStart).unbind(d).one(d, function () {
                av.next();
                g()
            });
            aH.removeClass(b + "on").addClass(b + "off")
        }

        function e() {
            a = false;
            bb.hide();
            c();
            aP.unbind(aY, f).unbind(a0, c);
            aH.removeClass(b + "off " + b + "on")
        }

        return function () {
            if (a) {
                if (!az.slideshow) {
                    aP.unbind(aU, e);
                    e()
                }
            } else {
                if (az.slideshow && a7[1]) {
                    a = true;
                    aP.one(aU, e);
                    if (az.slideshowAuto) {
                        g()
                    } else {
                        h()
                    }
                    bb.show()
                }
            }
        }
    }());

    function bi(a) {
        if (!aI) {
            aS = a;
            bs();
            a7 = ao(aS);
            a8 = 0;
            if (az.rel !== "nofollow") {
                a7 = ao("." + ap).filter(function () {
                    var b = ao.data(this, aK), c;
                    if (b) {
                        c = ao(this).data("rel") || b.rel || this.rel
                    }
                    return (c === az.rel)
                });
                a8 = a7.index(aS);
                if (a8 === -1) {
                    a7 = a7.add(aS);
                    a8 = a7.length - 1
                }
            }
            a3.css({
                opacity: parseFloat(az.opacity),
                cursor: az.overlayClose ? "pointer" : "auto",
                visibility: "visible"
            }).show();
            if (aG) {
                aH.add(a3).removeClass(aG)
            }
            if (az.className) {
                aH.add(a3).addClass(az.className)
            }
            aG = az.className;
            if (az.closeButton) {
                aJ.html(az.close).appendTo(aL)
            } else {
                aJ.appendTo("<div/>")
            }
            if (!aq) {
                aq = bn = true;
                aH.css({visibility: "hidden", display: "block"});
                aV = bd(aO, "LoadedContent", "width:0; height:0; overflow:hidden");
                aL.css({width: "", height: ""}).append(aV);
                bc = bh.height() + aF.height() + aL.outerHeight(true) - aL.height();
                be = aT.width() + a9.width() + aL.outerWidth(true) - aL.width();
                bm = aV.outerHeight(true);
                bo = aV.outerWidth(true);
                az.w = ay(az.initialWidth, "x");
                az.h = ay(az.initialHeight, "y");
                aV.css({width: "", height: az.h});
                av.position();
                aC(a2, az.onOpen);
                aR.add(bf).hide();
                aH.focus();
                if (az.trapFocus) {
                    if (aQ.addEventListener) {
                        aQ.addEventListener("focus", aB, true);
                        aP.one(aW, function () {
                            aQ.removeEventListener("focus", aB, true)
                        })
                    }
                }
                if (az.returnFocus) {
                    aP.one(aW, function () {
                        ao(aS).focus()
                    })
                }
            }
            bk()
        }
    }

    function br() {
        if (!aH && aQ.body) {
            ba = false;
            bj = ao(aD);
            aH = bd(aO).attr({
                id: aK,
                "class": ao.support.opacity === false ? at + "IE" : "",
                role: "dialog",
                tabindex: "-1"
            }).hide();
            a3 = bd(aO, "Overlay").hide();
            aZ = ao([bd(aO, "LoadingOverlay")[0], bd(aO, "LoadingGraphic")[0]]);
            bl = bd(aO, "Wrapper");
            aL = bd(aO, "Content").append(bf = bd(aO, "Title"), aN = bd(aO, "Current"), a5 = ao('<button type="button"/>').attr({id: at + "Previous"}), a1 = ao('<button type="button"/>').attr({id: at + "Next"}), bb = bd("button", "Slideshow"), aZ);
            aJ = ao('<button type="button"/>').attr({id: at + "Close"});
            bl.append(bd(aO).append(bd(aO, "TopLeft"), bh = bd(aO, "TopCenter"), bd(aO, "TopRight")), bd(aO, false, "clear:left").append(aT = bd(aO, "MiddleLeft"), aL, a9 = bd(aO, "MiddleRight")), bd(aO, false, "clear:left").append(bd(aO, "BottomLeft"), aF = bd(aO, "BottomCenter"), bd(aO, "BottomRight"))).find("div div").css({"float": "left"});
            aX = bd(aO, false, "position:absolute; width:9999px; visibility:hidden; display:none; max-width:none;");
            aR = a1.add(a5).add(aN).add(bb);
            ao(aQ.body).append(a3, aH.append(bl, aX))
        }
    }

    function bp() {
        function a(b) {
            if (!(b.which > 1 || b.shiftKey || b.altKey || b.metaKey || b.ctrlKey)) {
                b.preventDefault();
                bi(this)
            }
        }

        if (aH) {
            if (!ba) {
                ba = true;
                a1.click(function () {
                    av.next()
                });
                a5.click(function () {
                    av.prev()
                });
                aJ.click(function () {
                    av.close()
                });
                a3.click(function () {
                    if (az.overlayClose) {
                        av.close()
                    }
                });
                ao(aQ).bind("keydown." + at, function (b) {
                    var c = b.keyCode;
                    if (aq && az.escKey && c === 27) {
                        b.preventDefault();
                        av.close()
                    }
                    if (aq && az.arrowKey && a7[1] && !b.altKey) {
                        if (c === 37) {
                            b.preventDefault();
                            a5.click()
                        } else {
                            if (c === 39) {
                                b.preventDefault();
                                a1.click()
                            }
                        }
                    }
                });
                if (ao.isFunction(ao.fn.on)) {
                    ao(aQ).on("click." + at, "." + ap, a)
                } else {
                    ao("." + ap).live("click." + at, a)
                }
            }
            return true
        }
        return false
    }

    if (ao.colorbox) {
        return
    }
    ao(br);
    av = ao.fn[aK] = ao[aK] = function (c, b) {
        var a = this;
        c = c || {};
        br();
        if (bp()) {
            if (ao.isFunction(a)) {
                a = ao("<a/>");
                c.open = true
            } else {
                if (!a[0]) {
                    return a
                }
            }
            if (b) {
                c.onComplete = b
            }
            a.each(function () {
                ao.data(this, aK, ao.extend({}, ao.data(this, aK) || aM, c))
            }).addClass(ap);
            if ((ao.isFunction(c.open) && c.open.call(a)) || c.open) {
                bi(a[0])
            }
        }
        return a
    };
    av.position = function (h, c) {
        var a, j = 0, b = 0, e = aH.offset(), g, f;
        bj.unbind("resize." + at);
        aH.css({top: -90000, left: -90000});
        g = bj.scrollTop();
        f = bj.scrollLeft();
        if (az.fixed) {
            e.top -= g;
            e.left -= f;
            aH.css({position: "fixed"})
        } else {
            j = g;
            b = f;
            aH.css({position: "absolute"})
        }
        if (az.right !== false) {
            b += Math.max(bj.width() - az.w - bo - be - ay(az.right, "x"), 0)
        } else {
            if (az.left !== false) {
                b += ay(az.left, "x")
            } else {
                b += Math.round(Math.max(bj.width() - az.w - bo - be, 0) / 2)
            }
        }
        if (az.bottom !== false) {
            j += Math.max(aE() - az.h - bm - bc - ay(az.bottom, "y"), 0)
        } else {
            if (az.top !== false) {
                j += ay(az.top, "y")
            } else {
                j += Math.round(Math.max(aE() - az.h - bm - bc, 0) / 2)
            }
        }
        aH.css({top: e.top, left: e.left, visibility: "visible"});
        bl[0].style.width = bl[0].style.height = "9999px";
        function d() {
            bh[0].style.width = aF[0].style.width = aL[0].style.width = (parseInt(aH[0].style.width, 10) - be) + "px";
            aL[0].style.height = aT[0].style.height = a9[0].style.height = (parseInt(aH[0].style.height, 10) - bc) + "px"
        }

        a = {width: az.w + bo + be, height: az.h + bm + bc, top: j, left: b};
        if (h) {
            var i = 0;
            ao.each(a, function (k) {
                if (a[k] !== au[k]) {
                    i = h;
                    return
                }
            });
            h = i
        }
        au = a;
        if (!h) {
            aH.css(a)
        }
        aH.dequeue().animate(a, {
            duration: h || 0, complete: function () {
                d();
                bn = false;
                bl[0].style.width = (az.w + bo + be) + "px";
                bl[0].style.height = (az.h + bm + bc) + "px";
                if (az.reposition) {
                    setTimeout(function () {
                        bj.bind("resize." + at, av.position)
                    }, 1)
                }
                if (c) {
                    c()
                }
            }, step: d
        })
    };
    av.resize = function (a) {
        var b;
        if (aq) {
            a = a || {};
            if (a.width) {
                az.w = ay(a.width, "x") - bo - be
            }
            if (a.innerWidth) {
                az.w = ay(a.innerWidth, "x")
            }
            aV.css({width: az.w});
            if (a.height) {
                az.h = ay(a.height, "y") - bm - bc
            }
            if (a.innerHeight) {
                az.h = ay(a.innerHeight, "y")
            }
            if (!a.innerHeight && !a.height) {
                b = aV.scrollTop();
                aV.css({height: "auto"});
                az.h = aV.height()
            }
            aV.css({height: az.h});
            if (b) {
                aV.scrollTop(b)
            }
            av.position(az.transition === "none" ? 0 : az.speed)
        }
    };
    av.prep = function (d) {
        if (!aq) {
            return
        }
        var a, e = az.transition === "none" ? 0 : az.speed;
        aV.empty().remove();
        aV = bd(aO, "LoadedContent").append(d);
        function c() {
            az.w = az.w || aV.width();
            az.w = az.mw && az.mw < az.w ? az.mw : az.w;
            return az.w
        }

        function b() {
            az.h = az.h || aV.height();
            az.h = az.mh && az.mh < az.h ? az.mh : az.h;
            return az.h
        }

        aV.hide().appendTo(aX.show()).css({
            width: c(),
            overflow: az.scrolling ? "auto" : "hidden"
        }).css({height: b()}).prependTo(aL);
        aX.hide();
        ao(ar).css({"float": "none"});
        a = function () {
            var k = a7.length, i, h = "frameBorder", f = "allowTransparency", g;
            if (!aq) {
                return
            }
            function j() {
                if (ao.support.opacity === false) {
                    aH[0].style.removeAttribute("filter")
                }
            }

            g = function () {
                clearTimeout(bq);
                aZ.hide();
                aC(aY, az.onComplete)
            };
            bf.html(az.title).add(aV).show();
            if (k > 1) {
                if (typeof az.current === "string") {
                    aN.html(az.current.replace("{current}", a8 + 1).replace("{total}", k)).show()
                }
                a1[(az.loop || a8 < k - 1) ? "show" : "hide"]().html(az.next);
                a5[(az.loop || a8) ? "show" : "hide"]().html(az.previous);
                aA();
                if (az.preloading) {
                    ao.each([a6(-1), a6(1)], function () {
                        var o, n, m = a7[this], l = ao.data(m, aK);
                        if (l && l.href) {
                            o = l.href;
                            if (ao.isFunction(o)) {
                                o = o.call(m)
                            }
                        } else {
                            o = ao(m).attr("href")
                        }
                        if (o && bg(l, o)) {
                            o = ax(l, o);
                            n = aQ.createElement("img");
                            n.src = o
                        }
                    })
                }
            } else {
                aR.hide()
            }
            if (az.iframe) {
                i = bd("iframe")[0];
                if (h in i) {
                    i[h] = 0
                }
                if (f in i) {
                    i[f] = "true"
                }
                if (!az.scrolling) {
                    i.scrolling = "no"
                }
                ao(i).attr({
                    src: az.href,
                    name: (new Date()).getTime(),
                    "class": at + "Iframe",
                    allowFullScreen: true,
                    webkitAllowFullScreen: true,
                    mozallowfullscreen: true
                }).one("load", g).appendTo(aV);
                aP.one(a4, function () {
                    i.src = "//about:blank"
                });
                if (az.fastIframe) {
                    ao(i).trigger("load")
                }
            } else {
                g()
            }
            if (az.transition === "fade") {
                aH.fadeTo(e, 1, j)
            } else {
                j()
            }
        };
        if (az.transition === "fade") {
            aH.fadeTo(e, 0, function () {
                av.position(0, a)
            })
        } else {
            av.position(e, a)
        }
    };
    function bk() {
        var b, e, c = av.prep, a, d = ++aw;
        bn = true;
        ar = false;
        aS = a7[a8];
        bs();
        aC(a4);
        aC(a0, az.onLoad);
        az.h = az.height ? ay(az.height, "y") - bm - bc : az.innerHeight && ay(az.innerHeight, "y");
        az.w = az.width ? ay(az.width, "x") - bo - be : az.innerWidth && ay(az.innerWidth, "x");
        az.mw = az.w;
        az.mh = az.h;
        if (az.maxWidth) {
            az.mw = ay(az.maxWidth, "x") - bo - be;
            az.mw = az.w && az.w < az.mw ? az.w : az.mw
        }
        if (az.maxHeight) {
            az.mh = ay(az.maxHeight, "y") - bm - bc;
            az.mh = az.h && az.h < az.mh ? az.h : az.mh
        }
        b = az.href;
        bq = setTimeout(function () {
            aZ.show()
        }, 100);
        if (az.inline) {
            a = bd(aO).hide().insertBefore(ao(b)[0]);
            aP.one(a4, function () {
                a.replaceWith(aV.children())
            });
            c(ao(b))
        } else {
            if (az.iframe) {
                c(" ")
            } else {
                if (az.html) {
                    c(az.html)
                } else {
                    if (bg(az, b)) {
                        b = ax(az, b);
                        ar = aQ.createElement("img");
                        ao(ar).addClass(at + "Photo").bind("error", function () {
                            az.title = false;
                            c(bd(aO, "Error").html(az.imgError))
                        }).one("load", function () {
                            var f;
                            if (d !== aw) {
                                return
                            }
                            ao.each(["alt", "longdesc", "aria-describedby"], function (h, i) {
                                var g = ao(aS).attr(i) || ao(aS).attr("data-" + i);
                                if (g) {
                                    ar.setAttribute(i, g)
                                }
                            });
                            if (az.retinaImage && aD.devicePixelRatio > 1) {
                                ar.height = ar.height / aD.devicePixelRatio;
                                ar.width = ar.width / aD.devicePixelRatio
                            }
                            if (az.scalePhotos) {
                                e = function () {
                                    ar.height -= ar.height * f;
                                    ar.width -= ar.width * f
                                };
                                if (az.mw && ar.width > az.mw) {
                                    f = (ar.width - az.mw) / ar.width;
                                    e()
                                }
                                if (az.mh && ar.height > az.mh) {
                                    f = (ar.height - az.mh) / ar.height;
                                    e()
                                }
                            }
                            if (az.h) {
                                ar.style.marginTop = Math.max(az.mh - ar.height, 0) / 2 + "px"
                            }
                            if (a7[1] && (az.loop || a7[a8 + 1])) {
                                ar.style.cursor = "pointer";
                                ar.onclick = function () {
                                    av.next()
                                }
                            }
                            ar.style.width = ar.width + "px";
                            ar.style.height = ar.height + "px";
                            setTimeout(function () {
                                c(ar)
                            }, 1)
                        });
                        setTimeout(function () {
                            ar.src = b
                        }, 1)
                    } else {
                        if (b) {
                            aX.load(b, az.data, function (f, g) {
                                if (d === aw) {
                                    c(g === "error" ? bd(aO, "Error").html(az.xhrError) : ao(this).contents())
                                }
                            })
                        }
                    }
                }
            }
        }
    }

    av.next = function () {
        if (!bn && a7[1] && (az.loop || a7[a8 + 1])) {
            a8 = a6(1);
            bi(a7[a8])
        }
    };
    av.prev = function () {
        if (!bn && a7[1] && (az.loop || a8)) {
            a8 = a6(-1);
            bi(a7[a8])
        }
    };
    av.close = function () {
        if (aq && !aI) {
            aI = true;
            aq = false;
            aC(aU, az.onCleanup);
            bj.unbind("." + at);
            a3.fadeTo(az.fadeOut || 0, 0);
            aH.stop().fadeTo(az.fadeOut || 0, 0, function () {
                aH.add(a3).css({opacity: 1, cursor: "auto"}).hide();
                aC(a4);
                aV.empty().remove();
                setTimeout(function () {
                    aI = false;
                    aC(aW, az.onClosed)
                }, 1)
            })
        }
    };
    av.remove = function () {
        if (!aH) {
            return
        }
        aH.stop();
        ao.colorbox.close();
        aH.stop().remove();
        a3.remove();
        aI = false;
        aH = null;
        ao("." + ap).removeData(aK).removeClass(ap);
        ao(aQ).unbind("click." + at)
    };
    av.element = function () {
        return ao(aS)
    };
    av.settings = aM
}(jQuery, document, window));
(function (c, q, j, m) {
    var g = "ontouchstart" in q, p = g ? "touchstart" : "mousedown", k = g ? "touchmove" : "mousemove", v = g ? "touchend" : "mouseup", b = g ? "touchcancel" : "mouseup", d = function (a) {
        var l = Math.floor(a / 3600), f = Math.floor(a % 3600 / 60), i = Math.ceil(a % 3600 % 60);
        return (l == 0 ? "" : l > 0 && l.toString().length < 2 ? "0" + l + ":" : l + ":") + (f.toString().length < 2 ? "0" + f : f) + ":" + (i.toString().length < 2 ? "0" + i : i)
    }, h = function (a) {
        var f = j.createElement("audio");
        return !!(f.canPlayType && f.canPlayType("audio/" + a.split(".").pop().toLowerCase() + ";").replace(/no/, ""))
    };
    c.fn.audioPlayer = function (f) {
        var f = c.extend({
            classPrefix: "audioplayer",
            strPlay: "Play",
            strPause: "Pause",
            strVolume: "Volume"
        }, f), a = {}, e = {
            playPause: "playpause",
            playing: "playing",
            time: "time",
            timeCurrent: "time-current",
            timeDuration: "time-duration",
            bar: "bar",
            barLoaded: "bar-loaded",
            barPlayed: "bar-played",
            volume: "volume",
            volumeButton: "volume-button",
            volumeAdjust: "volume-adjust",
            noVolume: "novolume",
            mute: "mute",
            mini: "mini"
        };
        for (var i in e) {
            a[i] = f.classPrefix + "-" + e[i]
        }
        this.each(function () {
            if (c(this).prop("tagName").toLowerCase() != "audio") {
                return false
            }
            var J = c(this), O = J.attr("src"), n = J.get(0).getAttribute("autoplay"), n = n === "" || n === "autoplay" ? true : false, A = J.get(0).getAttribute("loop"), A = A === "" || A === "loop" ? true : false, I = false;
            if (typeof O === "undefined") {
                J.find("source").each(function () {
                    O = c(this).attr("src");
                    if (typeof O !== "undefined" && h(O)) {
                        I = true;
                        return false
                    }
                })
            } else {
                if (h(O)) {
                    I = true
                }
            }
            var s = c('<div class="' + f.classPrefix + '">' + (I ? c("<div>").append(J.eq(0).clone()).html() : '<embed src="' + O + '" width="0" height="0" volume="100" autostart="' + n.toString() + '" loop="' + A.toString() + '" />') + '<div class="' + a.playPause + '" title="' + f.strPlay + '"><a href="#">' + f.strPlay + "</a></div></div>"), P = I ? s.find("audio") : s.find("embed"), P = P.get(0);
            if (I) {
                s.find("audio").css({width: 0, height: 0, visibility: "hidden"});
                s.append('<div class="' + a.time + " " + a.timeCurrent + '"></div><div class="' + a.bar + '"><div class="' + a.barLoaded + '"></div><div class="' + a.barPlayed + '"></div></div><div class="' + a.time + " " + a.timeDuration + '"></div><div class="' + a.volume + '"><div class="' + a.volumeButton + '" title="' + f.strVolume + '"><a href="#">' + f.strVolume + '</a></div><div class="' + a.volumeAdjust + '"><div><div></div></div></div></div>');
                var F = s.find("." + a.bar), z = s.find("." + a.barPlayed), U = s.find("." + a.barLoaded), l = s.find("." + a.timeCurrent), Q = s.find("." + a.timeDuration), t = s.find("." + a.volumeButton), K = s.find("." + a.volumeAdjust + " > div"), R = 0, M = function (r) {
                    theRealEvent = g ? r.originalEvent.touches[0] : r;
                    P.currentTime = Math.round(P.duration * (theRealEvent.pageX - F.offset().left) / F.width())
                }, G = function (r) {
                    theRealEvent = g ? r.originalEvent.touches[0] : r;
                    P.volume = Math.abs((theRealEvent.pageY - (K.offset().top + K.height())) / K.height())
                }, o = setInterval(function () {
                    U.width(P.buffered.end(0) / P.duration * 100 + "%");
                    if (P.buffered.end(0) >= P.duration) {
                        clearInterval(o)
                    }
                }, 100);
                var B = P.volume, D = P.volume = 0.111;
                if (Math.round(P.volume * 1000) / 1000 == D) {
                    P.volume = B
                } else {
                    s.addClass(a.noVolume)
                }
                Q.html("…");
                l.text(d(0));
                P.addEventListener("loadeddata", function () {
                    Q.text(d(P.duration));
                    K.find("div").height(P.volume * 100 + "%");
                    R = P.volume
                });
                P.addEventListener("timeupdate", function () {
                    l.text(d(P.currentTime));
                    z.width(P.currentTime / P.duration * 100 + "%")
                });
                P.addEventListener("volumechange", function () {
                    K.find("div").height(P.volume * 100 + "%");
                    if (P.volume > 0 && s.hasClass(a.mute)) {
                        s.removeClass(a.mute)
                    }
                    if (P.volume <= 0 && !s.hasClass(a.mute)) {
                        s.addClass(a.mute)
                    }
                });
                P.addEventListener("ended", function () {
                    s.removeClass(a.playing)
                });
                F.on(p, function (r) {
                    M(r);
                    F.on(k, function (u) {
                        M(u)
                    })
                }).on(b, function () {
                    F.unbind(k)
                });
                t.on("click", function () {
                    if (s.hasClass(a.mute)) {
                        s.removeClass(a.mute);
                        P.volume = R
                    } else {
                        s.addClass(a.mute);
                        R = P.volume;
                        P.volume = 0
                    }
                    return false
                });
                K.on(p, function (r) {
                    G(r);
                    K.on(k, function (u) {
                        G(u)
                    })
                }).on(b, function () {
                    K.unbind(k)
                })
            } else {
                s.addClass(a.mini)
            }
            if (n) {
                s.addClass(a.playing)
            }
            s.find("." + a.playPause).on("click", function () {
                if (s.hasClass(a.playing)) {
                    c(this).attr("title", f.strPlay).find("a").html(f.strPlay);
                    s.removeClass(a.playing);
                    I ? P.pause() : P.Stop()
                } else {
                    c(this).attr("title", f.strPause).find("a").html(f.strPause);
                    s.addClass(a.playing);
                    I ? P.play() : P.Play()
                }
                return false
            });
            J.replaceWith(s)
        });
        return this
    }
})(jQuery, window, document);
$(document).on({
    click: function (e) {
        var i = $("#action-zone");
        var h = i.parents("#menu");
        var j = i.parents(".navbar-inner");
        var g = i.data("open") || false;
        if (g) {
            i.data("open", false).slideUp();
            h.css("height", "auto");
            j.css("overflow", "")
        } else {
            i.data("open", true).slideDown()
        }
    }
}, ".openMenu");
$(window).on({
    resize: function (b) {
        if ($(window).width() > 767) {
            $("#action-zone").css("display", "")
        }
    }
});
$(this).on({
    scroll: function () {
        if ($(this).scrollTop() > $("div#menu").height()) {
            $("div#menu").addClass("shadowNav")
        } else {
            $("div#menu").removeClass("shadowNav")
        }
    }
});
(function (u, D) {
    var y = "backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor", w = /^([\-+])=\s*(\d+\.?\d*)/, A = [{
        re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
        parse: function (a) {
            return [a[1], a[2], a[3], a[4]]
        }
    }, {
        re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
        parse: function (a) {
            return [a[1] * 2.55, a[2] * 2.55, a[3] * 2.55, a[4]]
        }
    }, {
        re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/, parse: function (a) {
            return [parseInt(a[1], 16), parseInt(a[2], 16), parseInt(a[3], 16)]
        }
    }, {
        re: /#([a-f0-9])([a-f0-9])([a-f0-9])/, parse: function (a) {
            return [parseInt(a[1] + a[1], 16), parseInt(a[2] + a[2], 16), parseInt(a[3] + a[3], 16)]
        }
    }, {
        re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
        space: "hsla",
        parse: function (a) {
            return [a[1], a[2] / 100, a[3] / 100, a[4]]
        }
    }], q = u.Color = function (c, d, b, a) {
        return new u.Color.fn.parse(c, d, b, a)
    }, x = {
        rgba: {props: {red: {idx: 0, type: "byte"}, green: {idx: 1, type: "byte"}, blue: {idx: 2, type: "byte"}}},
        hsla: {
            props: {
                hue: {idx: 0, type: "degrees"},
                saturation: {idx: 1, type: "percent"},
                lightness: {idx: 2, type: "percent"}
            }
        }
    }, v = {
        "byte": {floor: true, max: 255},
        percent: {max: 1},
        degrees: {mod: 360, floor: true}
    }, B = q.support = {}, C = u("<p>")[0], r, s = u.each;
    C.style.cssText = "background-color:rgba(1,1,1,.5)";
    B.rgba = C.style.backgroundColor.indexOf("rgba") > -1;
    s(x, function (b, a) {
        a.cache = "_" + b;
        a.props.alpha = {idx: 3, type: "percent", def: 1}
    });
    function p(d, b, a) {
        var c = v[b.type] || {};
        if (d == null) {
            return (a || !b.def) ? null : b.def
        }
        d = c.floor ? ~~d : parseFloat(d);
        if (isNaN(d)) {
            return b.def
        }
        if (c.mod) {
            return (d + c.mod) % c.mod
        }
        return 0 > d ? 0 : c.max < d ? c.max : d
    }

    function z(c) {
        var a = q(), b = a._rgba = [];
        c = c.toLowerCase();
        s(A, function (d, g) {
            var f, e = g.re.exec(c), i = e && g.parse(e), h = g.space || "rgba";
            if (i) {
                f = a[h](i);
                a[x[h].cache] = f[x[h].cache];
                b = a._rgba = f._rgba;
                return false
            }
        });
        if (b.length) {
            if (b.join() === "0,0,0,0") {
                u.extend(b, r.transparent)
            }
            return a
        }
        return r[c]
    }

    q.fn = u.extend(q.prototype, {
        parse: function (e, c, b, a) {
            if (e === D) {
                this._rgba = [null, null, null, null];
                return this
            }
            if (e.jquery || e.nodeType) {
                e = u(e).css(c);
                c = D
            }
            var d = this, g = u.type(e), f = this._rgba = [];
            if (c !== D) {
                e = [e, c, b, a];
                g = "array"
            }
            if (g === "string") {
                return this.parse(z(e) || r._default)
            }
            if (g === "array") {
                s(x.rgba.props, function (h, i) {
                    f[i.idx] = p(e[i.idx], i)
                });
                return this
            }
            if (g === "object") {
                if (e instanceof q) {
                    s(x, function (i, h) {
                        if (e[h.cache]) {
                            d[h.cache] = e[h.cache].slice()
                        }
                    })
                } else {
                    s(x, function (j, i) {
                        var h = i.cache;
                        s(i.props, function (l, k) {
                            if (!d[h] && i.to) {
                                if (l === "alpha" || e[l] == null) {
                                    return
                                }
                                d[h] = i.to(d._rgba)
                            }
                            d[h][k.idx] = p(e[l], k, true)
                        });
                        if (d[h] && u.inArray(null, d[h].slice(0, 3)) < 0) {
                            d[h][3] = 1;
                            if (i.from) {
                                d._rgba = i.from(d[h])
                            }
                        }
                    })
                }
                return this
            }
        }, is: function (a) {
            var c = q(a), d = true, b = this;
            s(x, function (e, h) {
                var g, f = c[h.cache];
                if (f) {
                    g = b[h.cache] || h.to && h.to(b._rgba) || [];
                    s(h.props, function (i, j) {
                        if (f[j.idx] != null) {
                            d = (f[j.idx] === g[j.idx]);
                            return d
                        }
                    })
                }
                return d
            });
            return d
        }, _space: function () {
            var b = [], a = this;
            s(x, function (d, c) {
                if (a[c.cache]) {
                    b.push(d)
                }
            });
            return b.pop()
        }, transition: function (c, a) {
            var b = q(c), f = b._space(), e = x[f], h = this.alpha() === 0 ? q("transparent") : this, g = h[e.cache] || e.to(h._rgba), d = g.slice();
            b = b[e.cache];
            s(e.props, function (n, i) {
                var m = i.idx, j = g[m], l = b[m], k = v[i.type] || {};
                if (l === null) {
                    return
                }
                if (j === null) {
                    d[m] = l
                } else {
                    if (k.mod) {
                        if (l - j > k.mod / 2) {
                            j += k.mod
                        } else {
                            if (j - l > k.mod / 2) {
                                j -= k.mod
                            }
                        }
                    }
                    d[m] = p((l - j) * a + j, i)
                }
            });
            return this[f](d)
        }, blend: function (c) {
            if (this._rgba[3] === 1) {
                return this
            }
            var d = this._rgba.slice(), a = d.pop(), b = q(c)._rgba;
            return q(u.map(d, function (f, e) {
                return (1 - a) * b[e] + a * f
            }))
        }, toRgbaString: function () {
            var a = "rgba(", b = u.map(this._rgba, function (d, c) {
                return d == null ? (c > 2 ? 1 : 0) : d
            });
            if (b[3] === 1) {
                b.pop();
                a = "rgb("
            }
            return a + b.join() + ")"
        }, toHslaString: function () {
            var b = "hsla(", a = u.map(this.hsla(), function (d, c) {
                if (d == null) {
                    d = c > 2 ? 1 : 0
                }
                if (c && c < 3) {
                    d = Math.round(d * 100) + "%"
                }
                return d
            });
            if (a[3] === 1) {
                a.pop();
                b = "hsl("
            }
            return b + a.join() + ")"
        }, toHexString: function (b) {
            var c = this._rgba.slice(), a = c.pop();
            if (b) {
                c.push(~~(a * 255))
            }
            return "#" + u.map(c, function (d) {
                    d = (d || 0).toString(16);
                    return d.length === 1 ? "0" + d : d
                }).join("")
        }, toString: function () {
            return this._rgba[3] === 0 ? "transparent" : this.toRgbaString()
        }
    });
    q.fn.parse.prototype = q.fn;
    function t(b, c, a) {
        a = (a + 1) % 1;
        if (a * 6 < 1) {
            return b + (c - b) * a * 6
        }
        if (a * 2 < 1) {
            return c
        }
        if (a * 3 < 2) {
            return b + (c - b) * ((2 / 3) - a) * 6
        }
        return b
    }

    x.hsla.to = function (b) {
        if (b[0] == null || b[1] == null || b[2] == null) {
            return [null, null, null, b[3]]
        }
        var a = b[0] / 255, h = b[1] / 255, f = b[2] / 255, d = b[3], k = Math.max(a, h, f), l = Math.min(a, h, f), g = k - l, e = k + l, j = e * 0.5, i, c;
        if (l === k) {
            i = 0
        } else {
            if (a === k) {
                i = (60 * (h - f) / g) + 360
            } else {
                if (h === k) {
                    i = (60 * (f - a) / g) + 120
                } else {
                    i = (60 * (a - h) / g) + 240
                }
            }
        }
        if (g === 0) {
            c = 0
        } else {
            if (j <= 0.5) {
                c = g / e
            } else {
                c = g / (2 - e)
            }
        }
        return [Math.round(i) % 360, c, j, d == null ? 1 : d]
    };
    x.hsla.from = function (c) {
        if (c[0] == null || c[1] == null || c[2] == null) {
            return [null, null, null, c[3]]
        }
        var b = c[0] / 360, g = c[1], d = c[2], a = c[3], f = d <= 0.5 ? d * (1 + g) : d + g - d * g, e = 2 * d - f;
        return [Math.round(t(e, f, b + (1 / 3)) * 255), Math.round(t(e, f, b) * 255), Math.round(t(e, f, b - (1 / 3)) * 255), a]
    };
    s(x, function (e, d) {
        var c = d.props, a = d.cache, f = d.to, b = d.from;
        q.fn[e] = function (k) {
            if (f && !this[a]) {
                this[a] = f(this._rgba)
            }
            if (k === D) {
                return this[a].slice()
            }
            var i, j = u.type(k), g = (j === "array" || j === "object") ? k : arguments, h = this[a].slice();
            s(c, function (l, m) {
                var n = g[j === "object" ? l : m.idx];
                if (n == null) {
                    n = h[m.idx]
                }
                h[m.idx] = p(n, m)
            });
            if (b) {
                i = q(b(h));
                i[a] = h;
                return i
            } else {
                return q(h)
            }
        };
        s(c, function (g, h) {
            if (q.fn[g]) {
                return
            }
            q.fn[g] = function (j) {
                var k = u.type(j), m = (g === "alpha" ? (this._hsla ? "hsla" : "rgba") : e), n = this[m](), l = n[h.idx], i;
                if (k === "undefined") {
                    return l
                }
                if (k === "function") {
                    j = j.call(this, l);
                    k = u.type(j)
                }
                if (j == null && h.empty) {
                    return this
                }
                if (k === "string") {
                    i = w.exec(j);
                    if (i) {
                        j = l + parseFloat(i[2]) * (i[1] === "+" ? 1 : -1)
                    }
                }
                n[h.idx] = j;
                return this[m](n)
            }
        })
    });
    q.hook = function (a) {
        var b = a.split(" ");
        s(b, function (d, c) {
            u.cssHooks[c] = {
                set: function (h, j) {
                    var i, f, e = "";
                    if (j !== "transparent" && (u.type(j) !== "string" || (i = z(j)))) {
                        j = q(i || j);
                        if (!B.rgba && j._rgba[3] !== 1) {
                            f = c === "backgroundColor" ? h.parentNode : h;
                            while ((e === "" || e === "transparent") && f && f.style) {
                                try {
                                    e = u.css(f, "backgroundColor");
                                    f = f.parentNode
                                } catch (g) {
                                }
                            }
                            j = j.blend(e && e !== "transparent" ? e : "_default")
                        }
                        j = j.toRgbaString()
                    }
                    try {
                        h.style[c] = j
                    } catch (g) {
                    }
                }
            };
            u.fx.step[c] = function (e) {
                if (!e.colorInit) {
                    e.start = q(e.elem, c);
                    e.end = q(e.end);
                    e.colorInit = true
                }
                u.cssHooks[c].set(e.elem, e.start.transition(e.end, e.pos))
            }
        })
    };
    q.hook(y);
    u.cssHooks.borderColor = {
        expand: function (b) {
            var a = {};
            s(["Top", "Right", "Bottom", "Left"], function (c, d) {
                a["border" + d + "Color"] = b
            });
            return a
        }
    };
    r = u.Color.names = {
        aqua: "#00ffff",
        black: "#000000",
        blue: "#0000ff",
        fuchsia: "#ff00ff",
        gray: "#808080",
        green: "#008000",
        lime: "#00ff00",
        maroon: "#800000",
        navy: "#000080",
        olive: "#808000",
        purple: "#800080",
        red: "#ff0000",
        silver: "#c0c0c0",
        teal: "#008080",
        white: "#ffffff",
        yellow: "#ffff00",
        transparent: [null, null, null, 0],
        _default: "#ffffff"
    }
}(jQuery));
$.fn.bgalpha = function (b) {
    return $(this).each(function () {
        $(this).css("background-color", $.Color(this, "background-color").alpha(b).toRgbaString())
    })
};
$(document).ready(function () {
    $("audio").audioPlayer();
    $("#nav").bgalpha(0.5);
    $("#content .content-background").bgalpha(0.9);
    $("#copyright-line").bgalpha(0.5);
    $(".current, .button a").bgalpha(0.25);
    $(".audioplayer").bgalpha(0.7);
    $(".zoom").colorbox({
        maxWidth: "95%",
        maxHeight: "95%",
        rel: "gallery",
        current: "{current} / {total}",
        title: function () {
            return $(this).find("img").attr("alt")
        }
    });
    $(".zoom-frame").colorbox({
        width: "95%", height: "95%", iframe: true, title: function () {
            return $(this).attr("title")
        }
    })
});
function H() {
    var b;
    clearTimeout(b);
    var b = setTimeout(function () {
        $(".content-background").css("height", "auto");
        var d = $("#nav").height();
        var a = $("#content").height();
        if (a < d) {
            $(".content-background").css("height", (d - $("#copyright-line").height() - 35) + "px")
        }
    }, 1000)
}
$(window).load(H);
$(window).resize(H);