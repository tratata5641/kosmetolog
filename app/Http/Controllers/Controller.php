<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $viewFolder = '';

    protected function prepareView($viewName = 'index', $data = array()){
        $folder = ($this->viewFolder) ? $this->viewFolder . '.' : '';
        return view($folder . $viewName, $data);
    }
}
