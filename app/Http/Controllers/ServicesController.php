<?php

namespace App\Http\Controllers;


class ServicesController extends Controller
{
    protected $viewFolder = 'service';

    public function index()
    {
        return $this->prepareView();
    }

    public function mezoterapiya()
    {
        return $this->prepareView('mezoterapiya');
    }
    public function antiAge()
    {
        return $this->prepareView('antiAge');
    }
    public function biorevitalizatsiya()
    {
        return $this->prepareView('biorevitalizatsiya');
    }
    public function dermaroller()
    {
        return $this->prepareView('dermaroller');
    }
    public function maski()
    {
        return $this->prepareView('maski');
    }
    public function peeling()
    {
        return $this->prepareView('peeling');
    }
    public function chistka()
    {
        return $this->prepareView('chistka');
    }
    public function shugaring()
    {
        return $this->prepareView('shugaring');
    }
    public function summer()
    {
        return $this->prepareView('summer');
    }
}