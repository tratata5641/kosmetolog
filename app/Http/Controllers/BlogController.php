<?php

namespace App\Http\Controllers;


class BlogController extends Controller
{
    protected $viewFolder = 'blog';

    public function blog()
    {
        return $this->prepareView('blog');
    }
    public function kyslota()
    {
        return $this->prepareView('kyslota');
    }
    public function peelingSummer()
    {
        return $this->prepareView('peelingSummer');
    }
    public function shugaringBlog()
    {
        return $this->prepareView('shugaringBlog');
    }
}