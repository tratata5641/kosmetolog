<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('about', function () {
    return view('about');
});

Route::get('contacts/contacts', function () {
    return view('contacts/contacts');
});

Route::get('offer/offers', function () {
    return view('offer/offers');
});

Route::group(['prefix' => 'services'], function () {
    Route::get('/', 'ServicesController@index');
    Route::get('mezoterapiya', 'ServicesController@mezoterapiya');
    Route::get('antiAge','ServicesController@antiAge');
    Route::get('biorevitalizatsiya','ServicesController@biorevitalizatsiya');
    Route::get('dermaroller','ServicesController@dermaroller');
    Route::get('maski','ServicesController@maski');
    Route::get('peeling','ServicesController@peeling');
    Route::get('chistka','ServicesController@chistka');
    Route::get('shugaring','ServicesController@shugaring');
    Route::get('summer','ServicesController@summer');
});

Route::group(['prefix' => 'blog'], function () {
    Route::get('blog', 'BlogController@blog');
    Route::get('kyslota', 'BlogController@kyslota');
    Route::get('peelingSummer','BlogController@peelingSummer');
    Route::get('shugaringBlog','BlogController@shugaringBlog');
});

Route::get('blog/summer', function () {
    return view('service/summer');
});
