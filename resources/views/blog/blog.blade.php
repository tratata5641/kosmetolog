@extends('layouts.default')
@section('title','Блог')
@section('content')
    <div class="content-main">
        <div class="home-block">
            <h2>
                Блог
            </h2>
            <div class="block"> <!--summer-->
                <a href="summer">
                    Подготовка кожи к солнечным ваннам</a>
                <a href="summer">
                    <img src="../images/summer-1.jpg" alt=""/>
                </a>
                <div class="short-description">
                    Что происходит с кожей летом и почему нужна подготовка к солнечным инсоляциям?
                </div>
                <div class="button">
                    <a href="summer" rel="nofollow">
                        Подробнее</a>
                </div>
            </div>
            <div class="block"> <!--shugaringBlog-->
                <a href="shugaringBlog">
                    Главные преимущества шугаринга</a>
                <a href="shugaringBlog">
                    <img src="../images/shugaring-1.jpg" alt=""/>
                </a>
                <div class="short-description">
                    Главные преимущества шугаринга
                </div>
                <div class="button">
                    <a href="shugaringBlog" rel="nofollow">
                        Подробнее</a>
                </div>
            </div>
            <div class="block"> <!--pilingSummer-->
                <a href="peelingSummer">
                    Пилинг летом</a>
                <a href="peelingSummer">
                    <img src="../images/peelingSummer-1.jpg" alt=""/>
                </a>
                <div class="short-description">
                    Можно ли делать пилинг летом? Можно! Но не всякий. Для такого сезона подходят поверхностные химические пилинги. При выборе пилинга обязательно необходимо учитывать индивидуальные особенности кожи.
                </div>
                <div class="button">
                    <a href="peelingSummer" rel="nofollow">
                        Подробнее</a>
                </div>
            </div>
            <div class="block"> <!--kyslota-->
                <a href="kyslota">
                    Гиалуроновая кислота</a>
                <a href="kyslota">
                    <img src="../images/kyslota-1.png" alt=""/>
                </a>
                <div class="short-description">
                    Гиалуроновая кислота – своеобразный магнит для жидкости. Она, как губка, впитывает влагу и перераспределяет ее по тканям организма, предотвращая испарение уже имеющейся в эпидермисе влаги.
                </div>
                <div class="button">
                    <a href="kyslota" rel="nofollow">
                        Подробнее</a>
                </div>
            </div>
        </div>
    </div>
@endsection