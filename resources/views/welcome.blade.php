@extends('layouts.default')

@section('title', 'Личный косметолог')

@section('content')
    <div class="content-main home">
        <div class="about">
            <h1>
                Личный косметолог</h1>
            <div class="description">
                <h2><strong>Виктория Прокопчук</strong></h2>
                <p>Я &ndash; косметолог и эстетист по телу с медицинским и специальным образованием, а
                    также с 6-летним опытом работы в индустрии красоты. Регулярно повышаю свою
                    квалификацию, посещая обучающие семинары, выставки, презентации и курсы. Таким
                    образом, я приобретаю новые знания и опыт, иду в ногу с динамично развивающейся
                    бьюти-индустрией для того, чтобы предложить Вам последние разработки и познакомить
                    Вас с новыми косметологическими достижениями.</p>
                <p>За моими плечами работа и преподавательский опыт в учебном центре &laquo;Polianna&raquo;,
                    а также, опыт косметолога-методиста в Альянсе Компаний ТОП Косметикс - официальный
                    представитель ТМ СHRISTINA, ТМ HYAcorp Fine (Германия), TM JBP (Япония), TM
                    Alexandria professional (США), ТМ Anesi (Испания), ТМ Depileve (Испания).</p>
                <p>Специализируюсь на проведении корректирующих програм: биоревитализация, мезотерапия,
                    микроигольчатая терапия, пилинги, Anti-age, лифтинговых восстанавливающих процедур.
                    Также выполняю процедуры обёртывания и шугаринга.</p>
            </div>
        </div>
        <div class="home-block">
            <h2>Услуги</h2>
            @include('layouts.services')
        </div>
        <div class="home-block">
            <h2>
                Блог</h2>
            <div class="block">
                <a href="services/shugaring">
                    Главные преимущества шугаринга</a>
                <a href="services/shugaring">
                    <img src="images/shugaring-1.jpg" alt=""/>
                </a>
                <div class="short-description">
                </div>
                <div class="button">
                    <a href="services/shugaring" rel="nofollow">
                        Подробнее</a>
                </div>
            </div>
            <div class="block">
                <a href="services/summer">
                    Подготовка кожи к солнечным ваннам</a>
                <a href="services/summer">
                    <img src="images/summer-1.jpg" alt=""/>
                </a>
                <div class="short-description">
                    Что происходит с кожей летом и почему нужна подготовка к солнечным инсоляциям?
                </div>
                <div class="button">
                    <a href="services/summer" rel="nofollow">
                        Подробнее</a>
                </div>
            </div>
            <div class="block">
                <a href="services/peeling">
                    Пилинг летом</a>
                <a href="services/peeling">
                    <img src="images/peelingSummer-1.jpg" alt=""/>
                </a>
                <div class="short-description">
                    Можно ли делать пилинг летом? Можно! Но не всякий. Для такого сезона подходят
                    поверхностные химические пилинги. При выборе пилинга обязательно необходимо
                    учитывать индивидуальные особенности кожи.
                </div>
                <div class="button">
                    <a href="services/peeling" rel="nofollow">
                        Подробнее</a>
                </div>
            </div>
        </div>
    </div>
@endsection