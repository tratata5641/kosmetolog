@extends('layouts.default')

@section('title', 'Биоревитализация')

@section('content')

<div class="content-main">
    <h2 class="headtitle">
        Биоревитализация
    </h2>
    <div class="block">
        <img src="../images/biorevitalizatsiya-2.jpg" alt=""/>
        <div class="description">
            <p>
                Термином биоревитализация обозначают внутрикожные инъекции препаратов высокомолекулярной гиалуроновой кислоты.
                <br/>Почему такое большое значение придается именно гиалуроновой кислоте?Известно, что кожа остается гладкой и упругой благодаря переплетению волокон структурных белков – коллагена и эластина. Коллагеновые волокна фиксируются с помощью эластина, выполняющего функцию связующего материала. Пространство между длинными цепочками молекул коллагена и эластина заполняет гиалуроновая кислота, которая и удерживает волокна в правильном положении. Если гиалуроновой кислоты достаточно, кожа остается подтянутой и упругой, если ее содержание в тканях сокращается – кожа становится морщинистой и вялой, нарушается («плывет») овал лица.
            </p>
            <p>
                Синтез ГК в коже с возрастом также снижается, а разрушение под действием различных внешних и внутренних факторов – ускоряется. Кроме того, при старении качественное состояние ГК становится совсем другим, и она уже не может полноценно выполнять все свои функции. Уменьшение общего содержания влаги приводит к тому, что кожа становится сухой, дряблой, на ней появляются морщины и складки.
            </p>
            <p class="smallheadtitle">
                С какой целью выполняется курс биоревитализации и каких результатов мы добиваемся?
            </p>
            <ul>
                <li>Восполняется имеющийся дефицит ГК в тканях кожи, кожа насыщается водой, становится устойчивой к воздействию внешних факторов.</li>
                <li>Создаются оптимальные физиологические условия для синтеза коллагена и других составляющих внеклеточной матрицы, т.е. естественного обновления дермы.</li>
                <li>Восстанавливается гидробаланс глубоких слоев кожи, повышается ее тонус и тургор.</li>
                <li>Активизируются процессы регенерации. Введенная ГК предупреждает появление морщин, восстанавливает объем тканей, устраняет мелкие и корректирует глубокие морщины.</li>
                <li>Усиливаются фотозащитные свойства кожи, обеспечивается эффективная профилактика фотоповреждения</li>
                <li>Введенная высокомолекулярная ГК способствует инактивации свободных радикалов, оказывает антиоксидантное действие и принимает участие в снижении воспалительного процесса.</li>
                <li>Оптимальное количество гиалуроновой кислоты доставляется непосредственно в зону лечения, благодаря введению ГК на уровне дермы, а не эпидермиса. Это гарантирует эффективность воздействия.</li>
                <li>Улучшается цвет кожи.</li>
            </ul>
            <p class="smallheadtitle">
                Биоревитализация в целях профилактики фотостарения для кожи без возрастных изменений:
            </p>
            <ul>
                <li>основной курс - 2-3 процедуры с интервалом в 15 дней,</li>
                <li>поддерживающий курс – 1 процедура каждые 6 месяцев.</li>
            </ul>
            <p class="smallheadtitle">
                Терапевтическая биоревитализация для кожи с признаками старения:
            </p>
            <ul>
                <li>основной курс - 3-4 процедур с интервалом в 15 дней,</li>
                <li>поддерживающие процедуры - каждые 3-4 месяца.</li>
            </ul>
            <p>
                Длительность косметического эффекта после введения препарата индивидуальна и зависит от многих факторов: структуры кожи, реакции тканей на инъекцию, объема введенного препарата, возраста пациента, образа жизни и др. В летний период в связи с высокой температурой воздуха, усилением солнечной активности, сопутствующими факторами загрязнения окружающей среды процесс биодеградации ГК, как правило, ускоряется. Поэтому летом рекомендуется провести дополнительные процедуры биоревитализации.
            </p>
            <p class="smallheadtitle">
                Результаты
            </p>
            <p>
                Одни эффекты биоревитализации наступают достаточно быстро – в течение первой недели после процедуры, другие носят отсроченный характер. Уже после первого сеанса происходит видимое разглаживание морщин и складок – так называемый эффект лифтинга, который достигается за счет интенсивной гидратации тканей после введения ГК в зоны, где ее недостаток был наиболее очевиден. Однако этот эффект временный и проходит спустя 7-14 дней. Он не является конечной целью процедуры, хотя очень нравится пациентам.
                <br/>Отсроченный (конечный) результат проявляется по окончании курса. Именно в это время кожа становится более устойчивой к негативным внешним воздействиям, ее тонус и наполненность постепенно улучшаются, снижается количество и уменьшается глубина морщин. Благодаря механическому воздействию иглы на кожу улучшается периферическое кровообращение и, соответственно, питание и оксигенация тканей, цвет кожи. Этот эффект довольно стойкий - держится в среднем около 3 месяцев.
            </p>
            <p>
                Сразу же после проведения процедуры рекомендуется использовать средства для ухода за кожей с противовоспалительным и антибактериальным действием. В день процедуры не стоит делать макияж. В течение 3-х дней не следует посещать сауну, баню, бассейн, тренажерный зал. Визит в солярий тоже лучше отложить – дней на 10-14. Химические пилинги или сеансы физиотерапии рекомендуется назначать не ранее, чем через неделю после биоревитализации. В период активного солнца нужно прибегать к дополнительной защите кожи от воздействия УФА-лучей и развития нежелательных эффектов – появления гиперпигментации, «пергаментной» кожи.
            </p>
        </div>
    </div>
</div>

@endsection