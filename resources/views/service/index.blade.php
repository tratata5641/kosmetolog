@extends('layouts.default')

@section('content')
    <div class="content-main">
        <div class="home-block">
            <h2>
                Услуги
            </h2>
            @include('layouts.services')
            <div class="block"> <!--antiAge-->
                <a href="services/antiAge">
                    Омолаживающие процедуры</a>
                <a href="services/antiAge">
                    <img src="/images/antiAge-1.jpg" alt=""/>
                </a>
                <div class="short-description">
                    В зависимости от типа старения, возраста и гормонального статуса подбирается индивидуальная
                    программа ухода, которая будет учитывать все потребности кожи. Каждая салонная процедура состоит из
                    5-8 шагов, которые гармонично дополняют друг друга для достижения наилучших результатов в омоложении
                    кожи.
                </div>
                <div class="button">
                    <a href="services/antiAge" rel="nofollow">
                        Подробнее</a>
                </div>
            </div>
            <div class="block"> <!--maski-->
                <a href="services/maski">
                    Маски</a>
                <a href="services/maski">
                    <img src="/images/maski-1.jpg" alt=""/>
                </a>
                <div class="short-description">
                    Несмотря на столь древнее происхождение, маски не утратили свою актуальность в современном обществе.
                    Маски способствуют улучшению состояния кожи, увлажняют и очищают её, ускоряют регенерацию и
                    разглаживают морщины.
                </div>
                <div class="button">
                    <a href="services/maski" rel="nofollow">
                        Подробнее</a>
                </div>
            </div>
            <div class="block"> <!--piling-->
                <a href="services/peeling">
                    Пилинг</a>
                <a href="services/peeling">
                    <img src="/images/peeling-1.jpg" alt=""/>
                </a>
                <div class="short-description">
                    В последние годы пилинг стал одной из самых популярных косметических процедур, которые проводят в
                    салонах красоты.
                    Пилинг (от англ. to peel – снимать кожуру) – общее название для методов частичного или полного
                    удаления рогового слоя,
                    а также нижележащих слоев кожи с целью омоложения и/или коррекции эстетических дефектов.

                </div>
                <div class="button">
                    <a href="services/peeling" rel="nofollow">
                        Подробнее</a>
                </div>
            </div>
            <div class="block"> <!--chistka-->
                <a href="services/chistka">
                    Чистка</a>
                <a href="services/chistka">
                    <img src="/images/chistka-1.jpg" alt=""/>
                </a>
                <div class="short-description">
                    Часто словосочетание чистка лица сопоставляется с термином проблемная кожа.
                    Однако, комедоны (угри) - появляются на коже более чем 80% жителей мегаполиса.
                    Поскольку комедоны находятся глубоко внутри, никакие кремы, скрабы, маски не помогут удалить их.
                    Облегчить жизнь нашей коже, придать ей гладкости и эстетической привлекательности поможет только
                    профессиональная чистка лица у косметолога.
                </div>
                <div class="button">
                    <a href="services/chistka" rel="nofollow">
                        Подробнее</a>
                </div>
            </div>
            <div class="block"> <!--shugaring-->
                <a href="services/shugaring">
                    Шугаринг</a>
                <a href="services/shugaring">
                    <img src="/images/shugaring-1.jpg" alt=""/>
                </a>
                <div class="short-description">
                    Shugaring.ShortDescription();
                </div>
                <div class="button">
                    <a href="services/shugaring" rel="nofollow">
                        Подробнее</a>
                </div>
            </div>
        </div>
    </div>
@endsection
