<ul id="action-zone" class="nav">
    <div class="block">
        <a href="/services/biorevitalizatsiya">
            Биоревитализация</a>
        <a href="/services/biorevitalizatsiya">
            <img src="/images/biorevitalizatsiya-1.jpg" alt=""/>
        </a>
        <div class="short-description">
            Омоложение и увлажнение кожи на морфологическом уровне посредством введения в нее
            препаратов гиалуроновой кислоты
        </div>
        <div class="button">
            <a href="/services/biorevitalizatsiya" rel="nofollow">
                Подробнее</a>
        </div>
    </div>
    <div class="block"> <!--dermaroller-->
        <a href="services/dermaroller">
            Микроигольчатая терапия (дермароллер)
        </a>
        <a href="services/dermaroller">
            <img src="/images/dermaroller-1.jpg" alt=""/>
        </a>
        <div class="short-description">
            В последние годы эстетическая медицина открывает новые методики безоперационных,
            малоинвазивных технологий. Относительно новой, заслуживающей внимания процедурой
            является дермароллер-терапия, высокий эстетический эффект, хорошую переносимость и
            безопасность которой по достоинству оценили специалисты и их клиенты.
        </div>
        <div class="button">
            <a href="services/dermaroller" rel="nofollow">
                Подробнее</a>
        </div>
    </div>
    <div class="block"> <!--mezoterapiya-->
        <a href="services/mezoterapiya">
            Мезотерапия</a>
        <a href="services/mezoterapiya">
            <img src="/images/mezoterapiya-1.jpg" alt=""/>
        </a>
        <div class="short-description">
            Мезотерапия давно зарекомендовала себя, как высокоэффективная и безопасная
            процедура. Идеальное решение в любом возрасте и при любом типе кожи.
        </div>
        <div class="button">
            <a href="services/mezoterapiya" rel="nofollow">
                Подробнее</a>
        </div>
    </div>
</ul>