<!doctype html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>@yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
	<link rel="shortcut icon" href='/'>
	<link rel="stylesheet" property="stylesheet" type="text/css" href="/css/all.min.css"/>
	<link rel="stylesheet" property="stylesheet" type="text/css" href="/css/custom.css"/>
</head>
<body>
<div id="wrapper">
	<div class="navigation-bar">
		<div class="row">
			<div class="grid_12 margin">
				<a class="openMenu na" href='keke'>Меню</a></div>
		</div>
	</div>
	<div class="row">
		<div class="grid_3" id="nav" style="background-color: rgba(255, 255, 255, 0.498039);">
			<div>
				<div class="logo"><a href="/" title="На главную" rel="nofollow">
						<img src="/images/Logo.png" alt="На главную" title="На главную"/></a></div>
				<div id="menu" class="navbar">
					@include('layouts.menu')
				</div>
				<div class="empty"></div>
			</div>
		</div>
		<div class="grid_6" id="content">
			<div class="content-background">
				@yield('content')
			</div>
			<div id="copyright-line">
				<div class="copyright">
					<span></span>
				</div>
				<div class="developer">
					<span></span>
				</div>
			</div>
		</div>
	</div>
</div>
<script type='text/javascript' src="js/all.min.js"></script>
</body>
</html>
